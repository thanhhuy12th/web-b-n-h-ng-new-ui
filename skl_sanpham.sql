-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 22, 2019 lúc 06:45 AM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bachkhoashop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `skl_sanpham`
--

CREATE TABLE `skl_sanpham` (
  `id` int(11) NOT NULL,
  `tensp` varchar(255) NOT NULL,
  `alias` varchar(150) DEFAULT NULL,
  `img` varchar(999) NOT NULL DEFAULT 'default.jpg',
  `category` tinyint(4) NOT NULL,
  `type_giamgia` tinyint(4) DEFAULT NULL,
  `type_product` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:normal, 1:hot, 2:popular',
  `soluong` int(11) DEFAULT '0',
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `price_km` decimal(10,0) DEFAULT NULL,
  `giamgia` varchar(100) DEFAULT NULL,
  `chitietngan` text,
  `information` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `skl_sanpham`
--

INSERT INTO `skl_sanpham` (`id`, `tensp`, `alias`, `img`, `category`, `type_giamgia`, `type_product`, `soluong`, `price`, `price_km`, `giamgia`, `chitietngan`, `information`, `created_at`, `updated_at`) VALUES
(3, 'Đèn chùm cao cấp RCP-8888/15D 1', 'Den-chum-cao-cap-RCP-8888-15D-1', 'sanpham/i1r7yqJQltUTDf2vYnpCgiV82iV4fGJWSjMuNTv8.jpeg', 27, 1, 0, 20, '26250000', '23625000', NULL, '<p>Đ&egrave;n ch&ugrave;m cao cấp</p>', '<p>M&atilde; sản phẩm: RCP-8888/15DXuất sứ: Nhập Khẩu Bảo h&agrave;nh: 12 Th&aacute;ngC&ocirc;ng suất: 15x 25W/E14 T&igrave;nh trạng: C&ograve;n H&agrave;ngK&iacute;ch thước: D1000*H950mm</p>', '2019-06-17 09:48:02', '2019-05-22 00:12:41'),
(4, 'Đèn chùm cao cấp RCP-8888/15D 2', 'Den-chum-cao-cap-RCP-8888-15D-2', 'sanpham/i1r7yqJQltUTDf2vYnpCgiV82iV4fGJWSjMuNTv8.jpeg', 27, 0, 0, 20, '26250000', '23000000', '0', '<p>Đ&egrave;n ch&ugrave;m cao cấp</p>', '<p>M&atilde; sản phẩm: RCP-8888/15D</p>\r\n\r\n<p>Xuất sứ: Nhập Khẩu</p>\r\n\r\n<p>Bảo h&agrave;nh: 12 Th&aacute;ng</p>\r\n\r\n<p>C&ocirc;ng suất: 15x 25W/E14</p>\r\n\r\n<p>T&igrave;nh trạng: C&ograve;n H&agrave;ng</p>\r\n\r\n<p>K&iacute;ch thước: D1000*H950mm</p>', '2019-06-19 03:40:59', '2019-05-22 00:12:41'),
(5, 'Đèn chùm cao cấp RCP-8888/15D 3', 'Den-chum-cao-cap-RCP-8888-15D-3', 'sanpham/i1r7yqJQltUTDf2vYnpCgiV82iV4fGJWSjMuNTv8.jpeg', 27, 0, 0, 20, '26250000', '23625000', NULL, 'Đèn chùm cao cấp', 'Mã sản phẩm: RCP-8888/15DXuất sứ: Nhập Khẩu Bảo hành: 12 ThángCông suất: 15x 25W/E14 Tình trạng: Còn HàngKích thước: D1000*H950mm', '2019-05-22 00:12:41', '2019-05-22 00:12:41'),
(6, 'Đồng hồ Atlantic AT-60335.41.21R 2', 'Dong-ho-Atlantic-AT-60335-41-21R-2', 'sanpham/xbE033gX9X4ow26smEVH6V7PgN74DKHpYxwJuRfM.jpeg||sanpham/OkWaFonGX4IIXX45D4TqUN3HcNTDCqZ4ReZ6CwqD.jpeg||sanpham/hfG5sW4ZGkEDQ7BBeeXKJmCJJiOzWwC8waVDkghH.jpeg||sanpham/WrYWgq9i2OLKe1c6q1O0cY8BlrsjKG9XuWaVwr5c.jpeg', 27, 1, 0, 12, '9440000', '8440000', NULL, '<p>Thương hiệu: Thụy Sỹ</p>\r\n\r\n<p>Xuất xứ: Thụy Sỹ</p>\r\n\r\n<p>Kiểu d&aacute;ng: Đồng hồ Nam</p>', '<p>H&atilde;ng sản xuất: Atlantic Swiss</p>\r\n\r\n<p>Loại đồng hồ: Đồng hồ nam</p>\r\n\r\n<p>Chất liệu d&acirc;y: Th&eacute;p kh&ocirc;ng gỉ Chất liệu mặt: Sapphire ( K&iacute;nh chống trầy )</p>\r\n\r\n<p>Chất liệu vỏ : Th&eacute;p kh&ocirc;ng gỉ</p>\r\n\r\n<p>Đường k&iacute;nh vỏ : 42 mm</p>\r\n\r\n<p>Chống nước: 3 bar Bảo h&agrave;nh: 10 năm</p>\r\n\r\n<p>Năng lượng sử dụng: Quartz</p>\r\n\r\n<p>Thương hiệu: Thụy Sỹ</p>\r\n\r\n<p>Tư vấn v&agrave; đặt h&agrave;ng: 098.668.1189</p>\r\n\r\n<p>Thanh to&aacute;n: Trực tiếp khi nhận sản phẩm</p>', '2019-06-19 09:11:58', '2019-05-22 00:16:11'),
(7, 'Kệ sách - bàn liền kệ sách - bàn 1', 'Ke-sach---ban-lien-ke-sach---ban-1', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '5100550000', '1300000', NULL, '<p>Gỗ c&ocirc;ng nghiệp phủ melanine chống xước, chống nước</p>\r\n\r\n<p>T&iacute;nh năng L&agrave;m việc, giải tr&iacute;, b&agrave;n laptop</p>\r\n\r\n<p>B&agrave;n để đồ ăn trong kh&ocirc;ng gian nhỏ, c&oacute; nhiều chức năng tiết kiệm kh&ocirc;ng gian sinh hoạt v&agrave; giải tr&iacute; đa dạng</p>', '<p>Sản phẩm được l&agrave;m từ gỗ c&ocirc;ng nghiệp nhập khẩu từ Đ&agrave;i Loan. Về chất lượng hơn hẳn c&aacute;c sản phẩm l&agrave;m từ gỗ c&ocirc;ng nghiệp th&ocirc;ng thường trong nước. Sản phẩm kh&ocirc;ng bị cong v&ecirc;nh, ẩm mốc, mối mọt, kh&ocirc;ng c&oacute; m&ugrave;i, chịu được nước v&agrave; n&oacute;ng ngo&agrave;i ra tấm gỗ c&ograve;n được thiết kế chịu được độ nặng</p>', '2019-06-20 02:37:19', '2019-05-22 00:25:15'),
(8, 'Kệ sách - bàn liền kệ sách - bàn 2', 'Ke-sach---ban-lien-ke-sach---ban-2', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(9, 'Kệ sách - bàn liền kệ sách - bàn 3', 'Ke-sach---ban-lien-ke-sach---ban-3', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(10, 'Kệ sách - bàn liền kệ sách - bàn 4', 'Ke-sach---ban-lien-ke-sach---ban-4', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(11, 'Kệ sách - bàn liền kệ sách - bàn 5', 'Ke-sach---ban-lien-ke-sach---ban-5', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(12, 'Kệ sách - bàn liền kệ sách - bàn 6', 'Ke-sach---ban-lien-ke-sach---ban-6', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(13, 'Kệ sách - bàn liền kệ sách - bàn 7', 'Ke-sach---ban-lien-ke-sach---ban-7', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(14, 'Kệ sách - bàn liền kệ sách - bàn 8', 'Ke-sach---ban-lien-ke-sach---ban-8', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(15, 'Kệ sách - bàn liền kệ sách - bàn 9', 'Ke-sach---ban-lien-ke-sach---ban-9', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(16, 'Kệ sách - bàn liền kệ sách - bàn 10', 'Ke-sach---ban-lien-ke-sach---ban-10', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(17, 'Kệ sách - bàn liền kệ sách - bàn 11', 'Ke-sach---ban-lien-ke-sach---ban-11', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(18, 'Kệ sách - bàn liền kệ sách - bàn 12', 'Ke-sach---ban-lien-ke-sach---ban-12', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(19, 'Kệ sách - bàn liền kệ sách - bàn 13', 'Ke-sach---ban-lien-ke-sach---ban-13', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(20, 'Kệ sách - bàn liền kệ sách - bàn 14', 'Ke-sach---ban-lien-ke-sach---ban-14', 'sanpham/vcXvcfvqAUXaxs4nbX7QSQ74HxYE8IrQhISoMNVN.jpeg', 21, 0, 0, 20, '1550000', '1371020', NULL, 'Gỗ công nghiệp phủ melanine chống xước, chống nước  Tính năng Làm việc, giải trí, bàn laptop  Bàn để đồ ăn trong không gian nhỏ, có nhiều chức năng tiết kiệm không gian sinh hoạt và giải trí đa dạng', 'Sản phẩm được làm từ gỗ công nghiệp nhập khẩu từ Đài Loan. Về chất lượng hơn hẳn các sản phẩm làm từ gỗ công nghiệp thông thường trong nước. Sản phẩm không bị cong vênh, ẩm mốc, mối mọt, không có mùi, chịu được nước và nóng ngoài ra tấm gỗ còn được thiết kế chịu được độ nặng', '2019-05-22 00:25:15', '2019-05-22 00:25:15'),
(23, 'Vadium', 'Vadium', 'sanpham/i5Fu80FIxGEyM6Q61gF0OsNNcw6oKnK1duI3DZxE.jpeg', 19, 0, 1, 100, '200000', '150000', 'DB1', '<p>Vat lieu sieu cung</p>', '<p>Vat lieu sieu cung</p>', '2019-06-08 07:11:21', '2019-06-08 07:11:21'),
(24, 'Lê Mai Quyền', 'Le-Mai-Quyen', 'sanpham/cGnktt2FLSg34l3JEPAsFrxotVtkahVVpVXTe8Xq.jpeg', 22, 0, 0, 1000, '2200000', '200000', NULL, '<p>qưe</p>', '<p>qưewqe</p>', '2019-06-08 07:12:52', '2019-06-08 07:12:52'),
(25, 'Đèn chùm cao cấp RCP-8888/15D', 'Den-chum-cao-cap-RCP-8888-15D', 'sanpham/o8nnYIrmCOM5mBiQYF2QzIrARsddCCCVjF1Gkfpv.jpeg||sanpham/xhmSw6hXTGhtvmUHdfzDaITpTpXsGbCv766MKp9H.jpeg', 19, 0, 0, 100, '200000', '333333', 'DB1', '<p>Đ&egrave;n ch&ugrave;m cao cấp RCP-8888/15D</p>', '<p>Đ&egrave;n ch&ugrave;m cao cấp RCP-8888/15D</p>', '2019-06-11 06:47:10', '2019-06-11 06:47:10');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `skl_sanpham`
--
ALTER TABLE `skl_sanpham`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `skl_sanpham`
--
ALTER TABLE `skl_sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
