<?php

use Illuminate\Database\Seeder;

class DanhmucTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=10;$i++){
	        DB::table('skl_dmsp')->insert([
	         	'alias'	=> 'DM ' .$i,
	         	'name'	=> 'Danh muc ' .$i
	        ]);
 		}
    }
}
