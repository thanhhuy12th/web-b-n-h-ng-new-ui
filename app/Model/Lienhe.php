<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Lienhe extends Model
{
    public $timestamps = true;
    protected $table = 'skl_lienhe';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_lienhe');
    }
    public function storeLienhe($data)
    {
    	return $this->dbTable()
    				->insert($data);
    }
    public function index()
    {
        return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function destroyLienhe($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
}
