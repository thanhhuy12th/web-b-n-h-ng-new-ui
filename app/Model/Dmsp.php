<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;


class Dmsp extends Model
{
    public $timestamps = true;
    protected $table = 'skl_dmsp';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_dmsp');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function storeDmsp($data)
    {
    	return $this->dbTable()
                    ->insert($data);
    }
    public function updateDmsp($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getDmsp($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyDmsp($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }

    public function listCateByIdParent($id)
    {
        return $this->dbTable()
                    ->where('id_parent',$id)
                    ->get()
                    ->toArray();
    }
    
    public function getParent()
    {
        return $this->dbTable()
                    ->select('id','name','id_parent')
                    ->get()
                    ->toArray();
    }
    public function getId()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
    public function getDanhmucSpPage($alias)
    {
        return $this->dbTable()
                    ->where('alias',$alias)
                    ->get()
                    ->first();
    }
    public function getDanhmucSpHome()
    {
        return $this->dbTable()
                    ->where('id_parent',0)
                    ->orderBy('id','asc')
                    ->get()
                    ->toArray();
    }
    public function getIdCon()
    {
        return $this->dbTable()
                    ->orderBy('id','asc')
                    ->get()
                    ->toArray();                 
    }
    public function getIdConPage($idcha)
    {
        return $this->dbTable()
                    ->where('id_parent',$idcha)
                    ->get()->toArray();                 
    }
    public function getIdchaPage($alias)
    {
        return $this->dbTable()->where([
                        ['alias','=', $alias],
                        ['id_parent','=',0],
                    ])
                    ->first();
    }
    public function getAliasCon($alias)
    {
        return $this->dbTable()
                    ->where('alias',$alias)
                    ->first();                 
    }
}
