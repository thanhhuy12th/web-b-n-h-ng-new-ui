<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Province extends Model
{
    //Tinh
    public $timestamps = true;
    protected $table = 'skl_province';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_province');
    }

    public function getData()
    {
    	return $this->dbTable()
                    ->orderBy('name','ASC')
                    ->get()
                    ->toArray();
    }
    public function getWhereId($id)
    {
        return $this->dbTable()
                    ->where('provinceid',$id)
                    ->orderByRaw('position ASC, name ASC')
                    ->get()
                    ->toArray();
    }
    public function getProvince($id)
    {
        return $this->dbTable()
                    ->where('provinceid',$id)
                    ->first();
    }
    public function getProvinceData()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
}
