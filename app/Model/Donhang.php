<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Donhang extends Model
{
    public $timestamps = true;
    protected $table = 'skl_order';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_order');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function destroyDonhang($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function getDonhang($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function updateDonhang($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getDonhangToken($token)
    {
        return $this->dbTable()
                    ->where('token',$token)
                    ->first();
    }
    public function getDonhangByEmail($email)
    {
        return $this->dbTable()
                    ->where('email',$email)
                    ->get()
                    ->toArray();
    }
    public function getDonhangByEmailPagination($email)
    {
        return $this->dbTable()
                    ->rightJoin("skl_orderdetail","skl_order.id","=","skl_orderdetail.id_order")
                    ->where('email',$email)
                    ->paginate(10);
    }
    public function getDonhangByAffCodePagination($aff_code)
    {
        
        return $this->dbTable()
                    ->rightJoin("skl_orderdetail","skl_order.id","=","skl_orderdetail.id_order")
                    ->where('referral',$aff_code)
                    ->paginate(10);
    }
    public function getDonhangByAffCodeLv2Pagination($aff_code)
    {
        
        return DB::table("skl_member as m")
                    ->rightJoin("skl_member as m1","m1.id","=","m.id")
                    ->rightJoin("skl_order as o","o.referral","=","m1.aff_code")
                    ->rightJoin("skl_orderdetail as od","o.id","=","od.id_order")
                    ->where('m.referral_by',$aff_code)
                    ->select("od.*","o.hoten","o.email", "m1.fullname as fullname_ref")
                    ->paginate(10);
    }
    public function getDonhangByAffCodeCount($aff_code)
    {
        
        return $this->dbTable()
                    ->rightJoin("skl_orderdetail","skl_order.id","=","skl_orderdetail.id_order")
                    ->select('skl_orderdetail.*', 'skl_order.hoten', 'skl_order.sodt')
                    ->orderBy('created_at','DESC')
                    ->where('referral',$aff_code)
                    ->get();
    }
    public function getDonhangByAffCodeLv2Count($aff_code)
    {
        
        return DB::table("skl_member as m")
                    ->rightJoin("skl_member as m1","m1.id","=","m.id")
                    ->rightJoin("skl_order as o","o.referral","=","m1.aff_code")
                    ->rightJoin("skl_orderdetail as od","o.id","=","od.id_order")
                    ->where('m.referral_by',$aff_code)
                    ->select("od.*","o.hoten","o.email","o.sodt", "m1.fullname as fullname_ref")
                    ->orderBy('created_at','DESC')
                    ->get();
    }
    public function getDataLimit()
    {
        return $this->dbTable()
                    ->orderBy('created_at','DESC')
                    ->limit(5)
                    ->get()
                    ->toArray();
    }
}
