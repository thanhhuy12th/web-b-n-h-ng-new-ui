<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Status extends Model
{
    public $timestamps = true;
    protected $table = 'skl_status';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_status');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id')
                    ->paginate(10);
    }
    public function date()
    {
    	return $this->dbTable()
                    ->select('begin','end')
                    ->get()
                    ->toArray();
    }
    public function storeStatus($data)
    {
    	return $this->dbTable()->insert($data);
    	// return Sanpham::create($data);
    }
    public function updateStatus($data,$name)
    {
        return $this->dbTable()
                    ->where('name',$name)
                    ->update($data);
    }
    public function getStatus()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
}
