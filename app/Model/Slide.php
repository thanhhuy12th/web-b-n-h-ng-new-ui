<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Slide extends Model
{
    public $timestamps = true;
    protected $table = 'skl_slide';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_slide');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function storeSlide($data)
    {
    	return $this->dbTable()->insert($data);
    	// return Sanpham::create($data);
    }
    public function updateSlide($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getSlide($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroySlide($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function destroyImg($id)
    {
        $path = 'image/';
        $item = $this->dbTable()->select('img')
                                ->where('id',$id)->get()->first();
        if(!empty($item))
        {
                $filename = $path.$item->img;
                $data = File::delete($filename);
        }
    }

    public function getSlideHome()
    {
        return $this->dbTable()
                    ->orderBy('vitri','asc')
                    ->get()
                    ->toArray();
    }

}
