<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Ward extends Model
{
    //Khu vuc
    public $timestamps = true;
    protected $table = 'skl_ward';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_ward');
    }

    public function getData()
    {
    	return $this->dbTable()
                    ->orderBy('name','ASC')
                    ->get()
                    ->toArray();
    }
    public function getWard($id)
    {
        return $this->dbTable()
                    ->where('wardid',$id)
                    ->first();
    }
    public function getWardData()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
}
