<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Hinhthuc extends Model
{
    public $timestamps = true;
    protected $table = 'skl_hinhthuc';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_hinhthuc');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function storeHinhthuc($data)
    {
    	return $this->dbTable()->insert($data);
    	// return Sanpham::create($data);
    }
    public function updateHinhthuc($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getHinhthuc($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyHinhthuc($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function getHinhthucData()
    {
        return $this->dbTable()
                    ->orderBy('id','asc')
                    ->get()
                    ->toArray();
    }
}
