<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Member extends Model
{
    public $timestamps = true;
    protected $table = 'skl_member';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_member');
    }
    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('updated_at','DESC')
                    ->get()
                    ->toArray();
    }
    public function store($data)
    {
    	return $this->dbTable()
    				->insert($data);
    }
    public function getMember($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function updataMember($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function destroyMember($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function getMemberEmail($email)
    {
        return $this->dbTable()
                    ->where('email',$email)
                    ->first();
    }
    public function getMemberEmailWithoutMyEmail($id,$email)
    {
        return $this->dbTable()
                    ->where('id',"!=",$id)
                    ->where('email',$email)
                    ->get()
                    ->toArray();
    }
    public function updataMemberEmailToken($data,$email)
    {
        return $this->dbTable()
                    ->where('email',$email)
                    ->update($data);
    }
    public function updataMemberEmailPass($data,$email,$remember_token)
    {
        return $this->dbTable()
                    ->where([
                        ['email',$email],
                        ['remember_token',$remember_token],
                    ])
                    ->update($data);
    }
    public function checkUpdataMemberEmailPass($email,$remember_token)
    {
        return $this->dbTable()
                    ->where([
                        ['email',$email],
                        ['remember_token',$remember_token],
                    ])
                    ->first();
    }
    public function checkTokenEmail()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
    public function laydulieu()
    {
        return $this->dbTable()
                    ->orderBy('id','ASC')
                    ->get()
                    ->toArray();
    }

    public function listmember()
    {
        return $this->dbTable() 
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function AffUpdate($data, $id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function AffUpdateCreAff($data, $aff_code)
    {
        return $this->dbTable()
                    ->where('aff_code',$aff_code)
                    ->update($data);
    }
    public function getMemberByAffCode($aff_code)
    {
        return $this->dbTable()
                    ->where('aff_code',$aff_code)
                    ->first();
    }
    public function getMemberByReferralArr($referral_by)
    {
        return $this->dbTable()
                    ->where('referral_by',$referral_by)
                    ->get()
                    ->toArray();
    }
    public function getMemberByReferralLv1ArrPaginate($referral_by)
    {
        return $this->dbTable()
                    ->where('referral_by',$referral_by)
                    ->paginate(10);
    }
    public function getMemberByReferralLv2ArrPaginate($referral_by)
    {
        return $this->dbTable()
                    ->rightJoin("skl_member as m1","m1.referral_by","=","skl_member.aff_code")
                    ->where('skl_member.referral_by',$referral_by)
                    ->select("m1.*","skl_member.fullname as fullname_ref")
                    ->paginate(10);
    }
    public function getMemberByReferralLv1ArrCount($referral_by)
    {
        return $this->dbTable()
                    ->orderBy('created_at','DESC')
                    ->where('referral_by',$referral_by)
                    ->get();
    }
    public function getMemberByReferralLv2ArrCount($referral_by)
    {
        return $this->dbTable()
                    ->rightJoin("skl_member as m1","m1.referral_by","=","skl_member.aff_code")
                    ->where('skl_member.referral_by',$referral_by)
                    ->select("m1.*","skl_member.fullname as fullname_ref")
                    ->orderBy('created_at','DESC')
                    ->get();
    }
    public function getDataLimit()
    {
        return $this->dbTable()
                    ->orderBy('updated_at','DESC')
                    ->limit(5)
                    ->get()
                    ->toArray();
    }

    public function getMemberByReferralArrLimit($referral_by)
    {
        return $this->dbTable()
                    ->where('referral_by',$referral_by)
                    ->get()
                    ->toArray();
    }
    
}
