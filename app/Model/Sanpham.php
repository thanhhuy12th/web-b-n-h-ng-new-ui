<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Sanpham extends Model
{
	public $timestamps = true;
    protected $table = 'skl_sanpham';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_sanpham');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
                    
    }
    public function storeProduct($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function updateProduct($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getProduct($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyProduct($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function destroyFile($id)
    {
        $path = 'image/';
        $item = $this->dbTable()->select('img')
                                ->where('id',$id)->get();
        if(!empty($item))
        {
            $arr_img = explode("||",$item);
            for($i=0;$i<count($arr_img);$i++)
            {
                $filename = str_replace( 'image/[{"img":"', 'image/', $path.$arr_img[$i]);
                $data = File::delete($filename);
                
            }
            return $data;
        }
    }
    public function updataImg($id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update(['img' => "product_default.jpg"]);             
    }
    public function getImg($id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->get()
                    ->first();
    }
    public function getSanphamPage()
    {
        return $this->dbTable()
                    ->get();
    }
    public function getSanphamDetail($alias)
    {
        return $this->dbTable()
                    ->where('alias',$alias)
                    ->first();
    }
    public function getSanphamDmDetail($category)
    {
        return $this->dbTable()
                    ->where('category',$category)
                    ->limit(4)
                    ->get()
                    ->toArray();
    }
    public function getSanphamHoadon()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
    public function laydulieu()
    {
        return $this->dbTable()
                    ->orderBy('id','ASC')
                    ->get()
                    ->toArray();
    }
}
