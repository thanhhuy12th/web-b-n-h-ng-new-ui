<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Vanchuyen extends Model
{
    public $timestamps = true;
    protected $table = 'skl_vanchuyen';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_vanchuyen');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function storeVanchuyen($data)
    {
    	return $this->dbTable()
    				->insert($data);

    }
    public function updateVanchuyen($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getVanchuyen($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyVanchuyen($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function getVanchuyenData()
    {
        return $this->dbTable()
                    ->orderBy('id','asc')
                    ->get()
                    ->toArray();
    }
}
