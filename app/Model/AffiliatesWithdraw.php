<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class AffiliatesWithdraw extends Model
{
    //Huyen
    public $timestamps = true;
    protected $table = 'skl_aff_withdraw';

    public function dbTable()
    {
        return DB::table('skl_aff_withdraw');
    }

    public function getData()
    {
        return $this->dbTable()
                    ->join("skl_member","skl_member.id","=","skl_aff_withdraw.aff_id")
                    ->select("skl_aff_withdraw.*","skl_member.fullname","skl_member.phone","skl_member.email","skl_member.credit_aff")
                    ->orderBy('skl_aff_withdraw.id','DESC')
                    ->get()
                    ->toArray();
    }
    public function getWhereId($id)
    {
        return $this->dbTable()
                    ->join("skl_member","skl_member.id","=","skl_aff_withdraw.aff_id")
                    ->select("skl_aff_withdraw.*","skl_member.fullname","skl_member.phone","skl_member.email","skl_member.credit_aff")
                    ->where('skl_aff_withdraw.aff_id',$id)
                    ->orderBy('skl_aff_withdraw.id','DESC')
                    ->get()
                    ->toArray();
    }
    public function getDetail($id)
    {
        return $this->dbTable()
                    ->join("skl_member","skl_member.id","=","skl_aff_withdraw.aff_id")
                    ->select("skl_aff_withdraw.*","skl_member.fullname","skl_member.phone","skl_member.email","skl_member.credit_aff")
                    ->where('skl_aff_withdraw.id',$id)
                    ->first();
    }

    public function AffWithDrawUpdate($data, $id)
    {
        return $this->dbTable()
        ->where('id',$id)
        ->update($data);
    }

    public function storeAff($data)
    {
        return $this->dbTable()->insert($data);
    }
    
    public function listAff($id)
    {
        return $this->dbTable()
                    ->where('aff_id',$id)
                    ->orderBy('created_at','DESC')
                    ->get()
                    ->toArray();
    }

}
