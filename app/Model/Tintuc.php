<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Tintuc extends Model
{
    public $timestamps = true;
    protected $table = 'skl_tintuc';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_tintuc');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id','DESC')
                    ->paginate(10);
    }
    public function store($data)
    {
    	return $this->dbTable()->insert($data);
    	// return Sanpham::create($data);
    }
    public function updateTintuc($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getTintuc($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyTintuc($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function updataImg($id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update(['img' => "product_default.jpg"]);             
    }
    public function destroyFile($id)
    {
        $path = 'image/';
        $item = $this->dbTable()->select('img')
                                ->where('id',$id)->get();
        if(!empty($item))
        {
        	$arr_img = explode("||",$item);
            for($i=0;$i<count($arr_img);$i++)
            {
                $filename = str_replace( 'image/[{"img":"', 'image/', $path.$arr_img[$i]);
                $data = File::delete($filename);
                
            }
            return $data;
        }
    }

    public function getTintucHome()
    {
        return $this->dbTable()
                    ->paginate(4);
    }
    
    public function getTintucPages()
    {
        return $this->dbTable()
                    ->orderBy('id','desc')
                    ->paginate(4);
    }
    public function getDetail($alias)
    {
        return $this->dbTable()
                    ->where('alias',$alias)
                    ->first();
    }
    public function getTintucDetail($alias)
    {
        return $this->dbTable()
                    ->where('alias',$alias)
                    ->first();
    }
    public function getTintucDmDetail($category)
    {
        return $this->dbTable()
                    ->where('category',$category)
                    ->limit(4)
                    ->get()
                    ->toArray();
    }
}
