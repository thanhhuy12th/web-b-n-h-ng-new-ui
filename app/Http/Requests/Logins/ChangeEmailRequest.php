<?php

namespace App\Http\Requests\Logins;

use Illuminate\Foundation\Http\FormRequest;

class ChangeEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [    
            'email'             => 'required|email|unique:skl_member,email',
            'maxacminh'         => 'required|min:6|max:6'
        ];
    }

    public function messages()
    {
        return [
            'email.required'            => trans('message.email_required'),
            'email.email'               => trans('message.email_email'),
            'email.unique'              => trans('message.email_unique'),
            'maxacminh.required'        => trans('message.maxacminh_required'),
            'maxacminh.min'             => trans('message.maxacminh_min'),
            'maxacminh.max'             => trans('message.maxacminh_max')
        ];
    }
}
