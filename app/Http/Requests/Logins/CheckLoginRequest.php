<?php

namespace App\Http\Requests\Logins;

use Illuminate\Foundation\Http\FormRequest;

class CheckLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [    
            'email'             => 'required|email',
            'password'          => 'required',
            
        ];
    }

    public function messages()
    {
        return [
            'email.required'            => trans('message.email_required'),
            'email.email'               => trans('message.email_email'),
            'password.required'         => trans('message.password_required'),
        ];
    }
}
