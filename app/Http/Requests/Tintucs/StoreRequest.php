<?php

namespace App\Http\Requests\Tintucs;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => 'required',
            'category'          => 'required',
            'photos'            => 'required',
            'intro'             => 'required',
            'content'           => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required'            => trans('message.title_required'),
            'category.required'         => trans('message.category_required'),
            'photos.required'           => trans('message.photos_required'),
            'intro.required'            => trans('message.intro_required'),
            'content.required'          => trans('message.content_required')
        ];
    }
}
