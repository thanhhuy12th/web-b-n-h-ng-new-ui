<?php

namespace App\Http\Requests\Members;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'      => 'required',
            'fullname'      => 'required',     
        ];
    }

    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.unique'           => trans('message.username_unique'),
            'fullname.required'         => trans('message.fullname_required'),
        ];
    }
}
