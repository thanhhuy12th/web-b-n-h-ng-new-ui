<?php

namespace App\Http\Requests\Members;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'passwordold'          => 'required|min:6|max:25',
            'passwordnew'          => 'required|min:6|max:25',
            'repeatpasswordnew'    => 'required|min:6|max:25',
            
        ];
    }

    public function messages()
    {
        return [
            'passwordold.min'              => trans('message.passwordnew_min'),
            'passwordold.max'              => trans('message.passwordnew_max'),
            'passwordold.required'         => trans('message.passwordnew_required'),
            'passwordnew.min'              => trans('message.passwordnew_min'),
            'passwordnew.max'              => trans('message.passwordnew_max'),
            'passwordnew.required'         => trans('message.passwordnew_required'),
            'repeatpasswordnew.min'         => trans('message.repeatpasswordnew_min'),
            'repeatpasswordnew.max'         => trans('message.repeatpasswordnew_max'),
            'repeatpasswordnew.required'   => trans('message.repeatpasswordnew_required'),

        ];
    }
}
