<?php

namespace App\Http\Requests\Coupons;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'          => 'required|unique:skl_giamgia,id',
            // 'begin'       => 'required',
            // 'end'         => 'required',
            'money'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id.required'         => trans('message.id_required'),
            'id.unique'           => trans('message.id_unique'),
            // 'begin.required'      => trans('message.begin_required'),
            // 'end.required'        => trans('message.end_required'),
            'money.required'      => trans('message.money_required'),

        ];
    }
}
