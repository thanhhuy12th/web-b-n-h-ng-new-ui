<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use App\Model\Cauhinh;

use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{

    // private $cauhinh;
    // public function __construct(Cauhinh $cauhinh)
    // {
    //     $this->cauhinh  = $cauhinh;
    // }
    public function getProvince($id = null)
    {
        $data = array();

        if($id == null)
        {
            $data = DB::table('skl_province')->orderBy('name','ASC')->get()->toArray();
        }
        else
        {
            $data = DB::table('skl_province')->where('provinceid','=',$id)->orderBy('name','ASC')->get()->toArray();

        }
        echo json_encode($data);
    }

    public function getDistrict($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_district')->orderBy('name','ASC')->get()->toArray();

        }
        else
        {
            $data = DB::table('skl_district')->where('provinceid','=',(int)$id)->orderBy('name','ASC')->get()->toArray();

        }
        
        echo json_encode($data);
    }   

    public function getWard($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_ward')->orderBy('name','ASC')->get()->toArray();

        }
        else
        {
            $data = DB::table('skl_ward')->where('districtid','=',(int)$id)->orderBy('name','ASC')->get()->toArray();

        }
        
        echo json_encode($data);
    }

    public function getHinhthuc($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_hinhthuc')->orderBy('id','ASC')->get()->toArray();

        }       
        echo json_encode($data);
    }

    public function getVanchuyen($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_vanchuyen')->orderBy('id','ASC')->get()->toArray();

        }       
        echo json_encode($data);
    }

    public function getGiamgia()
    {
        $data = DB::table('skl_giamgia')->get()->toArray();   
        echo json_encode($data);
    }

    public function getGiamgiaById(Request $req)
    {
        $res = DB::table('skl_giamgia')->where('id',$req->id)->first();
        $tamtinh = $req->tamtinh;
        if($res){

            if($res->loai == 0)
            {
                if($res->donvi == 0)
                {
                    $tamtinh -= $res->money;
                }else{
                    $tamtinh = ($tamtinh*$res->money)/100;
                }
            }else{/// 1
                if($res->donvi == 0)
                {   
                    $tamtinh -= $res->money;
                }else{
                    $proArr = explode("||",$req->cart);
                    $pro = DB::table('skl_sanpham')->get();
                    for ($i=0; $i < count($proArr); $i++) { 
                        for ($j=0; $j < count($pro); $j++) { 
                            if($proArr[$i] == $pro[$j]->id && $pro[$j]->giamgia == $req->id)
                                {
                                    $price = $pro[$j]->price_km;
                                   
                                }
                        }
                    }  
                    $tamtinh = ($tamtinh-$price)+(($price*50)/100);

                    
                   
                }
            }
            return response()->json(array('msg'=>$tamtinh), 200);
        }else{
            $err = 'Không tồn tại mã giảm giá này';
            return response()->json(array('err'=>$err), 200);
        }
        
        // $proArr = explode("||",$req->cart);
        // foreach ($proArr as $key) {
        //     # code...
        // }
        // $result = json_decode($arr);   
        
        // 
        // if($res)
        // {
        //     return response()->json($res);
        // }else{
        //     return response()->json('Err');
        // }
        
    }

    public function change_status_affilate(Request $request)
    {
        $id = $request->id;
        $status = $request->status =='true' ? 3 : 2;
        $percent_lv1 = DB::table('skl_config')->where('id','percent_lv1')->first()->chitiet;
        $percent_lv2 = DB::table('skl_config')->where('id','percent_lv2')->first()->chitiet;
        $res = DB::table('skl_member')->where('id', $id)->update(array('status' => $status,'percent_lv1'=>$percent_lv1, 'percent_lv2'=>$percent_lv2));
        if($res) {
            $msg = 'Cập nhật trạng thái thành công!!!';
            return response()->json(array('msg'=>$msg), 200);
        }
        else {
            $msg = 'Lỗi hệ thống';
            return response()->json(array('msg'=>$msg), 500);
        }
        
        
    }
}
