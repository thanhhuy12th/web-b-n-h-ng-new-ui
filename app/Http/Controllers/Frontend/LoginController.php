<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Auth;
use Cart;
use Hash;
use App\Model\Member;
use App\Model\District;
use App\Model\Ward;
use App\Model\Province;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use App\Http\Requests\Members\ChangePasswordRequest;
use App\Http\Requests\Members\UpdateAddressRequest;
use App\Http\Requests\Logins\StoreRequest;
use App\Http\Requests\Logins\CheckLoginRequest;
use App\Http\Requests\Logins\ChangeEmailRequest;
use App\Model\Cauhinh;
use App\Model\Dmsp;
use App\Model\Donhang;
use App\Model\AffiliatesWithdraw;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Carbon\Carbon;
use App\Http\Controllers\EmailController;

class LoginController extends Controller
{
    private $withdraw;
	private $guard;
    private $member;
    private $district;
    private $ward;
    private $cauhinh;
    private $province;
    private $dmsp;
    private $donhang;
    private $email;
    
    public function __construct(
                                Member      $member,
                                District    $district,
                                Ward        $ward,
                                Cauhinh     $cauhinh,
                                Dmsp        $dmsp,
                                Province    $province,
                                Donhang     $donhang, 
                                AffiliatesWithdraw    $withdraw,
                                EmailController $email
    ){
 
         $this->guard        = Auth::guard('member');
         $this->member       = $member;
         $this->district     = $district;
         $this->ward         = $ward;
         $this->province     = $province;
         $this->cauhinh      = $cauhinh;
         $this->dmsp         = $dmsp;
         $this->donhang      = $donhang;
         $this->withdraw     = $withdraw;
         $this->email        = $email;
    }

    public function getLogin()
    {

        return view('frontend.pages.login');
    }

    public function postLogin(CheckLoginRequest $request)
    {
        if(isset($_POST['btnDangnhapDonhang']))
        {
            $credentials = $request->only('email', 'password');
            if($this->guard->attempt($credentials))
            {
                return redirect()->back();
            }
            return redirect()->back()->with('alerterrv2','Tài khoản hoặc mật khẩu không đúng');
        }
        if(isset($_POST['btnDangnhapMain']))
        {
            $credentials = $request->only('email', 'password');
            if($this->guard->attempt($credentials))
            {
                if(!empty(Cart::restore(Auth::guard('member')->user()->id)))
                {
                    Cart::restore(Auth::guard('member')->user->id);
                    return redirect()->route('trangchu.index');
                }
                else
                {
                    return redirect()->route('trangchu.index');
                }
                
            }
            return redirect()->route('trangchu.getLogin')->with('alerterrv2','Tài khoản hoặc mật khẩu không đúng');
        }
    }

    public function getLogout()
    {
        if(Cart::count()>0 && Auth::guard('member')->check())
        {
            Cart::store(Auth::guard('member')->user()->id);
            Cart::destroy();
            Auth::guard('member')->logout();
            return redirect()->route('trangchu.index');
        }
        else
        {
            Auth::guard('member')->logout();
            return redirect()->route('trangchu.index'); 
        }
    }

    public function getLogout2()
    {
        if(Cart::count()>0 && Auth::guard('member')->check())
        {
            Cart::store(Auth::guard('member')->user()->id);
            Cart::destroy();
            Auth::guard('member')->logout();
            return redirect()->back();
        }
        else
        {
            Auth::guard('member')->logout();
            return redirect()->back();
        }
    }

    public function getRegister()
    {

        return view('frontend.pages.register');
    }

    public function postRegister(StoreRequest $request)
    {
        $data = array(
                        'username'  => $request->username,
                        'password'  => bcrypt($request->password),
                        'fullname'  => $request->fullname,
                        'phone'     => $request->phone,
                        'email'     => $request->email,
                        'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                    ); 
                
        $this->member->store($data);
        return redirect()->route('trangchu.getLogin')->with('alertsuc','Đăng ký tài khoản thành công');
    }

    public function getInformation()
    {
        $id =  Auth::guard('member')->user()->id;
        $aff_code_lvl_0 =  Auth::guard('member')->user()->aff_code;
        $arr_lvl_1 = $this->member->getMemberByReferralArr($aff_code_lvl_0);
        $arr_member = [];
        foreach ($arr_lvl_1 as $a1) {
            $dataA1= array(
                            "email"=>$a1->email,
                            "aff_code"=>$a1->aff_code
                         );
            array_push($arr_member,$dataA1);
        }
        foreach ($arr_member as $a2) {
            if($a2['aff_code'] != null){
                $arr_lvl_2 = $this->member->getMemberByReferralArr($a2['aff_code']);
                foreach ($arr_lvl_2 as $key) {
                    $dataA2= [
                        "email"=>$key->email,
                        "aff_code"=>$key->aff_code
                     ];
                array_push($arr_member,$dataA2);   

                }
                
            }
        }
        $arr_member_lvl_1 = $this->member->getMemberByReferralArrLimit($aff_code_lvl_0);
        $arr_member_lvl_2 = [];
        $arr_member1 = [];
        foreach ($arr_member_lvl_1 as $a1) {
            $dataA1= array(
                            "email"=>$a1->email,
                            "aff_code"=>$a1->aff_code
                         );
            array_push($arr_member1,$dataA1);
        }
        foreach ($arr_member1 as $a2) {
            if($a2['aff_code'] != null){
                $arr_lvl_2 = $this->member->getMemberByReferralArrLimit($a2['aff_code']);
                foreach ($arr_lvl_2 as $key) {
                    $dataA2= [
                        "email"=>$key->email,
                        "aff_code"=>$key->aff_code
                     ];
                array_push($arr_member_lvl_2,$dataA2);   
                }
            }
        }
        $data['affhistory'] = $this->withdraw->listAff($id);
        $data['arr_member_lvl_2'] = $arr_member_lvl_2;
        $data['arr_member_lvl_1'] = $arr_member_lvl_1;
        $data['getmember'] = $arr_member;
        $data['member'] = $this->member->getDataLimit();
        $data['donhang'] = $this->donhang->getDataLimit();
        return view('frontend.module.member.member-information',$data);
    }

    public function getAddress()
    {

        return view('frontend.module.member.member-update');
    }

}