<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Member;
use App\Model\AffiliatesWithdraw;

class AffiliatesController extends Controller
{
	private $member;
    private $withdraw;

    public function __construct(Member $member, AffiliatesWithdraw $withdraw)
    {
        $this->member = $member;
        $this->withdraw = $withdraw;
    }

	public function postUpAff(Request $req, $id)
	{
        $data = array(
                        'aff_code'  => $req->aff_code,
                        'status' => 2

                    ); 
        try {
            $this->member->AffUpdate($data,$id);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        } 
        return redirect()->back()->with('alertsuc','Yêu cầu của bạn đã được gửi đi.');
	}

    public function postRutTien(Request $req)
    {
        if (isset($_POST['btnmuahang']))
        {
            $data = array(
                        'aff_id'  => $id,
                        'amount' => $req->chuyentien,
                        'note' => 'Credit transfers',
                        'status' => 1
                    ); 
            $datamember = $this->member->getMember($id);
            $credit = $datamember->credit;
            $credit_aff = $datamember->credit_aff;
            $credit += $req->chuyentien;
            $credit_aff -= $req->chuyentien;
            $datatotal = array(
                        'credit'  => $credit,
                        'credit_aff' => $credit_aff
                    );
            $this->member->updataMember($datatotal,$id);
            try {
                
                $this->withdraw->storeAff($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            } 
            return redirect()->back()->with('alertsuc','Yêu cầu của bạn đã được gửi đi.');
        }
        if (isset($_POST['btnnganhang']))
        {
            $data = array(
                        'aff_id'  => $req->note,
                        'amount' => $req->chuyenkhoan,
                        'note' => $noidung,
                        'status' => 0
                    ); 
            
            try {
                $this->withdraw->storeAff($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            } 
            return redirect()->back()->with('alertsuc','Yêu cầu của bạn đã được gửi đi.');
        }
    }
}
