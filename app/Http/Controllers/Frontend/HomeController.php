<?php
namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Sanpham;
use App\Model\Dmsp;
use App\Model\Tintuc;
use App\Model\Slide;
use App\Model\Lienhe;
use Illuminate\Support\Collection;
use App\Http\Requests\Lienhes\StoreRequest;
use Cookie;
use Response;
use Carbon\Carbon;

class HomeController extends Controller
{
    private $sanpham;
    private $dmsp;
    private $tintuc;
    private $slide;
    private $lienhe;
    public function __construct(
                                Sanpham     $sanpham,
                                Dmsp        $dmsp,
                                Tintuc      $tintuc,
                                Slide       $slide,
                                Lienhe      $lienhe
                                )
    {
        $this->sanpham          = $sanpham;
        $this->dmsp             = $dmsp;
        $this->tintuc           = $tintuc;
        $this->slide            = $slide;
        $this->lienhe           = $lienhe;
    }
    public function getSlide()
    {
        $vitri = 0;
        $data = [];
        $slideParent = $this->slide->getSlideHome();
        for($i=0;$i<count($slideParent);$i++)
        {     
            $dataSlide = array( 'vitri'    => $i, 
                                'img'      => $slideParent[$i]->img,
                                'chitiet'  => $slideParent[$i]->chitiet);
            array_push($data, $dataSlide);
        }
        return $data;
    }

    public function index()
    {
        
        $res = \Illuminate\Support\Facades\Request::get('res');
        if($res != null)
        {
            Cookie::queue('aff_code', $res, 43200);
        }
        $data['slide']          = $this->getSlide();
        return view('frontend.pages.index',$data);
    }
    public function getPages($alias)
    {
        $data['idcha']          = $this->dmsp->getIdchaPage($alias);//navigations
        return view('frontend.pages.cate',$data);
    }
    public function getDetail($alias)
    {
        $data['sanpham']        = $this->sanpham->getSanphamDetail($alias);
        return view('frontend.pages.chitiet',$data);
    }
    public function getPagesCon($alias)
    {
        $data['idcon']          = $this->dmsp->getAliasCon($alias);//catecon
        return view('frontend.pages.catecon',$data);
    }
    public function getTintucDetail($alias)
    {
        $data['tintuc']         = $this->tintuc->getDetail($alias);
        return view('frontend.pages.chitiettin',$data);
    }
    public function Gioithieu()
    {
        return view('frontend.pages.gioithieu');
    }
    public function Lienhe()
    {
        return view('frontend.pages.lienhe');
    }
    public function store(StoreRequest $req)
    {
        $data = array(
                        'hoten'   => $req->hoten,
                        'email'   => $req->email,
                        'sdt'     => $req->sdt,
                        'noidung' => $req->noidung,
                        'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                    );
        $this->lienhe->storeLienhe($data);
        return redirect()->back()->with('alertsuc','Gửi thành công');
    }
}