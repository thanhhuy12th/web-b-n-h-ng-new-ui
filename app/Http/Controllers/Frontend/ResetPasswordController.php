<?php

namespace App\Http\Controllers\Frontend;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Model\Member;
use App\Model\Cauhinh;
use App\Http\Requests\Members\ResetPasswordRequest;
use App\Http\Requests\Members\ChangePasswordRequest;
use App\Http\Requests\Members\SendMailPasswordRequest;

class ResetPasswordController extends Controller
{
	private $member;
    private $cauhinh;
    private $email;

    public function __construct(Member $member, Cauhinh $cauhinh, EmailController $email)
    {
        $this->member  = $member;
        $this->cauhinh = $cauhinh;
        $this->email   = $email;

    }
	public function getTimMatKhau()
    {

        return view('frontend.module.member.member-resetpassword');
    }

    public function sendEmail (SendMailPasswordRequest $request) 
    {
        $email_gmail           = $this->cauhinh->getData('email_gmail')->chitiet;
        $password_gmail        = $this->cauhinh->getData('password_gmail')->chitiet;
        $tenwebsite            = $this->cauhinh->getData('tenwebsite')->chitiet;
	  	if (isset($_POST['sendMail']))
        {
        	$email = $this->member->getMemberEmail($_POST['checkemail']);
        	if(isset($email))
        	{
        		$token = "";
        		if(!empty($email->remember_token))
        		{
        			$token = $email->remember_token;
        		}
        		else
        		{
        			$token = substr(MD5(rand(0,10000)),0,16);
        			$data = array(
        						'remember_token' => $token,
        					);
        			$this->member->updataMemberEmailToken($data,$email->email);
        		}
				try {
					$body    = "<a href='http://127.0.0.1:8000/mat-khau-moi?email=".$_POST['checkemail']."&remember_token=".$token." '>Click vào đây nếu bạn muốn thay đổi mật khẩu</a>'";
                    $arr = [];
                    $arr['req'] = $_POST['checkemail'];
                    $arr['subject'] = 'ResetPassword';
                    $arr['body'] = $body;
                    $this->email->sendMail($arr);
				} catch (Exception $e) {
					return redirect()->back()->with('alerterr','Send failed');
				}
                return redirect()->back()->with('alertsuc','Vui lòng kiểm tra email của bạn');
        	}
        	else
        	{
        		return redirect()->back()->with('alerterr','Không tồn tại email '.$_POST['checkemail']);
        	}

		}
  	}
  	public function getNewPassword()
    {

		return view('frontend.module.member.member-newpassword');
	}

	public function postNewPassword(Request $request,ResetPasswordRequest $req)
    {
        if (isset($_POST['updatePass']))
        {
        	$email = Request::get('email');
        	$token = Request::get('remember_token');
        	$check = $this->member->checkUpdataMemberEmailPass($email,$token);
        	if(isset($check) && $token != Null)
        	{
                if($req->renewpassword == $req->newpassword)
                {
    	        	$data = array(
    	        						'password' => bcrypt($req->newpassword),
    	        						'remember_token' => Null,
    	        					);
                    try {
                        $this->member->updataMemberEmailPass($data,$email,$token);
                    } catch (Exception $e) {
                        return redirect()->back()->with('alerterr','Send failed');
                    }
    	        	return redirect()->route('trangchu.getLogin')->with('alertsuc',"Thay đổi mật khẩu thành công");
                }
                else
                {
                    return redirect()->back()->with('alerterr',"Mật khẩu nhập lại không chính xác");
                }

        	}
        	else
        	{
        		return redirect()->back()->with('alerterr',"Không thể thay đổi mật khẩu. Xin vui lòng gửi lại email");
        	}
        }
        else
        {
        	return redirect()->back()->with('alerterr','Lỗi');
        }
	}

}
