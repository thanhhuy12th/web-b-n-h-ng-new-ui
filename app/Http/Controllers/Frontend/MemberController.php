<?php

namespace App\Http\Controllers\Frontend;

use Auth;
use Cart;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use App\Http\Requests\Members\ChangePasswordRequest;
use App\Http\Requests\Members\UpdateInformationRequest;
use App\Http\Requests\Logins\StoreRequest;
use App\Http\Requests\Logins\CheckLoginRequest;
use App\Http\Requests\Logins\ChangeEmailRequest;
use App\Model\Cauhinh;
use App\Model\Dmsp;
use App\Model\Donhang;
use App\Model\AffiliatesWithdraw;
use App\Model\Member;
use App\Model\District;
use App\Model\Ward;
use App\Model\Province;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Carbon\Carbon;
use App\Http\Controllers\EmailController;

class MemberController extends Controller {
	private $withdraw;
	private $guard;
    private $member;
    private $district;
    private $ward;
    private $cauhinh;
    private $province;
    private $dmsp;
    private $donhang;
    private $email;
    
    public function __construct(
                                Member      $member,
                                District    $district,
                                Ward        $ward,
                                Cauhinh     $cauhinh,
                                Dmsp        $dmsp,
                                Province    $province,
                                Donhang     $donhang, 
                                AffiliatesWithdraw    $withdraw,
                                EmailController $email
                             ) 
    {
        $this->guard        = Auth::guard('member');
        $this->member       = $member;
        $this->district     = $district;
        $this->ward         = $ward;
        $this->province     = $province;
        $this->cauhinh      = $cauhinh;
        $this->dmsp         = $dmsp;
        $this->donhang      = $donhang;
        $this->withdraw     = $withdraw;
        $this->email        = $email;
    }

	public function dashboard_page(){
        $data['member'] = $this->guard->user();
        $countOrderLv1 = $this->donhang->getDonhangByAffCodeCount($this->guard->user()->aff_code)->count();
        $countOrderLv2 = $this->donhang->getDonhangByAffCodeLv2Count($this->guard->user()->aff_code)->count();
        $data['totalOrder'] = $countOrderLv1 + $countOrderLv2;
        $countMemberLv1 = $this->member->getMemberByReferralLv1ArrCount($this->guard->user()->aff_code)->count();
        $countMemberLv2 = $this->member->getMemberByReferralLv2ArrCount($this->guard->user()->aff_code)->count();
        $data['totalMember'] = $countMemberLv1 + $countMemberLv2;
        $data['percent_lv1'] = $this->guard->user()->percent_lv1;
        $data['percent_lv2'] = $this->guard->user()->percent_lv2;
        $data['orderLv1'] = $this->donhang->getDonhangByAffCodeCount($this->guard->user()->aff_code)->take(5);
        $data['orderLv2'] = $this->donhang->getDonhangByAffCodeLv2Count($this->guard->user()->aff_code)->take(5);
        $data['memberLv1'] = $this->member->getMemberByReferralLv1ArrCount($this->guard->user()->aff_code)->take(5);
        $data['memberLv2'] = $this->member->getMemberByReferralLv2ArrCount($this->guard->user()->aff_code)->take(5);
        $data['orderAll'] = $this->donhang->getDonhangByEmail($this->guard->user()->email);
		return view('frontend.pages.member.dashboard',$data);
	}

    public function payment_aff(Request $req)
    {
        $getOrder = $this->donhang->getDonhang($req->id);
        $total_order = $getOrder->tongtien;
        $credit = $this->guard->user()->credit;
        if($credit >= $total_order){
            $credit -= $total_order;
            $res = $this->member->updataMember(array('credit' => $credit),$this->guard->user()->id);
            if($res)
            {
                $this->donhang->updateDonhang(array('status_paid' => 1),$req->id);
            }
        }else{
            return redirect()->back()->with('alerterr','Tài khoản của bạn không đủ để thanh toán hóa đơn này');
        }
        
        return redirect()->back()->with('alertsuc','Thanh toán thành công');
    }

	public function information_page()
    {

		return view('frontend.pages.member.information');
	}

	public function password_page()
    {

		return view('frontend.pages.member.password');
	}

	public function email_page()
    {

		return view('frontend.pages.member.email');
	}

	public function email_step_2_page()
    {

		return view('frontend.pages.member.email-step-2');
	}

	public function order_page()
    {
		$data['listOrder'] = $this->donhang->getDonhangByEmailPagination($this->guard->user()->email);
		return view('frontend.pages.member.order',$data);
	}

	public function aff_lv1_page()
    {
		$data['listRef'] = $this->member->getMemberByReferralLv1ArrPaginate($this->guard->user()->aff_code);
		return view('frontend.pages.member.aff-lv1',$data);
	}

	public function aff_lv2_page(){
		$data['listRef'] = $this->member->getMemberByReferralLv2ArrPaginate($this->guard->user()->aff_code);
		return view('frontend.pages.member.aff-lv2',$data);
	}

	public function order_aff_lv1_page(){
        $data['listRefOrder'] = $this->donhang->getDonhangByAffCodePagination($this->guard->user()->aff_code);
        $data['percent_lv1'] = $this->guard->user()->percent_lv1;
		return view('frontend.pages.member.order-aff-lv1',$data);
	}

	public function order_aff_lv2_page(){
        $data['listRefOrder'] = $this->donhang->getDonhangByAffCodeLv2Pagination($this->guard->user()->aff_code);
        $data['percent_lv2'] = $this->guard->user()->percent_lv2;
		return view('frontend.pages.member.order-aff-lv2',$data);
	}

	public function aff_withdraw_page(){

		return view('frontend.pages.member.aff-withdraw');
	}

    public function aff_withdraw_credit(Request $req)
    {
        //Withdraw account
        $id = $this->guard->user()->id;
        if (isset($_POST['btnmuahang']))
        {
            $data = array(
                        'aff_id'  => $id,
                        'amount' => $req->chuyentien,
                        'note' => 'Credit transfers',
                        'status' => 1
                    ); 
            $datamember = $this->member->getMember($id);
            $credit = $datamember->credit;
            $credit_aff = $datamember->credit_aff;
            $credit += $req->chuyentien;
            $credit_aff -= $req->chuyentien;
            $datatotal = array(
                        'credit'  => $credit,
                        'credit_aff' => $credit_aff
                    );
            $this->member->updataMember($datatotal,$id);
            try {
                
                $this->withdraw->storeAff($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            } 
            return redirect()->back()->with('alertsuc','Thành công.');
        }
        //Withdraw bank
        if (isset($_POST['btnnganhang']))
        {
            $data = array(
                        'aff_id'  => $id,
                        'amount' => $req->chuyenkhoan,
                        'note' => $req->noidung,
                        'status' => 0
                    ); 
            
            try {
                $this->withdraw->storeAff($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            } 
            return redirect()->back()->with('alertsuc','Yêu cầu của bạn đã được gửi đi.');
        }
    }

	public function aff_withdraw_history_page(){
        $data['withdrawList'] = $this->withdraw->getWhereId($this->guard->user()->id);
		return view('frontend.pages.member.aff-withdraw-history',$data);
	}

	public function update_information(UpdateInformationRequest $request)
    {
        if(isset($_POST['btnLuuThongTin']))
        {
            try {
                $getId       = Auth::guard('member')->user()->id;
                $getDiachi   = Auth::guard('member')->user()->diachi;
                $data = array(
                                'username'  => $request->username,
                                'fullname'  => $request->fullname,
                                'phone'     => $request->phone,
                                'diachi'    => $request->diachi,
                                'ward'      => $request->ward,
                                'district'  => $request->district,
                                'province'  => $request->province,
                            );
                $this->member->updataMember($data,$getId);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
    }

    public function change_password(ChangePasswordRequest $request)
    {
        if(Auth::guard('member')->check())
        {
            if(isset($_POST['btnLuuMatKhau']))
            {
                $id       		= Auth::guard('member')->user()->id;
                $password_old   = Auth::guard('member')->user()->password;
                if($request->repeatpasswordnew == $request->passwordnew)
                {	
                	if(Hash::check($request->passwordold,$password_old)) {
                        try {
                            $data = array('password' => bcrypt($request->passwordnew));
                            $this->member->updataMember($data,$id);
                        }catch (QueryException $exception) {
                            return back()->with('alerterr','Sai định dạng');
                        } 
	                    return redirect()->back()->with('alertsuc','Đổi mật khẩu thành công');
                	}
                	else {
                		return redirect()->back()->with('alerterr','Mật khẩu cũ chưa chính xác');
                	}
                    
                }
                else
                {
                    return redirect()->back()->with('alerterr','Mật khẩu nhập lại phải trùng với mật khẩu mới');
                } 
            }
        }
        return redirect()->route('member.change_password');
    }

    public function change_email_step_1()
    {
        if(isset($_POST['btnSendMail']))
        {
            if(Auth::guard('member')->check())
            {
                $emailthongbao   = Auth::guard('member')->user()->email;
                $email_gmail     = $this->cauhinh->getData('email_gmail')->chitiet;
                $password_gmail  = $this->cauhinh->getData('password_gmail')->chitiet;
                $tenwebsite      = $this->cauhinh->getData('tenwebsite')->chitiet;
                $baseurl         = $this->cauhinh->getData('linkwebsite')->chitiet;
                $subject         = "Thay Đổi Email";
                $random          = mt_rand(100000,999999);
                $body            = "Mã xác nhận: ".$random."";
                $data = array(
                            'remember_token' => $random,
                        );
                
                try {
                    $res = $this->member->updataMemberEmailToken($data,$emailthongbao);
                    if($res)
                    {
                        $arr = [];
                        $arr['req'] = $emailthongbao;
                        $arr['subject'] = $subject;
                        $arr['body'] = $body;
                        $this->email->sendMail($arr);
                    }
                } catch (Exception $e) {
                    return redirect()->back()->with('alerterr','Gửi email không thành công. Xin vui lòng gửi lại');
                }
                return redirect()->route("member.email-step-2");
            }
            return redirect()->route('trangchu.getLogouts');
        }
    }

    public function change_email_step_2(Request $request)
    {
        if(isset($_POST['btnSaveChange']))
        {
            $member     = Auth::guard('member')->user();
            $userid     = Auth::guard('member')->id();
            $maxacminh  = $request->maxacminh;
            if($maxacminh == $member->remember_token)
            {
            	$exist_email = $this->member->getMemberEmailWithoutMyEmail($userid,$request->email);

            	if(count($exist_email) == 0) {
                    try {       
                        $data = array(
                            'email'  => $request->email,
                        );
                        $this->member->updataMember($data,$member->id);
                    } catch (Exception $e) {
                        return redirect()->back()->with('alerterr','Thay đổi email thất bại');
                    }
            		return redirect()->route('trangchu.getLogout');
            	}
            	else {
            		return redirect()->back()->with('alerterrv2','Đã tồn tại email này');
            	}
                
            }
            return redirect()->back()->with('alerterrv2','Mã xác minh không dúng');
        }
    }
}