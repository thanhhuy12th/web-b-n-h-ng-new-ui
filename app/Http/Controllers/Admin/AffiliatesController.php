<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Member;
use App\Model\Donhang;
use App\Model\AffiliatesWithdraw;
use Illuminate\Database\QueryException;
use DateTime;

class AffiliatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $member;
    private $withdraw;
    private $donhang;

    public function __construct(Member $member, Donhang $donhang, AffiliatesWithdraw $withdraw)
    {
        $this->member   = $member;
        $this->donhang  = $donhang;
        $this->withdraw = $withdraw;

    }
    public function index()
    {
        $data['affiliates'] = $this->member->listmember();
        return view('admin.module.ds_affiliates.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.ds_affiliates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aff_code_lvl_0 =  $this->member->getMember($id);
        $arr_lvl_1 = $this->member->getMemberByReferralArr($aff_code_lvl_0->aff_code);
        $arr_member = [];
        foreach ($arr_lvl_1 as $a1) {
            $dataA1= array(
                            "email"=>$a1->email,
                            "aff_code"=>$a1->aff_code
                         );
            array_push($arr_member,$dataA1);

        }
        foreach ($arr_member as $a2) {
            if($a2['aff_code'] != null){
                $arr_lvl_2 = $this->member->getMemberByReferralArr($a2['aff_code']);
                foreach ($arr_lvl_2 as $key) {
                    $dataA2= [
                        "email"=>$key->email,
                        "aff_code"=>$key->aff_code
                     ];
                array_push($arr_member,$dataA2);   

                }
                
            }
           
        }
        $arr_member_lvl_1 = $this->member->getMemberByReferralArrLimit($aff_code_lvl_0->aff_code);
        $arr_member_lvl_2 = [];
        $arr_member1 = [];
        foreach ($arr_member_lvl_1 as $a1) {
            $dataA1= array(
                            "email"=>$a1->email,
                            "aff_code"=>$a1->aff_code
                         );
            array_push($arr_member1,$dataA1);

        }
        foreach ($arr_member1 as $a2) {
            if($a2['aff_code'] != null){
                $arr_lvl_2 = $this->member->getMemberByReferralArrLimit($a2['aff_code']);
                foreach ($arr_lvl_2 as $key) {
                    $dataA2= [
                        "email"=>$key->email,
                        "aff_code"=>$key->aff_code
                     ];
                array_push($arr_member_lvl_2,$dataA2);   
                }
            }
        }

        $data['aff_code_lvl_0'] = $aff_code_lvl_0;
        $data['affhistory'] = $this->withdraw->listAff($id);
        $data['arr_member_lvl_2'] = $arr_member_lvl_2;
        $data['arr_member_lvl_1'] = $arr_member_lvl_1;
        $data['getmember'] = $arr_member;
        $data['member'] = $this->member->getDataLimit();
        $data['donhang'] = $this->donhang->getDataLimit();
        return view('admin.module.ds_affiliates.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['affiliates'] = $this->member->getMember($id);
        return view('admin.module.ds_affiliates.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array(
                        'percent_lv1'   => $request->percent_lv1,
                        'percent_lv2'   => $request->percent_lv2,
                        'status'        => $request->status
                    ); 
        try {
            $this->member->AffUpdate($data,$id);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        } 
        return redirect()->back()->with('alertsuc','Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
