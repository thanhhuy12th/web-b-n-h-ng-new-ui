<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tintucs\StoreRequest;
use App\Model\Tintuc;
use App\Model\Dmtt;
use Illuminate\Support\Facades\Auth;
use DB;
use Input;
use File;
use Carbon\Carbon;

class TintucController extends Controller
{
    private $tintuc;
    private $dmtt;

    public function __construct(
                                Tintuc $tintuc,
                                Dmtt $dmtt
                                )
    {
        $this->tintuc          = $tintuc;
        $this->dmtt            = $dmtt;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['dmtt'] = $this->dmtt->getParent();
        $data['tintuc']   = $this->tintuc->index();
        return view('admin.module.tintucs.tintuc-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['dmtt'] = $this->dmtt->getParent();
        return view('admin.module.tintucs.tintuc-create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {

        $data = array(
                        'title' => $req->title,
                        'alias' => (empty($req->alias)) ? chuyen($req->title) : $req->alias,
                        'category' => $req->category,
                        'intro' => $req->intro,
                        'content' => $req->content,
                        'img' => $req->img,
                        'id_admin' => Auth::user()->id,
                        'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                    );
        if($req->hasFile('photos')) 
        {
            //Quy dinh duoi
            $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
            //Lay image
            $files = $req->file('photos');
            $exe_flg = true;
            foreach($files as $file) {
                //Lay duoi image
                $extension = strtolower($file->getClientOriginalExtension());
                //Kiem tra duoi co trong quy dinh khong
                $check=in_array($extension,$allowedfileExtension);
                
                if(!$check) {
                    $exe_flg = false;
                    return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
                    break;
                }
            }
            if($exe_flg) {
                //upload tung image
                $arr = [];
                $errMsg1="";
                foreach ($req->photos as $photo) 
                {
                    $filename = $photo->store('tintuc');
                    array_push($arr, $filename);
                }
                if (!empty($arr)) 
                {
                    for ($i = 0; $i<count($arr); $i++) 
                    {
                    if($i + 1 == count($arr))
                        $errMsg1 .= $arr[$i]; 
                    else 
                        $errMsg1 .= $arr[$i]."||"; 
                    }
                }
                else 
                {
                        $errMsg1 = "NULL";
                } 
            $data['img'] = $errMsg1;
            try{
                $this->tintuc->store($data);
            }
            catch(QueryException $e){
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Thêm thành công');
            }  
        }
        else
        {
            $data['img'] = "product_default.jpg";
            $this->tintuc->store($data);
            return redirect()->back()->with('alertsuc','Thêm thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['dmtt'] = $this->dmtt->getParent();
        $data['tintuc'] = $this->tintuc->getTintuc($id);
        return view('admin.module.tintucs.tintuc-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $data = array(
                        'title' => $req->title,
                        'alias' => (empty($req->alias)) ? chuyen($req->title) : $req->alias,
                        'category' => $req->category,
                        'intro' => $req->intro,
                        'content' => $req->content,
                        // 'img' => $req->img,
                        'id_admin' => Auth::user()->id
                    );
            if($req->hasFile('photos')) 
            {
                //Quy dinh duoi
                $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
                //Lay image
                $files = $req->file('photos');
                $exe_flg = true;
                foreach($files as $file) {
                    //Lay duoi image
                    $extension = strtolower($file->getClientOriginalExtension());
                    //Kiem tra duoi co trong quy dinh khong
                    $check=in_array($extension,$allowedfileExtension);
                    
                    if(!$check) {
                        $exe_flg = false;
                        return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
                        break;
                    }
                }
                if($exe_flg) {
                    //upload tung image
                    $arr = [];
                    $errMsg1="";
                    foreach ($req->photos as $photo) 
                    {
                        $filename = $photo->store('tintuc');
                        array_push($arr, $filename);
                    }
                    if (!empty($arr)) 
                    {
                        for ($i = 0; $i<count($arr); $i++) 
                        {
                        if($i + 1 == count($arr))
                            $errMsg1 .= $arr[$i]; 
                        else 
                            $errMsg1 .= $arr[$i]."||"; 
                        }
                    }
                    
                $data['img'] = $req->hinhcu."||".$errMsg1;
                $this->tintuc->updateTintuc($data,$id);
                return redirect()->back()->with('alertsuc','Sửa thành công');
                }
            }
            else
            {
                $this->tintuc->updateTintuc($data,$id);
                return redirect()->back()->with('alertsuc','Sửa thành công');
            }
        }

        if (isset($_POST['xoaEdit']))
        {
            $this->tintuc->destroyFile($id);
            $this->tintuc->updataImg($id);
            return redirect()->back()->with('alertsuc','Xóa hình ảnh thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->tintuc->destroyFile($id);
                $this->tintuc->destroyTintuc($id);
            }
            return redirect()->route('tintuc.index')->with('alertsuc','Xóa thành công');
        }

    }
}
