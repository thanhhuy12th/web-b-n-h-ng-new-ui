<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Useradmins\StoreRequest;
use App\Http\Controllers\Controller;
use App\Model\Useradmin;
use Input;
use Carbon\Carbon;

class UseradminController extends Controller
{
    private $useradmin;


    public function __construct(Useradmin $useradmin)
    {
        $this->useradmin          = $useradmin;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['useradmin']   = $this->useradmin->index();
        return view('admin.module.useradmin.user-list',$data);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.module.useradmin.user-create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {
        $data = array(
                        'username' => $req->username,
                        'password' => bcrypt($req->password),
                        'rule' => $req->rule,
                        'fullname' => $req->fullname,
                        'phone' => $req->phone,
                        'email' =>$req->email,
                        'created_at'=> Carbon::now('Asia/Ho_Chi_Minh')
                    ); 
                
        $this->useradmin->store($data);
        return redirect()->back()->with('alertsuc','Thêm useradmin thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['useradmin'] = $this->useradmin->getUser($id);
        return view('admin.module.useradmin.user-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $data = array(
                        'rule' => $req->rule,
                        'fullname' => $req->fullname,
                        'phone' => $req->phone,
                        'email' =>$req->email,
                    ); 
                
        $this->useradmin->updateUser($data,$id);
        return redirect()->back()->with('alertsuc','Sửa useradmin thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                if($id > 1)
                {
                    $this->useradmin->destroyUser($id);
                }
                else
                {
                    return redirect()->back()->with('alerterr','Vui lòng không xóa admin tổng');
                }
            }
            return redirect()->route('useradmin.index')->with('alertsuc','Xóa user thành công');
        }

    }
}
