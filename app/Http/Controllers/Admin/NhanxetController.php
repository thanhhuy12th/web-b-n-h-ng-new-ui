<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Nhanxets\StoreRequest;
use App\Model\Nhanxet;
use DB;
use Input;
use File;
use Carbon\Carbon;
class NhanxetController extends Controller
{
    private $nhanxet;

    public function __construct(Nhanxet $nhanxet)
    {
        $this->nhanxet          = $nhanxet;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['nhanxet']   = $this->nhanxet->index();
        return view('admin.module.nhanxets.nhanxet-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.nhanxets.nhanxet-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {

        $data = array(
                        'fullname' => $req->fullname,
                        'content' => $req->content,
                        'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                    );

        if($req->hasFile('photos')) 
        {
            //Quy dinh duoi
            $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
            //Lay image
            $file = $req->file('photos');
            //Lay duoi image
            $extension = strtolower($file->getClientOriginalExtension());
            //Kiem tra duoi co trong quy dinh khong
            $check=in_array($extension,$allowedfileExtension);
            if($check) {
                //upload tung image
                $filename = $req->photos->store('nhanxet');
                $data['img'] = $filename;
                $this->nhanxet->storeNhanxet($data);
                return redirect()->back()->with('alertsuc','Thêm thành công');
            }
            else
            {
                return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
            }
        }
        else
        {
            $this->nhanxet->storeNhanxet($data);
            return redirect()->back()->with('alertsuc','Thêm thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['nhanxet'] = $this->nhanxet->getNhanxet($id);
        return view('admin.module.nhanxets.nhanxet-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $data = array(
                        'fullname' => $req->fullname,
                        'content' => $req->content,
                    );
            if($req->hasFile('photos')) 
            {
            //     //Quy dinh duoi
                $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
                //Lay image
                $file = $req->file('photos');
                //Lay duoi image
                $extension = strtolower($file->getClientOriginalExtension());
                //Kiem tra duoi co trong quy dinh khong
                $check=in_array($extension,$allowedfileExtension);
                if($check) {
                    //upload tung image
                    $this->nhanxet->destroyImg($id);
                    $filename = $req->photos->store('nhanxet');
                    $data['img'] = $filename;
                    $this->nhanxet->updateNhanxet($data,$id);
                    return redirect()->back()->with('alertsuc','Sửa thành công');
                }
                else
                {
                    return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
                }
            }
            $this->nhanxet->updateNhanxet($data,$id);
            return redirect()->back()->with('alertsuc','Sửa thành công');
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->slide->destroyImg($id);
                $this->slide->destroyNhanxet($id);
            }
            return redirect()->route('nhanxet.index')->with('alertsuc','Xóa thành công');
        }

    }
}
