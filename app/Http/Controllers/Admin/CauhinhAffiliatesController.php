<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cauhinh;
use DB;

class CauhinhAffiliatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private $cauhinh;
     public function __construct(Cauhinh $cauhinh)
    {
        $this->cauhinh = $cauhinh;
        
    }
    public function getConfigAff()
    {
        $name = [
                    'percent_lv1',
                    'percent_lv2'
                ];
        $arrData = [];
        for($i = 0 ; $i<count($name) ; $i++)
        {
            $data = $this->cauhinh->getData($name[$i]);
            array_push($arrData, $data);
        }
        return $arrData; 
    }
    public function editAffiliatesNoId()
    {
        $data['cauhinh'] = $this->getConfigAff();
        return view('admin.module.cauhinhs-affiliates.update-affiliates',$data);
    }

    public function updateAffiliatesNoId(Request $request)
    {
        $name = [
                    'percent_lv1',
                    'percent_lv2'
                    
                ];
        $data = [
            array('chitiet' => $request->percent_lv1),
            array('chitiet' => $request->percent_lv2),
            
            
                ];
        for($i = 0 ; $i<count($name) ; $i++)
        {
            for($j = 0 ; $j<count($data) ; $j++)
            {
                if($i==$j)
                {
                   
                    $this->cauhinh->updateData($name[$i],$data[$j]);
                }
            }
        }
        return redirect()->back()->with('alertsuc','Sửa thành công');
    }
}
