<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Status;

class HomeController extends Controller
{

	private $status;

    public function __construct(
                                Status $status
                                )
    {
        $this->status          = $status;
        
    }

    public function index()
    {
    	$data = [];
    	$data['status'] = $this->status->index();
    	$data['rule'] = (Auth::user()->rule);
    	$data['fullname'] =(Auth::user()->fullname);
    	
    	return view('admin.pages.index',$data);
    }
}
