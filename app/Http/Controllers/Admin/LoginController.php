<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Status;
use Carbon\Carbon;

class LoginController extends Controller
{
    private $status;

    public function __construct(
                                Status $status
                                )
    {
        $this->status          = $status;
        
    }

    public function getLogin()
    {
        return view('admin.login');
    }
    public function postLogin(Request $request)
    {

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            $arrAcc = $this->status->getStatus();
            $nameLogin = Auth::user()->fullname;
            $arr = [];
            for ($i = 0 ; $i < count($arrAcc) ; $i++) 
            {
                $nameArr = $arrAcc[$i]->name;
                array_push($arr, $nameArr);
            }
            if(in_array($nameLogin,$arr))
            {
                $data = array(
                            'time' => Carbon::now()
                             );
                $this->status->updateStatus($data,$nameLogin);
                return redirect()->route('index');
            }
            else
            {
                $data = array(
                                'name' => Auth::user()->fullname,
                                'rule' => Auth::user()->rule,
                                'time' => Carbon::now()
                                );
                $this->status->storeStatus($data);
                return redirect()->route('index');
            }

        } else {

            return redirect()->route('admin.getLogin');
        }
    }
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('admin.getLogin');
    }
}
