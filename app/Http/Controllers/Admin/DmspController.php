<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dmsps\StoreRequest;
use App\Model\Dmsp;
use Illuminate\Support\Facades\Auth;
use Input;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class DmspController extends Controller
{
    private $dmsp;

    public function __construct(
                                Dmsp $dmsp
                                )
    {
        $this->dmsp          = $dmsp;
        
    }

    public function getParent()
    {
        $data = [];
        
        $optParent = $this->dmsp->listCateByIdParent(0); 
        foreach($optParent  as $oP)
        {
            $dataOptPar = array('id'     => $oP->id, 
                                'name'   => $oP->name);
            array_push($data, $dataOptPar);

            $optChild = $this->dmsp->listCateByIdParent($oP->id);
            foreach ($optChild as $oC) 
            {
                // Tạo mảng 2 chiều danh mục con
                $dataOptChi= [
                                "id"=>$oC->id,
                                "name"=>"-- ".$oC->name
                             ];
                array_push($data, $dataOptChi);
            }
            
        }
        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['dmsp']   = $this->dmsp->index();
        $data['optArr'] = $this->getParent();
        return view('admin.module.dmsps.dmsp-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['optArr'] = $this->getParent();

        return view('admin.module.dmsps.dmsp-create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {
        $menu    = $this->dmsp->getId();
        if(count($menu) < 9)
        {
            $data = array(
                            'name' => $req->name,
                            'alias' => (empty($req->alias)) ? chuyen($req->name) : $req->alias,
                            'vitri' => $req->vitri,
                            'id_parent' => $req->id_parent,
                            'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                        );
            try {
                 $this->dmsp->storeDmsp($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Thêm thành công');
        }
        elseif($req->id_parent == 0)
        {
            $data = array(
                            'name' => $req->name,
                            'alias' => (empty($req->alias)) ? chuyen($req->name) : $req->alias,
                            'vitri' => $req->vitri,
                            'id_parent' => $req->id_parent,
                            'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                        );
            try {
                 $this->dmsp->storeDmsp($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Thêm thành công');
        }
        else
        {
            return redirect()->back()->with('alerterr','Danh mục con đã đạt số lượng tối đa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['optArr'] = $this->getParent();

        $data['dmsp'] = $this->dmsp->getDmsp($id);
        return view('admin.module.dmsps.dmsp-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $data = array(
                        'name' => $req->name,
                        'alias' => (empty($req->alias)) ? chuyen($req->name) : $req->alias,
                        'vitri' => $req->vitri,
                        'id_parent' => $req->id_parent,
                    );
            try {
                $this->dmsp->updateDmsp($data,$id);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->dmsp->destroyDmsp($id);
            }
            return redirect()->route('dmsp.index')->with('alertsuc','Xóa thành công');
        }
    }
}
