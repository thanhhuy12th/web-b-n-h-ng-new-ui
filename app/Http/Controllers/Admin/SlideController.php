<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Slides\StoreRequest;
use App\Model\Slide;
use DB;
use Input;
use File;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class SlideController extends Controller
{
    private $slide;

    public function __construct(Slide $slide)
    {
        $this->slide          = $slide;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['slide']   = $this->slide->index();
        return view('admin.module.slides.slide-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.slides.slide-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {

        $data = array(
                        'tieude' => $req->tieude,
                        'link' => $req->link,
                        'chitiet' => $req->chitiet,
                        'vitri' => $req->vitri,
                        'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                    );

        if($req->hasFile('photos')) 
        {
            //Quy dinh duoi
            $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
            //Lay image
            $file = $req->file('photos');
            //Lay duoi image
            $extension = strtolower($file->getClientOriginalExtension());
            //Kiem tra duoi co trong quy dinh khong
            $check=in_array($extension,$allowedfileExtension);
            if($check) {
                //upload tung image
                $filename = $req->photos->store('slide');
                $data['img'] = $filename;
                try {
                $this->slide->storeSlide($data);
                }catch (QueryException $exception) {
                    return back()->with('alerterr','Sai định dạng');
                }
                return redirect()->back()->with('alertsuc','Thêm slide thành công');
            } 
            else{
                return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
            }
        }
        else
        {
            try {
                $this->slide->storeSlide($data);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Thêm slide thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['slide'] = $this->slide->getSlide($id);
        return view('admin.module.slides.slide-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $data = array(
                        'tieude' => $req->tieude,
                        'link' => $req->link,
                        'chitiet' => $req->chitiet,
                        'vitri' => $req->vitri
                    );
            if($req->hasFile('photos')) 
            {
                //Quy dinh duoi
                $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
                //Lay image
                $file = $req->file('photos');
                //Lay duoi image
                $extension = strtolower($file->getClientOriginalExtension());
                //Kiem tra duoi co trong quy dinh khong
                $check=in_array($extension,$allowedfileExtension);
                if($check) {
                    //upload tung image
                    $this->slide->destroyImg($id);
                    $filename = $req->photos->store('slide');
                    $data['img'] = $filename;
                    try {
                        $this->slide->updateSlide($data,$id);
                    }catch (QueryException $exception) {
                        return back()->with('alerterr','Sai định dạng');
                    }
                    return redirect()->back()->with('alertsuc','Sửa slide thành công');
                }
                else{
                    return redirect()->back()->with('alerterr','Vui lòng chọn đúng định dạng ảnh');
                }
            }
            else
            {
                try {
                    $this->slide->updateSlide($data,$id);
                }catch (QueryException $exception) {
                    return back()->with('alerterr','Sai định dạng');
                }
            }
            return redirect()->back()->with('alertsuc','Sửa slide thành công');
        
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->slide->destroyImg($id);
                $this->slide->destroySlide($id);
            }
            return redirect()->route('slide.index')->with('alertsuc','Xóa slide thành công');
        }

    }
}
