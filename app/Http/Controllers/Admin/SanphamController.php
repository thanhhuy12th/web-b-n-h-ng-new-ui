<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Products\StoreRequest;
use App\Model\Sanpham;
use App\Model\Dmsp;
use App\Model\Coupon;
use App\Http\Controllers\LogController;
use DB;
use Input;
use File;
use Illuminate\Database\QueryException;
use Exception;
use Carbon\Carbon;

class SanphamController extends Controller
{
    private $sanpham;
    private $dmsp;
    private $coupon;
    private $logcontroller;

    public function __construct(
                                LogController $logcontroller,
                                Sanpham        $sanpham,
                                Dmsp           $dmsp,
                                Coupon           $coupon
                                )
    {
        $this->logcontroller    = $logcontroller;
        $this->sanpham          = $sanpham;
        $this->dmsp             = $dmsp;
        $this->coupon           = $coupon;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['dmsp']      = $this->dmsp->getParent();
        $data['sanpham']   = $this->sanpham->index();
        return view('admin.module.product.product-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['coupon']  = $this->coupon->selectData();
        $data['dmsp']    = $this->dmsp->getParent();
        return view('admin.module.product.product-create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {
        $data = array(
                        'tensp'         => $req->tensp,
                        'price'         => $req->price,
                        'chitietngan'   => $req->chitietngan,
                        'category'      => $req->category,
                        'alias'         => (empty($req->alias)) ? chuyen($req->tensp) : $req->alias,
                        'price_km'      => $req->price_km,
                        'information'   => $req->information,
                        'giamgia'       => $req->giamgia,
                        'soluong'       => 0,
                        'type_product'  => $req->type_product,
                        'type_giamgia'  => $req->type_giamgia,
                        'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                    ); 

        if($req->hasFile('photos')) 
        {
            //Quy dinh duoi
            $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
            //Lay image
            $files = $req->file('photos');
            $exe_flg = true;
            foreach($files as $file) {
                //Lay duoi image
                $extension = strtolower($file->getClientOriginalExtension());
                //Kiem tra duoi co trong quy dinh khong
                $check=in_array($extension,$allowedfileExtension);
                
                if(!$check) {
                    $exe_flg = false;
                    break;
                }
            }
            if($exe_flg) {
                //upload tung image
                $arr = [];
                $errMsg1="";
                foreach ($req->photos as $photo) 
                {
                    $filename = $photo->store('sanpham');
                    array_push($arr, $filename);
                }
                if (!empty($arr)) 
                {
                    for ($i = 0; $i<count($arr); $i++) 
                    {
                    if($i + 1 == count($arr))
                        $errMsg1 .= $arr[$i]; 
                    else 
                        $errMsg1 .= $arr[$i]."||"; 
                    }
                }
                else 
                {
                        $errMsg1 = "NULL";
                } 
            $data['img'] = $errMsg1;
            try {
                 $this->sanpham->storeProduct($data);
            }
            catch (QueryException $e) {
                $this->logcontroller->log($e->getMessage());
                return back()->with('alerterr','Vui lòng nhập đúng số tiền');
            }
            catch (Exception $e) {
                $this->logcontroller->log($e->getMessage());
                return back()->with('alerterr','Lỗi không thêm được');
            }
            return redirect()->back()->with('alertsuc','Thêm thành công');
            }  
        }
        else
        {
            $data['img'] = "product_default.jpg";
            try {
                $this->sanpham->storeProduct($data);
            }catch (Exception $e) {
                $this->logcontroller->log($e->getMessage());
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Thêm thành công');
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['coupon']  = $this->coupon->selectData();
        $data['sanpham'] = $this->sanpham->getProduct($id);
        $data['dmsp'] = $this->dmsp->getParent();
        return view('admin.module.product.product-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $req)
    {

        $detroyImgDefault = str_replace('||product_default.jpg','',$this->sanpham->getImg($id)->img);
        $newName = trim($detroyImgDefault,'||');
        $imgArray = explode("||",$newName);

        if (isset($_POST['addTintuc']))
        {

            $data = array(
                            'tensp'         => $req->tensp,
                            'price'         => $req->price,
                            'chitietngan'   => $req->chitietngan,
                            'category'       => $req->category,
                            'alias'         => (empty($req->alias)) ? chuyen($req->tensp) : $req->alias,
                            'price_km'      => $req->price_km,
                            'information'   => $req->information,
                            'giamgia'       => $req->giamgia,
                            'soluong'       => 0,
                            'type_product'  => $req->type_product,
                            'type_giamgia'  => $req->type_giamgia 
                    );

           
                if($req->hasFile('photos')) 
                {
                    if(count($imgArray) < 5 )
                    {
                        //Quy dinh duoi
                        $allowedfileExtension=['jpeg', 'jpg', 'png', 'gif'];
                        //Lay image
                        $files = $req->file('photos');
                        $exe_flg = true;
                        foreach($files as $file) {
                            //Lay duoi image
                            $extension = strtolower($file->getClientOriginalExtension());
                            //Kiem tra duoi co trong quy dinh khong
                            $check=in_array($extension,$allowedfileExtension);
                            
                            if(!$check) {
                                $exe_flg = false;
                                break;
                            }
                        }
                        if($exe_flg) {
                            //upload tung image
                            $arr = [];
                            $errMsg1="";
                            foreach ($req->photos as $photo) 
                            {
                                $filename = $photo->store('sanpham');
                                array_push($arr, $filename);
                            }
                            if (!empty($arr)) 
                            {
                                for ($i = 0; $i<count($arr); $i++) 
                                {
                                if($i + 1 == count($arr))
                                    $errMsg1 .= $arr[$i]; 
                                else 
                                    $errMsg1 .= $arr[$i]."||"; 
                                }
                            }
                            
                        $data['img'] = $errMsg1."||".$req->hinhcu;
                        try {
                            $this->sanpham->updateProduct($data,$id);
                        }catch (Exception $exception) {
                            return back()->with('alerterr','Sai định dạng');
                        }
                        return redirect()->back()->with('alertsuc','Sửa thành công');
                        }
                    }
                    else
                    {
                        return redirect()->back()->with('alerterr','Bạn chỉ được thêm tối đa 5 hình');
                    }  
                }
                else
                {
                    try {
                        $this->sanpham->updateProduct($data,$id);
                    }catch (QueryException $exception) {
                        return back()->with('alerterr','Sai định dạng');
                    }
                    return redirect()->back()->with('alertsuc','Sửa thành công');
                }
            }

        if (isset($_POST['xoaEdit']))
        {
            $this->sanpham->destroyFile($id);
            $this->sanpham->updataImg($id);
            return redirect()->back()->with('alertsuc','Xóa hình ảnh thành công');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    { 
        if (isset($_POST['btnXoaList']))
        {
            $getproduct = $this->sanpham->laydulieu();
            $checked = $request->input('btSelectItem',[]);
            for ($i = 0 ; $i< count($checked);$i++) 
            {
                $vitri= $checked[$i];
                for ($j = 0 ; $j< count($getproduct);$j++) 
                {
                    // echo "<br>";
                    // echo $getid[$vitri]->fullname;echo "<br>";
                    $getid=$getproduct[$vitri]->id;
                }
                $this->sanpham->destroyFile($getid);
                $this->sanpham->destroyProduct($getid);
            }
            return redirect()->route('sanpham.index')->with('alertsuc','Xóa sản phẩm thành công');
        }
        
    }
    
}
