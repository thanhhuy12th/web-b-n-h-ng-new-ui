<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Members\StoreRequest;
use App\Http\Controllers\Controller;
use App\Model\Member;
use Input;
use App\Model\District;
use App\Model\Ward;
use App\Model\Province;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class MemberController extends Controller
{
    private $member;
    private $ward;
    private $cauhinh;
    private $province;


    public function __construct(
                                Member      $member,
                                District    $district,
                                Ward        $ward,
                                Province    $province
                                )
    {
        $this->member       = $member;
        $this->ward         = $ward;
        $this->province     = $province;
        $this->district     = $district;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['province']           = $this->province->getProvinceData();
        $data['district']           = $this->district->getDistrictData();
        $data['ward']               = $this->ward->getWardData();
        $data['member']   = $this->member->index();
        return view('admin.module.members.member-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['getward']        = $this->ward->getData();
        $data['getdistrict']    = $this->district->getData();
        $data['getprovince']    = $this->province->getData();
        return view('admin.module.members.member-create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {
        $data = array(
                        'username'  => $req->username,
                        'password'  => bcrypt($req->password),
                        'diachi'    => $req->diachi,
                        'ward'      => $req->ward,
                        'district'  => $req->district,
                        'province'  => $req->province,
                        'fullname'  => $req->fullname,
                        'phone'     => $req->phone,
                        'email'     => $req->email,
                        'created_at'=> Carbon::now('Asia/Ho_Chi_Minh')
                    ); 
        try {
            $this->member->store($data);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        }  
        return redirect()->back()->with('alertsuc','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['getward']        = $this->ward->getData();
        $data['getdistrict']    = $this->district->getData();
        $data['getprovince']    = $this->province->getData();
        $data['member'] = $this->member->getMember($id);
        return view('admin.module.members.member-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $data = array(
                        'username'  => $req->username,
                        'password'  => bcrypt($req->password),
                        'diachi'    => $req->diachi,
                        'ward'      => $req->ward,
                        'district'  => $req->district,
                        'province'  => $req->province,
                        'fullname'  => $req->fullname,
                        'phone'     => $req->phone,
                        'email'     => $req->email,
                    ); 
        try {
            $this->member->updataMember($data,$id);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        } 
        return redirect()->back()->with('alertsuc','Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->member->destroyMember($id);
            }
            return redirect()->route('member.index')->with('alertsuc','Xóa thành công');
        }
    }
}
