<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\StoreRequest;
use App\Model\Coupon;
use Illuminate\Support\Facades\Auth;
use Input;
use Illuminate\Database\QueryException;
use Carbon\Carbon;

class CouponController extends Controller
{
    private $coupon;

    public function __construct(
                                Coupon $coupon
                                )
    {
        $this->coupon          = $coupon;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['coupon']   = $this->coupon->index();
        return view('admin.module.coupons.coupon-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Carbon::now('Asia/Ho_Chi_Minh');
        $date['hientai'] = date("Y-m-d", strtotime($data));
        return view('admin.module.coupons.coupon-create',$date);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {   
        $now1  = Carbon::now('Asia/Ho_Chi_Minh');
        $now2  = Carbon::now('Asia/Ho_Chi_Minh')->addDay();
        if($req->loai == 0)
        {
            if( $req->begin==null || $req->end==null)
            {
                $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $now1,
                            'end'      => $now2,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => $req->tongtien,
                            'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                            );
                try {
                    $add = $this->coupon->storeCoupon($data); 
                }catch (QueryException $exception) {
                    return back()->with('alerterr','Sai định dạng');
                }
                return redirect()->back()->with('alertsuc','Thêm thành công');
            }
            elseif ($req->begin>$req->end) {
                return redirect()->back()->with('alerterr','Vui lòng nhập ngày bắt đầu lớn hơn ngày kết thúc');
            }
            else{
                $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $req->begin,
                            'end'      => $req->end,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => $req->tongtien,
                            'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                            );
                try {
                    $add = $this->coupon->storeCoupon($data); 
                }catch (QueryException $exception) {
                    return back()->with('alerterr','Sai định dạng');
                }
                return redirect()->back()->with('alertsuc','Thêm thành công');
            }
        }else{  
            if($req->begin==null || $req->end==null)
            {
                $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $now1,
                            'end'      => $now2,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => 0,
                            'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                        );
                try {
                    $add = $this->coupon->storeCoupon($data); 
                }catch (QueryException $exception) {
                    return back()->with('alerterr','Sai định dạng');
                }
                return redirect()->back()->with('alertsuc','Thêm thành công');
            }
            elseif ($req->begin>$req->end) {
                return redirect()->back()->with('alerterr','Vui lòng nhập ngày bắt đầu lớn hơn ngày kết thúc');
            }
            else{
                $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $req->begin,
                            'end'      => $req->end,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => 0,
                            'created_at' => Carbon::now('Asia/Ho_Chi_Minh')
                        );
                try {
                    $add = $this->coupon->storeCoupon($data); 
                }catch (QueryException $exception) {
                    return back()->with('alerterr','Sai định dạng');
                }
                return redirect()->back()->with('alertsuc','Thêm thành công');
            }           
            $this->coupon->storeCoupon($data);
            return redirect()->back()->with('alertsuc','Thêm thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['coupon'] = $this->coupon->getCoupon($id);
        return view('admin.module.coupons.coupon-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $now1  = Carbon::now('Asia/Ho_Chi_Minh');
            $now2  = Carbon::now('Asia/Ho_Chi_Minh')->addDay();
            if($req->loai == 0)
            {
                if( $req->begin==null || $req->end==null)
                {
                    $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $now1,
                            'end'      => $now2,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => $req->tongtien,
                            );
                    try {
                        $add = $this->coupon->updateCoupon($data,$id); 
                    }catch (QueryException $exception) {
                        return back()->with('alerterr','Sai định dạng');
                    }
                    return redirect()->route('coupon.index')->with('alertsuc','Cập nhật thành công');
                }
                elseif ($req->begin>$req->end) {
                return redirect()->back()->with('alerterr','Vui lòng nhập ngày bắt đầu lớn hơn ngày kết thúc');
                }
                else{
                    $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $req->begin,
                            'end'      => $req->end,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => $req->tongtien,
                        );
                    try {
                        $add = $this->coupon->updateCoupon($data,$id); 
                    }catch (QueryException $exception) {
                        return back()->with('alerterr','Sai định dạng');
                    }
                    return redirect()->route('coupon.index')->with('alertsuc','Cập nhật thành công');
                }
                
            }else{
                if( $req->begin==null || $req->end==null)
                {
                    $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $now1,
                            'end'      => $now2,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => 0,
                            );
                    try {
                        $add = $this->coupon->updateCoupon($data,$id); 
                    }catch (QueryException $exception) {
                        return back()->with('alerterr','Sai định dạng');
                    }
                    return redirect()->route('coupon.index')->with('alertsuc','Cập nhật thành công');
                }
                elseif ($req->begin>$req->end) {
                return redirect()->back()->with('alerterr','Vui lòng nhập ngày bắt đầu lớn hơn ngày kết thúc');
                }
                else{
                    $data = array(
                            'id'       => $req->id,
                            'loai'     => $req->loai,
                            'begin'    => $req->begin,
                            'end'      => $req->end,
                            'money'    => $req->money,
                            'donvi'    => $req->donvi,
                            'tongtien' => 0,
                        );
                    try {
                        $add = $this->coupon->updateCoupon($data,$id); 
                    }catch (QueryException $exception) {
                        return back()->with('alerterr','Sai định dạng');
                    }
                    return redirect()->route('coupon.index')->with('alertsuc','Cập nhật thành công');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->coupon->destroyCoupon($id);
            }
            return redirect()->route('coupon.index')->with('alertsuc','Xóa thành công');
        }
    }
}
