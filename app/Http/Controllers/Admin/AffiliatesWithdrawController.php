<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AffiliatesWithdraw;
use App\Model\Member;
use Illuminate\Database\QueryException;
use DateTime;

class AffiliatesWithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $affWithdraw;
    private $member;

    public function __construct(AffiliatesWithdraw $affWithdraw, Member $member)
    {
        $this->affWithdraw = $affWithdraw;
        $this->member = $member;
    }
    public function index()
    {
        $data['withdrawList'] = $this->affWithdraw->getData();
        return view('admin.module.affiliates-withdraw.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.module.ds_affiliates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['withdraw'] = $this->affWithdraw->getDetail($id);
        return view('admin.module.affiliates-withdraw.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $record = $this->affWithdraw->getDetail($id);
        $credit_aff = $request->credit_aff;
        $amount = $request->amount;
        $status = $request->status;
        $note = $request->note;
        if($record->status != $status) {
            if($status == 1) {
                $credit_aff -= $amount;
            }
            else {
                $credit_aff += $amount;
            }
        }
        $data = array(
                        'amount'   => $amount,
                        'status'   => $status,
                        'note'     => $note
                    ); 
        try {
            $this->affWithdraw->AffWithdrawUpdate($data,$id);

            $data_member = array(
                'credit_aff'     => $credit_aff
            );
            $uid = $record->aff_id;
            $this->member->AffUpdate($data_member,$uid);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        } 
        return redirect()->back()->with('alertsuc','Cập nhật thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
