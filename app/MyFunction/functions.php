<?php
use Carbon\Carbon;
function nameLogin()
{
    if(Auth::guard('member')->check())
    {
        $result = Auth::guard('member')->user();
        return $result;
    }
    return redirect()->route('trangchu.getLogout');
}
function select_hinhthuc($id)
{
    $result = "";
    if(isset($id)){
        $result = \DB::table('skl_hinhthuc')->where('id',$id)->first();
        return $result;
    }
    return $result;
}
function select_vanchuyen($gia)
{
    $result = "";
    if(isset($gia)){
        $result = \DB::table('skl_vanchuyen')->where('gia',$gia)->first();
        return $result;
    }
    return $result;
}
function select_order_email()
{
    if(Auth::guard('member')->check())
    {
        $email = Auth::guard('member')->user()->email;
        $result = \DB::table('skl_order')->where('email',$email)->get()->toArray();
        return $result;
    }
    return redirect()->route('trangchu.getLogout');
}

function select_order_member($token)
{
    $result = \DB::table('skl_order')->where('token',$token)->first();
    return $result;
}

function select_products_for_order($id)
{
    $result = \DB::table('skl_sanpham')->where('id',$id)->first()->img;
    return $result;
}
function select_orderdetail($id_order)
{
    $result = \DB::table('skl_orderdetail')->where('id_order',$id_order)->orderBy('ten_sp','ASC')->get()->toArray();
    return $result;
}
function getCauhinh($name)
{
    $result = \DB::table('skl_config') ->select('chitiet')
                                        ->whereId($name)
                                        ->get()
                                        ->first()->chitiet;
    return $result;

}
function select_tintuc_to_category_child_limit($id_category)
{
    $result = \DB::table('skl_tintuc')->where('category',$id_category)->orderBy('created_at','ASC')->paginate(4);
    return $result;
}
function select_tintuc()
{
    $result = \DB::table('skl_tintuc')->orderBy('created_at','ASC')->get()->toArray();
    return $result;
}
function select_categoty_parent()
{
    $result = \DB::table('skl_dmsp')->where('id_parent',0)->orderBy('vitri','ASC')->get()->toArray();
    return $result;
}
function select_categoty_child($id_parent)
{
    if($id_parent==-1){
        $result = \DB::table('skl_dmsp')->where('id_parent','>',0)->orderBy('vitri','ASC')->get()->toArray();
        return $result;
    }
    else
    {
        $result = \DB::table('skl_dmsp')->where('id_parent',$id_parent)->orderBy('vitri','ASC')->get()->toArray();
        return $result;
    }
}
function select_products_to_category_child($id)
{
    $result = \DB::table('skl_sanpham')->where('category',$id)->orderBy('created_at','ASC')->paginate(6);
    return $result;
}
function select_products_to_category_child_limit($id)
{
    $result = \DB::table('skl_sanpham')->where('category',$id)->orderBy('created_at','ASC')->paginate(4);
    return $result;
}
function select_products_to_category_parent($id)
{
    $dataChild = \DB::table('skl_dmsp')->where('id_parent',$id)->orWhere('id',$id)->orderBy('created_at','ASC')->get()->toArray();
    // foreach ($dataChild as $key) {
    //     $result = $key->id;
    // }
    $condition = "";
    $counter = 0;
    foreach ($dataChild as $key) {
        if (++$counter == count($dataChild))
        {
            $condition .= "category=".$key->id;
        } else {
            $condition .= "category=".$key->id." OR ";
        }
    }
    $select = "select * from skl_sanpham where $condition";
    $result = DB::select($select);
    return $result;
}
function select_products_to_category_parent_limit($id,$start,$end)
{
    $dataChild = \DB::table('skl_dmsp')->where('id_parent',$id)->orWhere('id',$id)->orderBy('created_at','ASC')->get()->toArray();
    // foreach ($dataChild as $key) {
    //     $result = $key->id;
    // }
    $condition = "";
    $counter = 0;
    foreach ($dataChild as $key) {
        if (++$counter == count($dataChild))
        {
            $condition .= "category=".$key->id;
        } else {
            $condition .= "category=".$key->id." OR ";
        }
    }
    $select = "select * from skl_sanpham where $condition limit $start,$end";
    $result = DB::select($select);
    return $result;
}
function getProvince($id)
{
    if($id != -1)
    {
        $data = \DB::table('skl_province')->where('provinceid',$id)->first();
        return $data->type." ".$data->name;
    }
    else{
        $data="";
        return $data;
    }
}
function getDistrict($id)
{
    if($id != -1)
    {
        $data = \DB::table('skl_district')->where('districtid',$id)->first();
        return " ".$data->type." ".$data->name." ";
    }
    else{
        $data="";
        return $data;
    }
}
function getWard($id)
{
    if($id != -1)
    {
        $data = \DB::table('skl_ward')->where('wardid',$id)->first();
        return $data->type." ".$data->name;
    }
    else{
        $data="";
        return $data;
    }
}
function resize($width, $height, $tmp_name, $ext, $type,$path_upload){
    /* Get original image x y*/
    list($w, $h) = getimagesize($tmp_name);
    /* calculate new image size with ratio */
    $ratio = max($width/$w, $height/$h);
    $h = ceil($height / $ratio);
    $x = ($w - $width / $ratio) / 2;
    $w = ceil($width / $ratio);
    /* new file name */
    $path = $path_upload.md5(rand().now()).".".$ext;
    /* read binary data from image file */
    $imgString = file_get_contents($tmp_name);
    /* create image from string */
    $image = imagecreatefromstring($imgString);
    $tmp = imagecreatetruecolor($width, $height);
    if($type == 'image/png') {
        imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
        imagealphablending($tmp, false);
        imagesavealpha($tmp, true);
    }
    imagecopyresampled($tmp, $image,
        0, 0,
        $x, 0,
        $width, $height,
        $w, $h);
    /* Save image */
    switch ($type) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 70);
                break;
            case 'image/png':
                imagepng($tmp, $path, 9);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
    return $path;
    /* cleanup memory */
    imagedestroy($image);
    imagedestroy($tmp);
}
function process_upload($f,$path_upload){
    $res = "0";
// settings
    $max_file_size = 20480*20480;
    $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
    if ($_SERVER['REQUEST_METHOD'] == 'POST' AND isset($f)) {
    //Get image height width
        $image_info = getimagesize($f["tmp_name"]);
        $image_width = $image_info[0];
        $image_height = $image_info[1];
    //Tuy chinh kich co hinh
        if($image_width>=4000 || $image_height>=4000)
        {
            $sizes = array(($image_width/6.5) => ($image_height/6.5));
        }
        else if($image_width>=3000 || $image_height>=3000)
        {
            $sizes = array(($image_width/5) => ($image_height/5));
        }
        else if($image_width>=2000 || $image_height>=2000)
        {
            $sizes = array(($image_width/3.3) => ($image_height/3.3));
        }
        else if($image_width>=1000 || $image_height>=1000)
        {
            $sizes = array(($image_width/1.6) => ($image_height/1.6));
        }
        else {
            $sizes = array(($image_width) => ($image_height));
        }
        if( $f['size'] < $max_file_size ){
        // get file extension
            $ext = strtolower(pathinfo($f['name'], PATHINFO_EXTENSION));
            if (in_array($ext, $valid_exts)) {
                /* resize image */
                foreach ($sizes as $w => $h) {
                    $files[] = resize($w, $h,$f['tmp_name'],$ext,$f['type'],$path_upload);
                    $res = str_replace($path_upload,"",$files[0]);
                }
            } else {
                $res = '0';
            }
        } else{
            $res = '0';
        }
    }
    return $res;
}
function convert_array_image($data) {
        $arr = [];
        for ($i = 0; $i<count($data["name"]); $i++) {
            $arr_item = array(  "name" => $data["name"][$i],
                                "type" => $data["type"][$i],
                                "tmp_name" => $data["tmp_name"][$i],
                                "error" => $data["error"][$i],
                                "size" => $data["size"][$i]);
            array_push($arr, $arr_item);
        }
        return $arr;
}
function chuyen($str) {
// In thường
     $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
     $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
     $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
     $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
     $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
     $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
     $str = preg_replace("/(đ)/", 'd', $str);
// In đậm
     $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
     $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
     $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
     $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
     $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
     $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
     $str = preg_replace("/(Đ)/", 'D', $str);
     $str = str_replace("/","-",$str);
     $str = str_replace(",","-",$str);
     $str = str_replace(".","-",$str);
     $str = str_replace("+","-",$str);
     $str = str_replace("-","-",$str);
     $str = str_replace("(","-",$str);
     $str = str_replace(")","-",$str);
     $str = str_replace("?","-",$str);
     $str = str_replace("<","-",$str);
     $str = str_replace(">","-",$str);
     $str = str_replace("=","-",$str);
     $str = str_replace("_","-",$str);
     $str = str_replace("%","-",$str);
     $str = str_replace("^","-",$str);
     $str = str_replace("!","-",$str);
     $str = str_replace("@","-",$str);
     $str = str_replace("#","-",$str);
     $str = str_replace("`","-",$str);
     $str = str_replace("'","-",$str);
     $str = str_replace("$","-",$str);
     $str = str_replace("*","-",$str);
     $str = str_replace("|","-",$str);
     $str = str_replace("]","-",$str);
     $str = str_replace("[","-",$str);
     $str = str_replace("{","-",$str);
     $str = str_replace("}","-",$str);
     $str = str_replace('"','-',$str);
     $str = str_replace(";","-",$str);
     $str = str_replace(":","-",$str);
     $str = str_replace("~","-",$str);
     $str = str_replace(" ","-",$str);
     return $str; // Trả về chuỗi đã chuyển
}
function convertdate($time)
{
    $date = Carbon::parse($time)->format('d/m/Y h:i'); // now date is a carbon instance
    return $date;
}
function convertdate1($time)
{
    $date = Carbon::parse($time); // now date is a carbon instance
    $now  = Carbon::now('Asia/Ho_Chi_Minh');
    return $date->diffForHumans($now);
}
function convertdate2($batdau,$ketthuc)
{
    //  $now = Carbon::now();
    // echo $dt->diffForHumans($now); //12 phút trước
    // $dayAfter = (new DateTime('2014-07-10'));
    $now   = Carbon::today();
    // $now1   = Carbon::now('Asia/Ho_Chi_Minh');
    $date1 = Carbon::parse($batdau);
    $date2 = Carbon::parse($ketthuc);
    if($date1==$now && $date2==$now)
    {
        return "1 ngày";
    }
    elseif ($date1>$now && $date1>$now) 
    {
        return "Chưa đến thời gian";
    }
    elseif( $now>$date1 && $now<$date2 )
    {
        $cv1 = Carbon::parse($date1);
        $cv2 = Carbon::parse($date2);
        return $cv2->diffInDays($now)." ngày";
    }
    elseif( $now==$date1 && $now<$date2 )
    {
        $cv1 = Carbon::parse($date1);
        $cv2 = Carbon::parse($date2);
        return $cv2->diffInDays($cv1)." ngày";
    }
    elseif( $now>$date1 && $now==$date2 )
    {
        $cv1 = Carbon::parse($date1);
        $cv2 = Carbon::parse($date2);
        return "1 ngày";
    }
    elseif( $date1>$now && $date1>$now )
    {
        $cv1 = Carbon::parse($date1);
        $cv2 = Carbon::parse($date2);
        return $cv2->diffInDays($now1)." ngày";
    }
    else
    {
        echo "Hết hạn";
    }
    
}
function convert_mobile_to_call($phone_number) {
    $phone_number = str_replace(".","",$phone_number);
    $phone_number = str_replace("+","",$phone_number);
    echo $phone_number." -";
    $arr_phone = explode("", $phone_number);
    $arr_phone[0] = preg_replace('/0/', '84', $arr_phone[0]);
    $phone_number = implode("", $arr_phone);
}
function convert_money($data) {
    return number_format($data,0,',','.');
}
?>