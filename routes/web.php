<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('Frontend')->group(function(){
	Route::get('/','HomeController@index')->name('trangchu.index');
	Route::get('trang-chu','HomeController@index')->name('trangchu.index');
	Route::get('danh-muc-san-pham/{alias}','HomeController@getPages')->name('trangchu.getPages');
	Route::get('danh-muc-nho/{alias}','HomeController@getPagesCon')->name('trangchu.getPagesCon');
	Route::get('chi-tiet-san-pham/{alias}','HomeController@getDetail')->name('trangchu.detail');
	Route::get('chi-tiet-tin-tuc/{alias}','HomeController@getTintucDetail')->name('trangchu.tintucdetail');
	Route::get('gioi-thieu','HomeController@Gioithieu')->name('trangchu.gioithieu');
	Route::get('lien-he','HomeController@Lienhe')->name('trangchu.lienhe');
	Route::post('lien-he/create','HomeController@store')->name('trangchu.store');

	Route::get('trang-chu/dang-nhap','LoginController@getLogin')->name('trangchu.getLogin');
	Route::post('trang-chu/dang-nhap','LoginController@postLogin')->name('trangchu.postLogin');
	Route::get('trang-chu/dang-ky','LoginController@getRegister')->name('trangchu.getRegister');
	Route::post('trang-chu/dang-ky','LoginController@postRegister')->name('trangchu.postRegister');
	Route::get('dang-xuat','LoginController@getLogout')->name('trangchu.getLogout');
	Route::get('dang-xuat2','LoginController@getLogout2')->name('trangchu.getLogout2');
	Route::get('thong-tin-ca-nhan', 'LoginController@getInformation')->name('trangchu.getInformation');
	Route::get('thay-doi-dia-chi', 'LoginController@getAddress')->name('trangchu.getAddress');

	Route::get('tim-mat-khau', 'ResetPasswordController@getTimMatKhau')->name('trangchu.getTimMatKhau');
	Route::post('tim-mat-khau', 'ResetPasswordController@sendEmail')->name('trangchu.sendEmail');
	Route::get('mat-khau-moi', 'ResetPasswordController@getNewPassword')->name('trangchu.getNewPassword');
	Route::post('mat-khau-moi', 'ResetPasswordController@postNewPassword')->name('trangchu.postNewPassword');

	Route::get('them-san-pham/{id}','ShoppingCartController@addProductToCart')->name('get.add.product');
	Route::get('gio-hang-cua-ban','ShoppingCartController@listCartProduct')->name('get.list.cart');
	Route::get('so-luong','ShoppingCartController@updownQtyProduct')->name('updown.product');
	Route::get('xoa-san-pham/{id}','ShoppingCartController@detroyProductCart')->name('get.delete.product');
	Route::get('xoa-tat-ca','ShoppingCartController@detroyAllProductCart')->name('get.delete.all.product');
	Route::get('xem-lai-hoa-don', 'ShoppingCartController@getRepostCart')->name('get.RepostCart');
	Route::get('dat-hang', 'ShoppingCartController@getDathang')->name('get.Dathang');
	Route::post('dat-hang', 'ShoppingCartController@postDathang')->name('post.Dathang');
	Route::get('/datacart', 'ShoppingCartController@getCart');

}); 

Route::namespace('Frontend')->prefix('member')->group(function(){
	Route::get('/trang-chu', 'MemberController@dashboard_page')->name('member.dashboard');
	Route::get('/cap-nhat-thong-tin', 'MemberController@information_page')->name('member.information');
	Route::get('/doi-mat-khau', 'MemberController@password_page')->name('member.password');
	Route::get('/doi-email', 'MemberController@email_page')->name('member.email');
	Route::get('/doi-email-buoc-2', 'MemberController@email_step_2_page')->name('member.email-step-2');
	Route::get('/don-hang', 'MemberController@order_page')->name('member.order');
	Route::get('/hoa-hong-cap-1', 'MemberController@aff_lv1_page')->name('member.affiliate-lv1');
	Route::get('/hoa-hong-cap-2', 'MemberController@aff_lv2_page')->name('member.affiliate-lv2');
	Route::get('/don-hang-hoa-hong-cap-1', 'MemberController@order_aff_lv1_page')->name('member.order-affiliate-lv1');
	Route::get('/don-hang-hoa-hong-cap-2', 'MemberController@order_aff_lv2_page')->name('member.order-affiliate-lv2');
	Route::get('/rut-tien', 'MemberController@aff_withdraw_page')->name('member.affiliate-withdraw');
	Route::get('/lich-su-rut-tien', 'MemberController@aff_withdraw_history_page')->name('member.affiliate-withdraw-history');
	Route::get('/thanh-toan-credit-aff/{id}', 'MemberController@payment_aff')->name('member.payment_aff');

	Route::post('/cap-nhat-thong-tin', 'MemberController@update_information')->name('member.update_information');
	Route::post('/doi-mat-khau', 'MemberController@change_password')->name('member.change_password');
	Route::post('/doi-email-buoc-1', 'MemberController@change_email_step_1')->name('member.change_email_step_1');
	Route::post('/doi-email-buoc-2', 'MemberController@change_email_step_2')->name('member.change_email_step_2');
	Route::post('/rut-tien', 'MemberController@aff_withdraw_credit')->name('member.aff_withdraw_credit');

});

Route::namespace('Admin')->group(function(){
	Route::get('logins','LoginController@getLogin')->middleware('loginal')->name('admin.getLogin');
	Route::post('logins','LoginController@postLogin')->name('admin.postLogin');;
	Route::get('logout','LoginController@getLogout')->name('admin.getLogout');
});



Route::namespace('Admin')->middleware('logins')->prefix('admin')->group(function(){
	Route::resource('/','HomeController');
	Route::resource('sanpham','SanphamController');
	Route::resource('useradmin','UseradminController');
	Route::resource('member','MemberController');
	Route::resource('tintuc','TintucController');
	Route::resource('dmtt','DmttController');
	Route::resource('dmsp','DmspController');
	Route::resource('slide','SlideController');
	Route::resource('nhanxet','NhanxetController');
	Route::resource('coupon','CouponController');
	Route::resource('hinhthuc','HinhthucController');
	Route::resource('cauhinh','CauhinhController');
	Route::resource('lienhe','LienheController');
	Route::resource('vanchuyen','VanchuyenController');
	Route::resource('donhang','DonhangController');
	Route::resource('affiliates','AffiliatesController');
	Route::resource('affiliates-withdraw','AffiliatesWithdrawController');
	Route::get('cauhinh','CauhinhController@editNoId')->name('cauhinh.editNoId');
	Route::put('cauhinh','CauhinhController@updateNoId')->name('cauhinh.updateNoId');
	Route::get('in-hoa-don','DonhangController@getInhoadon')->name('hoadon.getInhoadon');
	Route::post('updatedata','DonhangController@updatedata');
	Route::get('cauhinh-affiliates','CauhinhAffiliatesController@editAffiliatesNoId')->name('cauhinh-affiliates.editAffiliatesNoId');
	Route::put('cauhinh-affiliates','CauhinhAffiliatesController@updateAffiliatesNoId')->name('cauhinh-affiliates.updateAffiliatesNoId');
	// Route::post('/update_data/{id}', 'DonhangController@update_data')->name('donhang.update_data');

});

