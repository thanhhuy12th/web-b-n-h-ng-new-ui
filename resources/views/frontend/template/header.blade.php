<div id="header">
    <div class="row">
        <div class="col-lg-3 col-sm-12">
            <a href="{{route('trangchu.index')}}">
                <img style="padding-top:5px" src="{{ asset('image/'.getCauhinh('photos')) }}" id="logo" alt="LOGO"/>
            </a>
            <p></p>
        </div>
        <div class="col-lg-4 col-sm-12">
            <center><label style=" font-size: 30px; padding-top: 20px;"><?php echo getCauhinh("tenwebsite"); ?></label></center>
            <form action="./product-category" style="margin-top:5px">
            <div class="form-row">
                        <input type="text" class="form-control" placeholder="Tìm kiếm" aria-label="Recipient's username" aria-describedby="button-addon2" style="width: 70%">
                        <input type="hidden" name="post_type" value="post">
                        <button type="submit" id="btn-search" class="bt btn-success" style="width: 20%"><i id="searchicon" class="fa fa-search"></i></button>
                </div>
                </form>
        </div>
        <div class="col-lg-5 col-sm-12">
            <div class="form-row1">
                <span>Hotline: <a href="tel:<?php echo getCauhinh("dienthoai"); ?>"><?php echo getCauhinh("dienthoai"); ?></a></span>
            </div>
            <div class="form-row2">
                <div id="box1" class="col">
                    <div class="l"><img id="minilg" src="{{ asset('image/img/giaohang.png') }}" style="height: 40px"></div>
                    <div class="r">
                        <a href="#"> <b>Giao Hàng</b>
                            <br> Trên toàn quốc </a>
                    </div>
                </div>
                <div id="box2" class="col">
                    <div class="l"><img id="minilg" src="{{ asset('image/img/hotline.png') }}" style="height: 40px"></div>
                    <div class="r">
                        <a href=""> <b><?php echo getCauhinh("dienthoai"); ?></b>
                        <br> Bảo hành chính hãng </a>
                    </div>
                </div>
                <div id="box3" class="col">
                    <div class="l"><img id="minilg" src="{{ asset('image/img/thanhtoan.png') }}" style="height: 40px"></div>
                    <div class="r">
                        <a href="#"> <b>Thanh toán</b>
                        <br> Khi nhận hàng </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--header-->