<nav class="navbar navbar-expand-lg  sticky-top" style=" padding: 0 .5rem;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><i class="fas fa-align-justify"></i></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('trangchu.index')}}">Trang chủ</a>
                    </li>
                    <?php 
                     $data_category = select_categoty_parent();
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Danh mục
                        </a>
                        <div class="dropdown-menu sub-danhmuc" aria-labelledby="navbarDropdown">
                        @foreach($data_category as $dc)
                            <a class="dropdown-item" href="{{route('trangchu.getPages', ['alias' => $dc->alias])}}">{{$dc->name}}</a>
                        @endforeach
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('trangchu.gioithieu')}}">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('trangchu.lienhe')}}">Liên hệ</a>
                    </li>                  
                </div>
                @if(Cart::count()>0)
                    <button type="button" class="btn pull-right btnpullright">
                        <div class="product-thumbnail">
                            <div>
                                <a class="nav-link"  href="{{ route('get.list.cart') }}" ><i class="fas fa-shopping-basket" style="font-size: 20px"></i></a>
                            </div>
                        <span class="product-thumbnail__quantity" aria-hidden="true">{{Cart::count()}}</span>
                    </div>
                    </button>
                @else
                    <button type="button" class="btn pull-right btnpullright">
                        <div class="product-thumbnail">
                            <div>
                                <a class="nav-link"  href="{{ route('get.list.cart') }}" ><i class="fas fa-shopping-basket" style="font-size: 20px"></i></a>
                            </div>
                        <span class="product-thumbnail__quantity" aria-hidden="true">0</span>
                    </div>
                    </button>
                @endif
                @if(Auth::guard('member')->check())
                    <button type="button" class="btn pull-right btnpullright">
                        <a class="nav-link" href="{{route('member.dashboard')}}"><i class="fas fa-heart"></i> {{nameLogin()->username}}</a>
                    </button>
                    <button type="button" class="btn pull-right btnpullright">
                        <a class="nav-link" href="{{route('trangchu.getLogout')}}"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a>
                    </button>                
                    @else
                    <button type="button" class="btn btnpullright">
                        <a class="nav-link" href="{{route('trangchu.getLogin')}}"><i class="fas fa-heart"></i> Login</a>
                    </button>
                    <button type="button" class="btn pull-right btnpullright">
                        <a class="nav-link" href="{{route('trangchu.getRegister')}}"><i class="fas fa-sign-in-alt"></i> Sign In</a>
                    </button>
                    @endif
            </li>
        </ul>
    </div>
</nav>
 