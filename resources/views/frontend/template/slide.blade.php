<div id="slides" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 carousel slide" data-ride="carousel" style="max-width: 100%;">
    <ul class="carousel-indicators">
        @foreach($slide as $sld)
	        	<li data-target="#slides" data-slide-to="{{$sld['vitri']}}" class=" {{ $sld['vitri'] == 0 ? "active" : "" }}"></li>
	    @endforeach
    </ul>
    <div class="carousel-inner">
       	@foreach($slide as $sld)
	        <div class="carousel-item {{  $sld['vitri'] == 0 ? "active" : "" }} thumbnail-c1-slider" >
	            <img class="d-block w-100" src="{{ asset('image/'.$sld['img']) }}" alt="{!!$sld['chitiet']!!}">
	        </div>
        @endforeach
	</div>
	
</div>

