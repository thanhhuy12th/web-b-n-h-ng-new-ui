
</style>
<div class="header-menu">
    <h5 style="color:#35cc7b">
        <img src="{{ asset('frontend/asset/img/list.png') }}" width="24px" height="24px">&nbsp;<strong> {{trans('message.danhmuc_sanpham')}}</strong>
    </h5>
</div>
<div class="side-row menu2" style="width: 100%">
    <ul>
        <?php 
            $data_category_child = select_categoty_child(-1)
        ?>
        @foreach($data_category_child as $prt)
            <li><a href="{{route('trangchu.getPagesCon', ['alias' => $prt->alias])}}">
                <img src="{{ asset('frontend/asset/img/right-arrow.png') }}" width="16px" height="16px" />
                {{$prt->name}}
                </a>
            </li>
        @endforeach
    </ul>
</div><!--Menuphai-->