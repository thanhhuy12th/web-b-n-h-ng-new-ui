@extends('frontend.welcome')
@section('content')
<div class="row jumbotron" style="padding-top:1em">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" style="background: #fff;padding:10px 20px;" >
        <div class="row" id="title">
            <div class="col-lg-12 col-mg-12 col-sm-12 col-xs-12">
                <h2 class="product-name"><a href="#">{{$idcha->name}}</a></h2>
            </div>
        </div>
        <div class="product-list">
            <div class="row">
                <?php
                    $row_per_page=9;
                    $alias = $idcha->id;
                    $select_about=select_products_to_category_parent($alias);
                    $rows = count($select_about);
                    if ($rows>$row_per_page) $page=ceil($rows/$row_per_page);
                    else $page=1;
                    if(isset($_GET['start']) && (int)$_GET['start'])
                        $start=$_GET['start'];
                    else
                        $start=0;
                    $select_about=select_products_to_category_parent_limit($alias,$start,$row_per_page);
                ?>
                @foreach($select_about as $dt)
                    <div id="box-product1" class="col-lg-4 col-sm-12 col-12">
                        <div style="position: relative;">
                            @php $arr_img = explode("||",$dt->img);@endphp
                            <img src="{{ asset('image/'.$arr_img[0]) }}" alt="{{$dt->tensp}}" class="image-prod">
                            <div class="overlay">
                                {{$dt->chitietngan}}
                            </div>
                        </div>
                        <h5>{{$dt->tensp}}</h5>
                        <div class="price-area">
                            <del><span class="price">{{convert_money($dt->price)}} đ</span></del> &nbsp;&nbsp;
                            <span class="price discount">{{convert_money($dt->price_km)}} đ</span>
                        </div>
                        <div class="circles" style="padding-top: 8px" >
                            <?php echo ceil((($dt->price-$dt->price_km)/$dt->price)*100)."%"; ?>
                        </div>
                        <p><a href="{{route('trangchu.detail', ['alias' => $dt->alias])}}" style="text-decoration: none; color: #ffffff"><button>Xem chi tiết</button></a></p>
                    </div>
                @endforeach
                    <row class="col-md-12">
                        <hr>
                        <div >
                            <center>
                            <?php
                                if(isset($idcha->id)) {
                                    $page_cr=($start/$row_per_page)+1;
                                    echo "<ul class='pagi'>";
                                    for($i=1;$i<=$page;$i++)
                                    {
                                        if ($page_cr!=$i) echo "<li class='btn-group' style='margin-right:10px'>"."<span class='pagi-item' ><a href='".$idcha->alias."?start=".$row_per_page*($i-1)."'>$i&nbsp;</a></span>"."</li>";
                                        else echo "<li class='btn-group' style='margin-right:10px'><span class='pagi-item active'>".$i."</span></li>";
                                    }
                                    echo "</ul>";
                                }
                                else {
                                    $page_cr=($start/$row_per_page)+1;
                                    echo "<ul class='pagi'>";
                                    for($i=1;$i<=$page;$i++)
                                    {
                                        if ($page_cr!=$i) echo "<li class='btn-group' style='margin-right:10px'>"."<span class='pagi-item'><a href='/?start=".$row_per_page*($i-1)."'>$i&nbsp;</a></span>"."</li>";
                                        else echo "<li class='btn-group' style='margin-right:10px'><span class='pagi-item active'>".$i."</span></li>";
                                    }
                                    echo "</ul>";
                                }
                            ?>
                            </center>
                        </div>
                    </row>
            </div>
        </div>
    </div>
    @include('frontend.template.menupages')
</div>
@endsection