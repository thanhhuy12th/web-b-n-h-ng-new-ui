@extends('frontend.welcome')

@section('content')
    
<div class="row jumbotron " style="padding-top:1em">
    <div class="row product-detail" style="background-color:#ffffff; padding:15px 40px; text-align: justify;    width: 100%;" >
        <div class="col-lg-4 col-md-4 sm-12 col-12" >
            @php $arr_img = explode("||",$sanpham->img);@endphp
            <img id="myImg" src="{{ asset('image/'.$arr_img[0]) }}" style="width:100%" alt="{{$sanpham->tensp}}">
            <!-- The Modal -->
            <div id="myModal" class="modal">
              <span class="close">&times;</span>
              <img class="modal-content" id="img01">
              <div id="caption"></div>
          </div>
      </div>
      <div class="col-lg-8 col-md-8 sm-12 col-12">
        <h4 class="detail-title"><span style="font-weight: 400;">{{$sanpham->tensp}}</span></h4>
        <div class="suk" style="width: 100%;">
            <div> 
                Mã sản phẩm : <span style="color: #3DAD49; text-transform: uppercase;">{{$sanpham->id}}</span>
            </div>
            <div>  
                Tình Trạng :<span style="color: #3DAD49; text-transform: uppercase;"> Còn Hàng</span> 
            </div>
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 no-padding-mobile" >
                    <p class="detail-price"><span>Giá:{{convert_money($sanpham->price_km)}}<span class="cur cur-layout2"> ₫</span></p>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 no-padding-mobile" >
                    <p class="old_price"><del>Giá cũ:{{convert_money($sanpham->price)}}<span class="cur cur-layout2"> ₫</span></del></p>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 no-padding-mobile" >
                    <p class="saving"><span>Tiết kiệm:</span> {{convert_money($sanpham->price - $sanpham->price_km)}}<span class="cur cur-layout2"> ₫</span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 no-padding-mobile" >
                    <a class="order-this-now" data-name="" href="{{ route('get.add.product',$sanpham->id) }}">
                        <span class="p1">MUA NGAY</span><span class="p2" style="font-size: 12px">(Giao hàng toàn quốc)</span>
                    </a>
                </div>
                <div class="col-md-6 no-padding-mobile" >
                    <a class="support" href="https://website.cloudone.vn/bachkhoa/tel:0913 942 113"> 
                        <span class="p1">{!!getCauhinh('dienthoai')!!}</span><span class="p2">(Miễn phí ship)</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="short-des">
            <p style="font-size: 14px; padding: 10px;">{!!$sanpham->chitietngan!!}</p> 
        </div> 
        <br>
        <center><button class="btn btn-danger" style="font-size: 1.3em" id="btn-text">Liên hệ ngay {!!getCauhinh('dienthoai')!!} để được tư vấn</button></center>       
    </div>
</div>
<div class="row" style="padding-top: 2em;width: 100%" >
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9"style="background-color:#ffffff; padding:15px 40px; text-align: justify;">
        {!!$sanpham->information!!}


    </div>
    @include('frontend.template.menupages')
</div>
<div class="row" style="padding-top:1em;width: 100%">
    <div class="product-list" style="width: 100%">
        <h2 class="product-name"><a href="#">Sản phẩm khác</a></h2>
        <div class="row">
            <?php
                $data = select_products_to_category_child_limit($sanpham->category);
            ?>
            @foreach($data as $dm)
                <div style="background: white" id="box-product1" class="col-lg-3 col-sm-12 col-12">
                    <div style="position: relative;">
                        @php $arr_img = explode("||",$dm->img);@endphp
                        <img src="{{ asset('image/'.$arr_img[0]) }}" alt="" class="image-prod" >
                        <div class="overlay">
                            {!!$dm->chitietngan!!}
                        </div>
                    </div>
                    <h5>{{$dm->tensp}}</h5>
                    <div class="price-area">
                        <del><span class="price">{{convert_money($dm->price)}} đ</span></del> &nbsp;&nbsp; 
                        <span class="price discount">{{convert_money($dm->price_km)}} đ</span>
                    </div>
                    <div class="circles" style="padding-top: 8px" >
                        <?php echo ceil((($dm->price-$dm->price_km)/$dm->price)*100)."%"; ?>
                    </div>
                    <p><a href="{{route('trangchu.detail', ['alias' => $dm->alias])}}" style="text-decoration: none; color: #ffffff"><button>Xem chi tiết</button></a></p>
                </div>
            @endforeach
        </div>
    </div>
</div>
</div>

<script>
        // Get the modal
        var modal = document.getElementById("myModal");
        
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById("myImg");
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
          modal.style.display = "block";
          modalImg.src = this.src;
          captionText.innerHTML = this.alt;
      }
      
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() { 
          modal.style.display = "none";
      }
</script><!--Zoom img-->
<!--  -->
@endsection