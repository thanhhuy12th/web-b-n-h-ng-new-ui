@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                		<div class="card-header header-elements-inline">
	                			<h5 class="card-title">Danh sách giới thiệu cấp 1</h5>
	                		</div>
	                		<div class="card-body">
	                			<table class="table">
	                				<thead>
	                					<tr>
	                						<th scope="col">Họ tên</th>
	                						<th scope="col">Điện thoại</th>
	                						<th scope="col">Email</th>
	                						<th scope="col">Thời gian</th>
	                					</tr>
	                				</thead>
	                				<tbody>
	                					@foreach($listRef as $item)
	                					<tr>
	                						<td>{{$item->fullname}}</td>
	                						<td>{{$item->phone}}</td>
	                						<td>{{$item->email}}</td>
	                						<td>{{$item->created_at}}</td>
	                					</tr>
	                					@endforeach
	                				</tbody>
	                			</table>
	                			{{$listRef->links()}}
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>AffLv1</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection