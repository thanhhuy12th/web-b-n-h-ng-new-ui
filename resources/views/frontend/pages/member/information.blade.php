@extends('frontend.welcome')
@section('content')
<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
        <div class="d-lg-flex justify-content-lg-between">
            @include('frontend.pages.member.components.menu')
            {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
            <div class="content-wrapper">
                <div class="d-none d-md-block">
                    <div class="content">
                        <div class="card">
                            <form action="{{route('member.update_information')}}" method="post" style="background-color: #fff;padding: 10px">
                                @csrf 
                                <div class="row form-group">
                                    <div class="form-group row col-lg-12" style="border-left: 3px solid #35cc7b;border-bottom: 1px solid #35cc7b;margin-bottom: 20px;color: #35cc7b;margin-top: 5px">
                                        <h5>Cập nhật thông tin cá nhân</h5>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <p><label>Họ và tên: </label><input value="{{nameLogin()->fullname}}" type="text" name="fullname" class="form-control" placeholder="Họ và tên"></p>
                                        <p><label>Email: </label><input value="{{nameLogin()->email}}" type="text" name="email" class="form-control" placeholder="123456789@gmail.com" disabled=""></p>
                                        <p><label>Số điện thoại: </label><input value="{{nameLogin()->phone}}" type="text" name="phone" class="form-control" placeholder="0859529539"></p>
                                        <p><label>Tên hiển thị: </label><input value="{{nameLogin()->username}}" type="text" name="username" class="form-control" placeholder="123 K HCM"></p>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        {{-- 
                                        <div class="row col-lg-12" style="border-left: 3px solid #35cc7b;border-bottom: 1px solid #35cc7b;margin-bottom: 20px;color: #35cc7b">
                                            <h5>Cập nhật địa chỉ</h5>
                                        </div>
                                        --}}
                                        <p><label>Địa chỉ giao hàng </label><input value="{{nameLogin()->diachi}}" type="text" name="diachi" class="form-control" placeholder="123 K"></p>
                                        <p><label>Tỉnh / Thành phố </label>
                                            <input type="hidden" id="idprovince1" value="{{nameLogin()->province}}">
                                            <select class="form-control custom-select-value" name="province" id="province">
                                            </select>
                                        </p>
                                        <p><label>Quận / Huyện: </label>
                                            <input type="hidden" id="iddistrict1" value="{{nameLogin()->district}}">
                                            <select class="form-control custom-select-value" name="district" id="district">
                                            </select>
                                        </p>
                                        <p><label>Phường / Xã: </label>
                                            <input type="hidden" id="idward1" value="{{nameLogin()->ward}}">
                                            <select class="form-control custom-select-value" name="ward" id="ward">
                                            </select>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: center;">
                                    <button name="btnLuuThongTin" type="submit" class="btn btn-primary" >Lưu thông tin</button>
                                </div>
                                <input type="hidden" id="tongtienmua" value="{{nameLogin()->tongtienmua}}">
                                <input type="hidden" id="vipmoney" value="{{getCauhinh('vipmoney')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
            <div class="d-md-none">
                <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
                    <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
                    <i class="icon-paragraph-justify3"></i>
                    </button>
                </nav>
                <div class="content-wrapper" style="margin-top: -40px">
                    <div class="content">
                        <div class="card">
                            <h1>AffLv1</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection