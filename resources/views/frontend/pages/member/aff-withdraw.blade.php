@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                		<form action="{{route('member.aff_withdraw_credit')}}" method="POST">
	                			@csrf
		                    	<ul class="nav nav-tabs">
	            		 			<li class="nav-item">
	            		 				<a class="nav-link active" data-toggle="tab" href="#aff-lv1">Rút tiền</a>
	            		 			</li>
	            		 			<li class="nav-item">
	            		 				<a class="nav-link" data-toggle="tab" href="#aff-lv2">Chuyển khoản</a>
	            		 			</li>
	            		 		</ul>
	            		 		<div class="tab-content">
	            		 			<div class="tab-pane container active form" id="aff-lv1">
            		 					<div class="form-group" style="margin-top: 10px">
            		 						<label>Số tiền: </label>
            		 						<input class="form-control" type="text" name="chuyentien" placeholder="Nhập số tiền cần chuyển sang tài khoản mua hàng">
            		 					</div>
            		 					<div style="text-align: center;">
            		 						<button type="submit" name="btnmuahang" class="btn btn-primary">Gửi</button>
            		 					</div>
	            		 			</div>
	            		 			<div class="tab-pane container fade" id="aff-lv2">
	            		 				<div class="form-group" style="margin-top: 10px">
            		 						<label>Số tiền: </label>
            		 						<input class="form-control" type="text" name="chuyenkhoan" placeholder="Nhập số tiền chuyển khoản">
            		 					</div>
            		 					<div class="form-group" style="margin-top: 10px">
            		 						<label>Ghi chú: </label>
            		 						<textarea class="form-control" name="noidung" placeholder="Nhập ghi chú"></textarea>
            		 					</div>
            		 					<div style="text-align: center;">
            		 						<button type="submit" name="btnnganhang" class="btn btn-primary">Gửi</button>
            		 					</div>
	            		 			</div>
	            		 		</div>
	            		 	</form>
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection