@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                    	

							<div class="card-header header-elements-inline">
	                			<h5 class="card-title">Danh sách hoa hồng LV2</h5>
	                		</div>
	                		<div class="card-body">
	                			<table class="table">
	                				<thead>
	                					<tr>
	                						<th>Email</th>
	                						<th>Họ tên</th>
	                						<th>Giới thiệu bởi</th>
	                						<th>Tên sản phẩm</th>
	                						<th>Trị giá</th>
	                						<th>Hoa hồng</th>
	                						<th>Thời gian</th>
	                					</tr>
	                				</thead>
	                				<tbody>
	                					@foreach($listRefOrder as $item)
	                					<tr>
	                						<td>{{$item->email}}</td>
	                						<td>{{$item->hoten}}</td>
	                						<td>{{$item->fullname_ref}}</td>
	                						<td>{{$item->ten_sp}} X {{$item->soluong}}</td>
	                						<td>{{number_format($item->price_km)}}</td>
	                						<td>{{number_format((($item->price_km) * $percent_lv2)/100)}}</td>
	                						<td>{{$item->created_at}}</td>
	                						
	                					</tr>
	                					@endforeach
	                				</tbody>
	                			</table>
	                			{{$listRefOrder->links()}}
	                		</div>



	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>OrderAffLv1</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection