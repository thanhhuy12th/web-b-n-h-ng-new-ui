@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                    	<div class="row">
	                    		<div class="col-md-4">
	                    			<div class="bg-teal-400 row" style=" margin-right: 0px; margin-left: 0px; border-radius: 3px;">
	                    				<div class="col-4"><i style="font-size: 5rem;" class="icon-piggy-bank dashboard-icon"></i></div>
	                    				<div class="col-8" style="padding-top: 6px">
	                    					<p>Tài Khoản</p>
	                    					<span style="font-size: 1.5rem">{{convert_money($member->credit)}} đ</span>
	                    				</div>
	                    			</div>
	                    		</div>
	                    		<div class="col-md-4">
	                    			<div class="bg-teal-401 row" style=" margin-right: 0px; margin-left: 0px;border-radius: 3px;">
	                    				<div class="col-4"><i style="font-size: 5rem;" class="icon-bag dashboard-icon"></i></div>
	                    				<div class="col-8" style="padding-top: 6px">
	                    					<p>Đơn hàng</p>
	                    					<span style="font-size: 1.5rem">{{$totalOrder}}</span>
	                    				</div>
	                    			</div>
	                    		</div>
	                    		<div class="col-md-4">
	                    			<div class="bg-teal-402 row" style=" margin-right: 0px; margin-left: 0px;border-radius: 3px;">
	                    				<div class="col-4"><i style="font-size: 5rem;" class="icon-user-tie dashboard-icon"></i></div>
	                    				<div class="col-8" style="padding-top: 6px">
	                    					<p>Giới thiệu</p>
	                    					<span style="font-size: 1.5rem">{{$totalMember}}</span>
	                    				</div>
	                    			</div>
	                    		</div>	                    		
	                    	</div>
	                    	<div class="row">
	                    		 <div class="col-md-12">
	                    		 	<div class="card">
	                    		 		<div class="card-header header-elements-inline">
	                    		 			<h5 class="card-title">Danh sách 5 đơn hàng mới nhất</h5>
	                    		 		</div>
	                    		 		<div class="card-body over-y-content-member">
	                    		 			<table class="table">
	                    		 				<thead>
	                    		 					<tr>
	                    		 						<th>Mã ĐH</th>
	                    		 						<th>Họ tên</th>
	                    		 						<th>Điện thoại</th>
	                    		 						<th>Trị giá</th>
	                    		 						<th>Thời gian</th>
	                    		 						<th>Trạng thái</th>
	                    		 						<th>Thanh toán</th>
	                    		 					</tr>
	                    		 				</thead>
	                    		 				<tbody>
	                    		 					@foreach($orderAll as $item)
	                		 							<tr>
	                		 								<td>{{$item->id}}</td>
					                						<td>{{$item->hoten}}</td>
					                						<td>{{$item->sodt}}</td>
					                						<td>{{number_format($item->tongtien)}}</td>
					                						<td>{{convertdate($item->created_at)}}</td>
					                						<td>{!!($item->status_paid == 0 ? '<span class="nlabel-danger">Chưa thanh toán</span>' : '<span class="nlabel-success">Hoàn thành</span>')!!}</td>
					                						<td>
					                							@if($item->status_paid == 0)
					                							<center><a onClick="return checkCreditAff('Xác nhận thanh toán ?');" href="{{route('member.payment_aff',['id'=>$item->id])}}"><i class="fa fa-money fa-member-table-icon orange"></i></a></center>
					                							@else
					                							<center><i  class="fa fa-check fa-member-table-icon green"></i></center>
					                							@endif
					                						</td>
					                					</tr>
                		 							@endforeach
	                    		 				</tbody>
	                    		 			</table>
	                    		 		</div>
	                    		 	</div>
	                    		 </div>
	                    	</div>
	                    	<div class="row">
	                    		<div class="col-md-12">
	                    		 	<div class="card">
	                    		 		<div class="card-header header-elements-inline">
	                    		 			<h5 class="card-title">Danh sách 5 giới thiệu mới nhất</h5>
	                    		 		</div>
	                    		 		<p></p>
	                    		 		<ul class="nav nav-tabs">
	                    		 			<li class="nav-item">
	                    		 				<a class="nav-link active" data-toggle="tab" href="#aff-lv1">Giới thiệu cấp 1</a>
	                    		 			</li>
	                    		 			<li class="nav-item">
	                    		 				<a class="nav-link" data-toggle="tab" href="#aff-lv2">Giới thiệu cấp 2</a>
	                    		 			</li>
	                    		 		</ul>
	                    		 		<div class="tab-content">
	                    		 			<div class="tab-pane container active" id="aff-lv1">
	                    		 				<div class="card-body over-y-content-member">
	                    		 					<table class="table">
	                    		 						<thead>
	                    		 							<tr>
	                    		 								<th scope="col">Mã</th>
	                    		 								<th scope="col">Họ tên</th>
	                    		 								<th scope="col">Điện thoại</th>
	                    		 								<th scope="col">Email</th>
	                    		 								<th scope="col">Thời gian</th>
	                    		 							</tr>
	                    		 						</thead>
	                    		 						<tbody>
	                    		 							@foreach($memberLv1 as $item)
	                    		 							<tr>
	                    		 								<td>{{$item->id}}</td>
	                    		 								<td>{{$item->fullname}}</td>
	                    		 								<td>{{$item->phone}}</td>
	                    		 								<td>{{$item->email}}</td>
	                    		 								<td>{{convertdate($item->created_at)}}</td>
	                    		 							</tr>
	                    		 							@endforeach
	                    		 						</tbody>
	                    		 					</table>
	                    		 				</div>
	                    		 			</div>
	                    		 			<div class="tab-pane container fade" id="aff-lv2">
	                    		 				<div class="card-body over-y-content-member">
	                    		 					<table class="table">
	                    		 						<thead>
	                    		 							<tr>
	                    		 								<th scope="col">Mã</th>
	                    		 								<th scope="col">Họ tên</th>
	                    		 								<th scope="col">Giới thiệu bởi</th>
	                    		 								<th scope="col">Điện thoại</th>
	                    		 								<th scope="col">Email</th>
	                    		 								<th scope="col">Thời gian</th>
	                    		 							</tr>
	                    		 						</thead>
	                    		 						<tbody>
	                    		 							@foreach($memberLv2 as $item)
	                    		 							<tr>
	                    		 								<td>{{$item->id}}</td>
	                    		 								<td>{{$item->fullname}}</td>
	                    		 								<td>{{$item->fullname_ref}}</td>
	                    		 								<td>{{$item->phone}}</td>
	                    		 								<td>{{$item->email}}</td>
	                    		 								<td>{{convertdate($item->created_at)}}</td>
	                    		 							</tr>
	                    		 							@endforeach
	                    		 						</tbody>
	                    		 					</table>
	                    		 				</div>
	                    		 			</div>
	                    		 		</div>
	                    		 		
	                    		 	</div>
	                    		</div>
	                    	</div>
	                    	<div class="row">
	                    		<div class="col-md-12">
	                    		 	<div class="card">
	                    		 		<div class="card-header header-elements-inline">
	                    		 			<h5 class="card-title">Danh sách 5 đơn hàng từ giới thiệu gần nhất</h5>
	                    		 		</div>
	                    		 		<p></p>
	                    		 		<ul class="nav nav-tabs">
	                    		 			<li class="nav-item">
	                    		 				<a class="nav-link active" data-toggle="tab" href="#order-aff-lv1">Đơn hàng cấp 1</a>
	                    		 			</li>
	                    		 			<li class="nav-item">
	                    		 				<a class="nav-link" data-toggle="tab" href="#order-aff-lv2">Đơn hàng cấp 2</a>
	                    		 			</li>
	                    		 		</ul>
	                    		 		<div class="tab-content">
	                    		 			<div class="tab-pane container active" id="order-aff-lv1">
	                    		 				<div class="card-body over-y-content-member">
	                    		 					<table class="table">
	                    		 						<thead>
	                    		 							<tr>
	                    		 								<th scope="col">Mã</th>
				                                                <th scope="col">Họ tên</th>
				                                                <th scope="col">Mặt hàng</th>
				                                                <th scope="col">Số tiền</th>
				                                                <th scope="col">Hoa hồng</th>
				                                                <th scope="col">Điện thoại</th>
				                                                <th scope="col">Thời gian</th>
	                    		 							</tr>
	                    		 						</thead>
	                    		 						<tbody>
	                    		 							@foreach($orderLv1 as $item)
	                    		 							<tr>
	                    		 								<td>{{$item->id}}</td>
						                						<td>{{$item->hoten}}</td>
						                						<td>{{$item->ten_sp}} X {{$item->soluong}}</td>
						                						<td>{{number_format($item->price_km)}}</td>
						                						<td>{{number_format((($item->price_km) * $percent_lv1)/100)}}</td>
						                						<td>{{$item->sodt}}</td>
						                						<td>{{convertdate($item->created_at)}}</td>
						                					</tr>
	                    		 							@endforeach
	                    		 						</tbody>
	                    		 					</table>
	                    		 				</div>
	                    		 			</div>
	                    		 			<div class="tab-pane container fade" id="order-aff-lv2">
	                    		 				<div class="card-body over-y-content-member">
	                    		 					<table class="table">
	                    		 						<thead>
	                    		 							<tr>
	                    		 								<th scope="col">Mã</th>
				                                                <th scope="col">Họ tên</th>
				                                                <th scope="col">Mặt hàng</th>
				                                                <th scope="col">Số tiền</th>
				                                                <th scope="col">Hoa hồng</th>
				                                                <th scope="col">Điện thoại</th>
				                                                <th scope="col">Thời gian</th>
	                    		 							</tr>
	                    		 						</thead>
	                    		 						<tbody>
	                    		 							@foreach($orderLv2 as $item)
	                    		 							<tr>
	                    		 								<td>{{$item->id}}</td>
						                						<td>{{$item->hoten}}</td>
						                						<td>{{$item->ten_sp}} X {{$item->soluong}}</td>
						                						<td>{{number_format($item->price_km)}}</td>
						                						<td>{{number_format((($item->price_km) * $percent_lv2)/100)}}</td>
						                						<td>{{$item->sodt}}</td>
						                						<td>{{convertdate($item->created_at)}}</td>
						                					</tr>
	                    		 							@endforeach
	                    		 						</tbody>
	                    		 					</table>
	                    		 				</div>
	                    		 			</div>
	                    		 		</div>
	                    		 		
	                    		 	</div>
	                    		</div>
	                    	</div>
	                	</div>


	                    	
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>dashboard</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection