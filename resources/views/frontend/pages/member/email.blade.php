@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                		<form action="{{route('member.change_email_step_1')}}" method="POST" style="background: #ffffff">
                			@csrf
                			{{method_field('POST')}}
	                			<div class="row">
	                				<div class="col-md-4">
	                					<img src="{{asset('frontend/asset/img/guard.jpg')}}" style="width: 100%">
	                				</div>
	                				<div class="col-md-8">
	                					<h4 style="text-align: center; font-size: 16px; margin-top:3rem;">Để bảo vệ bảo mật tài khoản của bạn, chúng tôi cần xác minh danh tính của bạn
	                						<br>
	                						<br> Vui lòng chọn cách xác minh:
	                					</h4>
	                					<br>
	                					<button type="submit" name="btnSendMail" class="btn btn-primary" style="width: 75%; margin-left:12.5%; background-color: #35cc7b; color: #ffffff; border:#35cc7b ">
	                						<i style="font-size: 25px" class="fa fa-envelope">&ensp;</i>Xác minh qua Email
	                					</button>
	                				</div>
	                			</div>
                			</form>
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>AffLv1</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection