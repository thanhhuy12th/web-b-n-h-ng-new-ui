<div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
        <i class="icon-arrow-left8"></i>
        </a>
        <a href="#" class="sidebar-mobile-expand">
        <i class="icon-screen-full"></i>
        <i class="icon-screen-normal"></i>
        </a>
    </div>
    <div class="sidebar-content">
        <div class="card-sidebar-mobile">
            <div class="card-body p-0">
                {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
                <div class="d-none d-md-block">
                    <div class="navbar navbar-expand-md navbar-dark" style="padding-left: 20px">
                        <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                        </a>
                    </div>
                    <ul class="nav nav-sidebar" data-nav-type="accordion">
                        <li class="nav-item ">
                            <a href="{{route('member.dashboard')}}" class="nav-link" >
                                <i class="icon-home4"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-user-tie"></i> <span>Thông tin cá nhân</span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="Rút tiền" style="display: none">
                                <li class="nav-item">
                                    <a href="{{route('member.information')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Cập nhật</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('member.password')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Đổi mật khẩu</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('member.email')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Đổi email</a>
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item ">
                            <a href="{{route('member.order')}}" class="nav-link">
                                <i class="icon-home4"></i>
                                <span>Đơn hàng</span>
                            </a>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-list3"></i> <span>Affiliates</span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="Danh sách" style="display: none">
                                <li class="nav-item">
                                    <a href="{{route('member.affiliate-lv1')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Danh sách cấp 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('member.affiliate-lv2')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Danh sách cấp 2</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('member.order-affiliate-lv1')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Đơn hàng HH cấp 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('member.order-affiliate-lv2')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Đơn hàng HH cấp 2</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-cash"></i> <span>Rút tiền</span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="Rút tiền" style="display: none">
                                <li class="nav-item">
                                    <a href="{{route('member.affiliate-withdraw')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Phương thức</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('member.affiliate-withdraw-history')}}" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Lịch sử</a>
                                </li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
                {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
                <div class="d-md-none">
                    <div class="navbar navbar-expand-md navbar-dark" style="padding-left: 20px">
                        <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                        </a>
                    </div>
                    <ul class="nav nav-sidebar" data-nav-type="accordion">
                        <li class="nav-item ">
                            <a href="#dash-board-mb" class="nav-link active" data-toggle="tab">
                            <i class="icon-home4"></i>
                            <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-list3"></i> <span>Danh sách</span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="Starter kit" style="display: none">
                                <li class="nav-item">
                                    <a href="#khach-hang-lvl-1-mb" data-toggle="tab" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Khách hàng lvl 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#khach-hang-lvl-2-mb" data-toggle="tab" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Khách hàng lvl 2</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#don-hang-lvl-1-mb" data-toggle="tab" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Đơn hàng lvl 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#don-hang-lvl-2-mb" data-toggle="tab" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Đơn hàng lvl 2</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-item-submenu">
                            <a href="#" class="nav-link"><i class="icon-cash    "></i> <span>Rút tiền</span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="Starter kit" style="display: none">
                                <li class="nav-item">
                                    <a href="#rut-tien-mb" data-toggle="tab" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Phương thức</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#lich-su-mb" data-toggle="tab" class="nav-link" style="padding: 0.625rem 0.25rem 0.625rem 3.5rem;">Lịch sử</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>