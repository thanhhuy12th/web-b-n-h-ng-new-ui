@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                		<h5 style="padding-top: 5px"><i style="font-size: 25px;color: #35cc7b" class="fa fa-envelope">&ensp;</i>Chúng tôi đã gửi một mã xác minh về email của bạn.</h5>
	                		<div class="row">
	                			<div class="col-md-4">
	                				<img src="{{asset('frontend/asset/img/guard.jpg')}}" style="width: 100%">
	                			</div>
	                			<div class="col-md-8">
	                				<form action="{{route('member.change_email_step_2')}}" method="POST" >
	                					@csrf   
	                					<div class="form-group" style="margin-top: 1.5rem">
	                						<label>Nhập Email đăng ký mới:</label>
	                						<input type="text" required="" name="email" placeholder="123456@gmail.com" class="form-control input-transparent" style="width: 75%">
	                					</div>
	                					<div class="form-group">
	                						<label>Nhập mã xác minh:</label>
	                						<input type="number" name="maxacminh" placeholder="Gồm 6 chữ số" class="form-control input-transparent" style="width: 75%">
	                					</div>
	                					<button onClick="return xacnhanxoa('Bạn sẽ chuyển sang trang đăng nhập');" type="submit" name="btnSaveChange"class="btn btn-primary" style="margin-left: 25%;width: 50%; background-color: #35cc7b; color: #ffffff; border:#35cc7b ">
	                						Xác nhận
	                					</button>
	                				</form>
	                			</div>
	                		</div> 
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>AffLv1</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection