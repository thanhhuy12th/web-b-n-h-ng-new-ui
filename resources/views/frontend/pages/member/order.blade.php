@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                		<div class="card-header header-elements-inline">
	                			<h5 class="card-title">Danh sách các đơn hàng đã đặt</h5>
	                		</div>
	                		<div class="card-body">
	                			<table class="table">
	                				<thead>
	                					<tr>
	                						<th>Tên sản phẩm</th>
	                						<th>Trị giá</th>
	                						<th>Thời gian</th>
	                						<th>Thanh toán</th>
	                						<th>Vận chuyển</th>
	                						<th>Thao tác</th>
	                					</tr>
	                				</thead>
	                				<tbody>
	                					@foreach($listOrder as $item)
	                					<tr>
	                						<td>{{$item->ten_sp}} X {{$item->soluong}}</td>
	                						<td>{{number_format($item->price)}}</td>
	                						<td>{{$item->created_at}}</td>
	                						<td>
	                							{!!
	                								($item->status_paid == 1) 
	                								? '<span class="nlabel-success">Đã thanh toán</span>'
	                								: '<span class="nlabel-danger">Chưa thanh toán</span>'
	                							!!}
	                							
	                						</td>
	                						<td>
	                							{!!
	                								$item->status_move == 1 
	                								? '<span class="nlabel-success">Đang vận chuyển</span>'
	                								: '<span class="nlabel-danger">Chưa xử lý</span>'
	                							!!}
	                							
	                						</td>
	                						<td>
	                							{!!
	                								$item->status_paid == 1 
	                								? '<center><i class="fa fa-check fa-member-table-icon green"></i></center>'
	                								: '<center><i class="fa fa-money fa-member-table-icon orange"></i></center>'
	                							!!}
	                							
	                						</td>
	                					</tr>
	                					@endforeach
	                				</tbody>
	                			</table>
	                			{{$listOrder->links()}}
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>OrderPage</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection