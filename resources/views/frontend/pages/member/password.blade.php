@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 40px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    <div class="card-body" style="padding: 0px">
	    <div class="d-lg-flex justify-content-lg-between">
	        @include('frontend.pages.member.components.menu')
	        {{-- +++++++++++++++++++Content PC+++++++++++++++++++ --}}
	        <div class="content-wrapper">
	            <div class="d-none d-md-block">
	                <div class="content">
	                	<div class="card">
	                		<form method="post" action="{{route('member.change_password')}}" style="background-color: #fff;padding: 10px">
	                			@csrf
	                			<div class="form-group">
	                				<label class="control-label" for="passwordnew">Mật khẩu cũ</label>
	                				<input type="password" name="passwordold" class="form-control input-transparent">
	                			</div>
	                			<div class="form-group">
	                				<label class="control-label" for="passwordnew">Mật khẩu mới</label>
	                				<input type="password" name="passwordnew" class="form-control input-transparent">
	                			</div>
	                			<div class="form-group">
	                				<label class="control-label" for="repeatpasswordnew">Xác nhận mật khẩu mới</label>
	                				<input type="password" name="repeatpasswordnew" class="form-control input-transparent">
	                			</div>
	                			<button style="margin-right: 5px" class="btn btn-primary" type="submit" name="btnLuuMatKhau">Lưu thay đổi</button>
	                			<button class="btn btn-danger" type="reset">Nhập lại</button>
	                		</form>
	                	</div>
	                </div>
	            </div>
	        </div>
	        {{-- +++++++++++++++++++Content MB+++++++++++++++++++ --}}
	        <div class="d-md-none">
	            <nav class="navbar navbar-expand-lg  sticky-top"style="position: sticky;left: 100%;top: 50%;z-index: 1;width: max-content">
	                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button" style="padding: 0px;cursor: pointer">
	                <i class="icon-paragraph-justify3"></i>
	                </button>
	            </nav>
	            <div class="content-wrapper" style="margin-top: -40px">
	                <div class="content">
	                	<div class="card">
	                    	<h1>password</h1>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
    
</div>
@endsection