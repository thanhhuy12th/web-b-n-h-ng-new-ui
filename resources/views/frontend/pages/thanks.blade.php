<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    @include('frontend.template.head')
    <link rel="stylesheet" href="{{ asset('frontend/asset/thankyou_v2.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset/checkout2.css') }}">
</head>
<body class="thanksbody" style="background-color: #e9ecef">
    <nav class="navbar">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10"><a class="navbar-brand" href="{{route('trangchu.index')}}"><h2 style=" color: #ffffff; margin-bottom: 0px; font-size: 18px;">{{getCauhinh('tenwebsite')}}</h2></a></div>
        </div>
    </nav>
    <div class ="wrap clearfix">
        <div class="row thanks-infos jumbotron" style="margin-top: -20px">
            <?php 
                $hoadon = select_order_member($token);
            ?>
            <div class="col-md-5 col-sm-12 order-info" define="{order_expand: false}" style="background: #fafafa;color: #777;border: 1px solid #dadada;">
                <div class="order-summary order-summary--custom-background-color ">
                    <div class="order-summary-header summary-header--thin summary-header--border">
                        <h3 style="font-weight: bold;"><label>Đơn hàng</label> #{{$hoadon->id}}</h3>
                    </div>
                    <div class="order-items" >
                        <div class="summary-body summary-section summary-product">
                            <div class="summary-product-list">
                                <ul class="product-list" id="listsanpham">
                                    <?php 
                                        $sanpham = select_orderdetail($hoadon->id);
                                    ?>
                                    @foreach($sanpham as $sp)
                                    <?php
                                        $img = select_products_for_order($sp->id_sanpham)
                                    ?>
                                    <li class="product product-has-image clearfix" style="list-style-type:none;">
                                        <div class="product-thumbnail pull-left">
                                            <div class="product-thumbnail__wrapper">
                                                @php $arr_img = explode("||",$img);@endphp
                                                <img class="product-thumbnail__image" src="{{ asset('image/'.$arr_img[0]) }}" style="width: 75px; height: 75px;">
                                            </div>
                                            <span class="product-thumbnail__quantity" aria-hidden="true"> {{$sp->soluong}}</span>
                                        </div>
                                        <div class="product-info pull-left">
                                            <span class="product-info-name"><strong> {{$sp->ten_sp}}</strong></span>
                                            <span class="product-info-description"></span>
                                        </div>
                                        <strong class="product-price pull-right" id="gia_33">{{convert_money($sp->price_km)}} VNĐ</strong>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="summary-section  border-top-none--mobile ">
                        <div class="total-line total-line-subtotal clearfix">
                            <span class="total-line-name pull-left">
                                Tạm tính
                            </span>
                            <span id="tamtinh" class="total-line-subprice pull-right">{{convert_money($hoadon->tongtien-$hoadon->phivanchuyen)}} VNĐ</span>
                        </div>
                        
                    </div>
                    <div class="summary-section">
                        <div class="total-line total-line-shipping clearfix" id="phivanchuyen2">
                            <span class="total-line-name pull-left">
                                Phí vận chuyển
                            </span>
                            <span class="pull-right" id="phi">
                                {{convert_money($hoadon->phivanchuyen)}} VNĐ
                            </span>
                        </div>
                        <div class="total-line total-line-total clearfix">
                            <span class="total-line-name total-line-name--bold pull-left">
                                Tổng cộng
                            </span>
                            <span id="total" class="total-line-price pull-right">{{convert_money($hoadon->tongtien)}} VNĐ</span>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-md-7">
                <div class="thankyou-message row" style="padding-top: 5px;background: #fafafa;color: #777;border: 1px solid #dadada;margin-left: 15px;margin-right: 15px">
                    <div class="thankyou-message-icon unprint col-md-2">
                        <div class="iconcheck icon--order-success svg">
                            <svg xmlns="http://www.w3.org/2000/svg" width="72px" height="72px">
                                <g fill="none" stroke="#8EC343" stroke-width="2">
                                    <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                    <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                                </g>
                            </svg>
                        </div>
                    </div> 
                    <div class="thankyou-message-text col-md-9" style="margin-top:5px">
                        <h3 style="color: #333;font-size: 18px;font-weight: 400;">Cảm ơn bạn đã đặt hàng</h3>
                            <p style="margin: 6px 0">
                                Một email xác nhận đã được gửi tới <span id="email" style="font-weight: bold;"> {{$hoadon->email}}.</span> Xin vui lòng kiểm tra email của bạn
                            </p>
                        <div style="font-style: italic;"></div>
                    </div>       
                </div>
                <div class="col-sm-12 customer-info">
                    <div class="shipping-info" style="background: #fafafa;color: #777;border: 1px solid #dadada;">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="order-summary order-summary--white no-border no-padding-top">
                                    <div class="order-summary-header">
                                        <h3 style="font-weight: bold;">
                                            Thông tin nhận hàng
                                        </h3>
                                    </div>
                                    <div class="summary-section no-border no-padding-top">
                                        <p class="address-name">{{$hoadon->hoten}}</p>
                                        <p class="address-address">{{$hoadon->diachi}}</p>
                                        <p class="address-district"><?php echo getWard($hoadon->ward); ?><?php echo getDistrict($hoadon->district); ?></p>
                                        <p class="address-province"><?php echo getProvince($hoadon->province); ?></p>
                                        <p class="address-phone">{{$hoadon->sodt}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="order-summary order-summary--white no-border">
                                    <div class="order-summary-header">
                                        <h3 style="font-weight: bold;">
                                            Thông tin thanh toán
                                        </h3>
                                    </div>
                                    <div class="summary-section no-border no-padding-top">
                                        <p class="address-name">{{$hoadon->hoten}}</p>
                                        <p class="address-address">{{$hoadon->diachi}}</p>
                                        <p class="address-district"><?php echo getWard($hoadon->ward); ?><?php echo getDistrict($hoadon->district); ?></p>
                                        <p class="address-province"><?php echo getProvince($hoadon->province); ?></p>
                                        <p class="address-phone">{{$hoadon->sodt}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="order-summary order-summary--white no-border">
                                    <div class="order-summary-header">
                                        <h3 style="font-weight: bold;">
                                            Hình thức thanh toán
                                        </h3>
                                    </div>
                                    <div class="summary-section no-border no-padding-top">
                                        <span>{{select_hinhthuc($hoadon->hinhthuc)->name}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>