<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    @include('frontend.template.head')
</head>
<body>
    <!-- header -->
    @include('frontend.template.header')
    @include('frontend.template.navigation')
<div class="jumbotron">
    @if(Cart::count()>0)
    @include('admin.blocks.alert')
    <table class="table table-condensed ordertbl">
        <thead>
            <tr style="background-color: #fff;">
                <th>Hình Ảnh</th>
                <th>Tên Sản Phẩm</th>
                <th>Giá Gốc</th>
                <th>Giá Khuyến Mãi</th>
                <th></th>
                <th>Thành Tiền</th>
                <th>Thay Đổi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr style="background-color: #fff">
                    @php $arr_img = explode("||",$product->options->img);@endphp
                    <td><img style="border-radius: 7px" width="100" height="100" src="{{ asset('image/'.$arr_img[0]) }}"></td>
                    <td style="padding-top:35px;">{{ $product->name }}</td>
                    <td style="padding-top:35px;">{{ convert_money($product->options->price_goc) }} đ</td>
                    <td style="padding-top:35px;">{{ convert_money($product->price) }} đ</td>
                    <td style="padding-top:25px;">
                        <a class="btn btn-success" style="background: #2aca74;width: 15%" href='{{url("so-luong?product_id=$product->id&decrease=1")}}'> - </a>
                        <a class="btn" style="color: #000;background: #FFF">Số lượng: {{ $product->qty }}</a>
                        
                        <a class="btn btn-success" style="background: #2aca74;width: 15%" href='{{url("so-luong?product_id=$product->id&increment=1")}}'> + </a>
                    </td>
                    <td style="padding-top:35px;">{{convert_money($product->price*$product->qty)}} đ</td>
                    <td style="padding-top:30px;"><a style="color:#08c; font-size: 18px;" href="{{route('get.delete.product',$product->id)}}">Xóa</a>
                    </td>
                </tr>
            @endforeach
            <tr style="background-color: #fff;margin-right: 5px">
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td style="font-weight: bold"> Tổng Thanh Toán</td>
                <td > {{convert_money($tongtien)}} VNĐ</td>
                <td> </td>
            </tr>
        </tbody>
    </table>
    <form action="{{ route('get.Dathang') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
         d
        <div class="row">
            <div class="col-md-6"></div>
                <div class="col-md-2">
                    <a href="{{ route('get.delete.all.product') }}"><input type="button" value="Xóa Tất Cả" class="btn btn-info" style="background: #269abc;color:white; float: right"></a>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('trangchu.index') }}" type="addProduct" class="btn btn-primary" style="background: #5cb85c;color:white; float: right">Tiếp Tục Mua Hàng</a>
                </div>
                <div class="col-md-2">
                    <button name="sendMail" type="submit" class="btn btn-success" style="background: #4674a7; float: right">ĐẶT HÀNG NGAY</button>
                </div>
        </div>
    </form>
    @else
    <div class="row">
        
        <div class="col-md-12" style="text-align: center;">
            <h4 >Không có sản phẩm nào trong giỏ hàng</h4>
            <a href="{{ route('trangchu.index') }}" type="addProduct" class="btn btn-primary" style="background: #5cb85c;color:white;">Tiếp Tục Mua Hàng</a>
        </div>
    </div>
    @endif
</div>
    @include('frontend.template.footer')
    <!-- Script website -->
    @include('frontend.template.footer_script')
</body>
</html>
