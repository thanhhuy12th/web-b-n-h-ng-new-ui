@extends('frontend.welcome')

@section('content')
<div class="row jumbotron" style="padding-top: 2em" >
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9"style="background-color:#ffffff; padding:25px 20px; text-align: justify;">
        <h3 style="color:#35cc7b;text-align: center;margin-bottom: 20px"><strong>{{$tintuc->title}}</strong></h3>
         @php $arr_img = explode("||",$tintuc->img);@endphp
            <img src="{{ asset('image/'.$arr_img[0]) }}" style="width:100%" alt="{{$tintuc->title}}">
        <div style="margin-top: 20px" class="new-details">
            {!!$tintuc->content!!}
        </div>
        <hr>
        <div class="row" style="padding-top:1em">
            <div class="product-list">
                <h2 class="product-name"><a href="#">Các bài viết khác</a></h2>
                <div class="row">
                    <?php 
                        $danhmuc = select_tintuc_to_category_child_limit($tintuc->category);
                    ?>
                    @foreach($danhmuc as $dm)
                    <ul class="all-posts">
                        <li>
                            <div class="thumbnail col-xl-2">
                                <a href="{{route('trangchu.tintucdetail', ['alias' => $dm->alias])}}">
                                @php $arr_img = explode("||",$dm->img);@endphp
                                <img src="{{ asset('image/'.$arr_img[0]) }}" style="width:100%" alt="{{$dm->title}}">
                                </a>
                            </div>
                            <div class="detail-post col-xl-10">
                                <h2 class="title-post">
                                    <a href="{{route('trangchu.tintucdetail', ['alias' => $dm->alias])}}" style="text-decoration: none; color: #0A0A0A;">{!!$dm->title!!}</a></h2>
                                <div class="content-excerpt">
                                    <p>{!!$dm->intro!!}</p>
                                </div>
                                <a class="read-more" href="{{route('trangchu.tintucdetail', ['alias' => $dm->alias])}}">Xem chi tiết<span>»</span></a>
                            </div>
                        </li>
                    </ul>
                    @endforeach
                </div>
            </div>
        </div>
        

    </div>
    @include('frontend.template.menupages')
</div>
@endsection