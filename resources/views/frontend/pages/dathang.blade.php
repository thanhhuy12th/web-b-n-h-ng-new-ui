<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    @include('frontend.template.head')
    <link rel="stylesheet" href="{{ asset('frontend/asset/checkout.css') }}">
    <!-- notifications CSS
        ============================================ -->
    <link rel="stylesheet" href="{{ asset('frontend/asset_admin/css/notifications/Lobibox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset_admin/css/notifications/notifications.css') }}">
</head>
<body style="background: #e9ecef">
<div class="modal modal-adminpro-general default-popup-PrimaryModal PrimaryModal-bgcolor fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('trangchu.postLogin')}}" method="post" style="margin-top: 0px">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-header modalback">
                    <div class="modal-close-area modal-close-df">
                        <a style="padding: 8px 10px;margin-top: -39px;margin-right: -58px;" class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                    </div>
                    <div class="modal-title"><h3 style="padding-right:125px;">Đăng nhập</h3></div>
                </div>
                <div class="modal-body">
                    <div class="col-md-10" style="margin-top: 10px">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input name="email" type="text" class="form-control" id="input" placeholder="example@gmail.com" style="opacity: 0.5; width: 300px">
                            </div>    
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <input name="password" type="password" class="form-control" id="input" placeholder="Password" style="opacity: 0.5; width: 300px">
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding: 8px" class="modal-footer notification-bt responsive-btn">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                     <button type="submit"   class="btn btn-success" style="height: calc(1.5em + .75rem + 2px);width: max-content">LOGIN  <i class="fas fa-arrow-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<nav class="navbar">
            <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10"><a class="navbar-brand" href="{{route('trangchu.index')}}"><h2 style=" color: #ffffff;padding: 3px">{{getCauhinh('tenwebsite')}}</h2></a></div>
        </div>
    </nav>
    <div class="jumbotron row" style="padding-top: 10px">
        <div class="col-md-4 col-sm-12 order-info1">
            <div class="order-summary order-summary--custom-background-color ">
                <div class="order-summary-header summary-header--thin summary-header--border">
                    <!--  -->
                    <h2><label class="control-label">Chi tiết đơn hàng</label></h2>
                    <a class="underline-none expandable expandable--pull-right mobile unprint" bind-event-click="toggle(this, '.order-items')" bind-class="{open: order_expand}" href="#">
                        Xem chi tiết
                    </a>
                </div>
                <div class="order-items mobile--is-collapsed" bind-class="{'mobile--is-collapsed': !order_expand}">
                    <div class="summary-body summary-section summary-product">
                        <div class="summary-product-list">
                            <ul class="product-list" id="listsanpham">
                                <input type="hidden" id="listca" value="{{$getType}}" >
                                @foreach($listcarts as $cart-)
                                <li class="product product-has-image clearfix" style="list-style-type:none;">
                                    <div class="product-thumbnail pull-left">
                                        <div class="product-thumbnail__wrapper">
                                            @php $arr_img = explode("||",$cart->options->img);@endphp
                                            <img class="product-thumbnail__image" src="{{ asset('image/'.$arr_img[0]) }}">
                                        </div>
                                        <span class="product-thumbnail__quantity" aria-hidden="true">{{$cart->qty}}</span>
                                    </div>
                                    <div class="product-info pull-left">
                                        <span class="product-info-name"><strong>{{$cart->name}}</strong></span>
                                        <span class="product-info-description"></span>
                                    </div>
                                    <strong class="product-price pull-right" id="gia_33">{{convert_money($cart->price)}} VNĐ</strong>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="summary-section" style="padding: 20px 12px 10px 12px;">
                        {{-- <a class="more hidden-xs hidden-sm" id="magiamgia_click">Nhập mã giảm giá</a> --}}
                        {{-- <a class="pull-right" id="btnHoantac" href="#">Hoàn tác</a> --}}
                        <div class="form-group" >
                            <div class="input-group" id="magiamgia">
                                <input id="code" type="text" class="form-control" placeholder="Nhập mã giảm giá ">
                                <span class="input-group-btn" id="btnGiamgia">
                                    <button class="btn btn-primary" id="btnApdung" type="button" style="display: block;">Áp dụng</button>
                                    <button class="btn btn-danger" id="btnHoantac" type="button" disabled="true" style="display: none;">Hoàn tác</button>
                                </span>
                            </div>
                            <div id="thongbao1"></div>
                        </div>
                </div>
                <div class="summary-section  border-top-none--mobile ">
                    <div class="total-line total-line-subtotal clearfix">
                        <span class="total-line-name pull-left">
                            Tạm tính
                        </span>
                        <span id="tamtinh" class="total-line-subprice pull-right">{{convert_money($giatamtinh)}} VNĐ</span>
                    </div>
                </div>
                <div class="summary-section">
                    <div class="total-line total-line-shipping clearfix" style="padding-bottom: 15px;padding-top: 15px">
                        <span class="total-line-name pull-left" >Phí vận chuyển</span>
                        <span id="phi" class="pull-right"></span>
                    </div>
                    <div class="total-line total-line-total clearfix">
                        <span class="total-line-name total-line-name--bold pull-left">
                            Tổng cộng
                        </span>
                        {{-- <div id="luutt">
                            <input type="hidden" id="gettamtinh" value="{{$giatamtinh}}">
                        </div>
                        <input type="hidden" id="gettamtinh1" value="{{$giatamtinh}}">
                        <input type="hidden" id="luuprice">
                        <input type="hidden" id="getGiamgia">
                        <input type="hidden" id="getGiamgiapt"> --}}
                        <span class="pull-right" id="tongtien"></span>
                        {{-- <span class="pull-right" id="tongtien1"></span> --}}
                    </div>
                </div>
            </div>
        </div>
        <form action="{{ route('post.Dathang') }}" method="post" class="col-md-8 col-sm-12s"  >
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12" style="background: #fafafa;color: #777;border: 1px solid #dadada;">
                    <div class="form-group m0" style="margin-top: 10px">
                        @include('admin.blocks.alert')
                        <h2><label class="control-label">Thông tin mua hàng</label></h2>
                    </div>
                    <div class="form-group">
                        @if(Auth::guard('member')->check())
                            <a href="#">{{Auth::guard('member')->user()->fullname}}</a>
                            <span style="padding: 0 5px;">/</span>
                            <a href="{{route('trangchu.getLogout2')}}">Đăng xuất</a>
                        @else
                            <a href="{{route('trangchu.getRegister')}}">Đăng ký tài khoản mua hàng</a>
                            <span style="padding: 0 5px;">/</span>
                            <a data-toggle="modal" data-target="#myModal" href="#">Đăng nhập</a>
                        @endif
                    </div>
                    <hr class="divider">
                    <div class="billing">
                        @if(Auth::guard('member')->check())
                            <div class="form-group">
                                <span>Email</span>
                                <input value="{{Auth::guard('member')->user()->email}}" name="email" type="text" class="form-control" placeholder="Nhập Email bạn hoặc: asd123@gmail.com">
                            </div>
                            <div class="form-group">
                                <span>Họ tên</span>
                                <input value="{{Auth::guard('member')->user()->fullname}}" name="hoten" type="text" class="form-control" placeholder="Họ tên">
                            </div>
                            <div class="form-group">
                                <span>Số điện thoại</span>
                                <input value="{{Auth::guard('member')->user()->phone}}" name="sodt" class="form-control" placeholder="Số điện thoại" >
                            </div>
                            <div class="form-group">
                                <span>Địa chỉ</span>
                                <input value="{{Auth::guard('member')->user()->diachi}}" name="diachi" class="form-control" placeholder="Địa chỉ" >
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="idprovince1" value="{{Auth::guard('member')->user()->province}}">
                                <select class="form-control custom-select-value" name="province" id="province">
                                </select>
                            </div>
                             <div class="form-group">
                                <input type="hidden" id="iddistrict1" value="{{Auth::guard('member')->user()->district}}">
                                <select class="form-control custom-select-value" name="district" id="district">
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="idward1" value="{{Auth::guard('member')->user()->ward}}">
                                <select class="form-control custom-select-value" name="ward" id="ward">
                                </select>
                            </div>
                            <hr class="divider">
                        @else
                            <div class="form-group">
                                <span>Email</span>
                                <input name="email" type="text" class="form-control" placeholder="Nhập Email bạn hoặc: asd123@gmail.com">
                            </div>
                            <div class="form-group">
                                <span>Họ tên</span>
                                <input name="hoten" type="text" class="form-control" placeholder="Họ tên">
                            </div>
                            <div class="form-group">
                                <span>Số điện thoại</span>
                                <input name="sodt" class="form-control" placeholder="Số điện thoại" >
                            </div>
                            <div class="form-group">
                                <span>Địa chỉ nhà</span>
                                <input name="diachi" class="form-control" placeholder="Địa chỉ" >
                            </div>
                            <div class="form-group">
                                <select class="form-control custom-select-value" name="province" id="province">
                                </select>
                            </div>
                             <div class="form-group">
                                <select class="form-control custom-select-value" name="district" id="district">
                                    <option value="-1">Lựa chọn Quận/ Huyện</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control custom-select-value" name="ward" id="ward">
                                    <option value="-1">Lựa chọn Phường/ Xã</option>
                                </select>
                            </div>
                            <hr class="divider">
                        @endif
                    </div>
                    <div class="form-group">
                        <textarea name="ghichu" class="form-control" placeholder="Ghi chú">
                        </textarea>
                    </div>
                    <div id="luutt">
                        <input type="hidden" name="gettamtinh" id="gettamtinh" value="{{$giatamtinh}}">
                    </div>
                    <input type="hidden" id="gettamtinh1" value="{{$giatamtinh}}">
                    <input type="hidden" id="luuprice">
                    <input type="hidden" id="getGiamgia">
                    <input type="hidden" id="getGiamgiapt">
                    <span class="pull-right" id="tongtien"></span>
                    <span class="pull-right" id="tongtien1"></span>
                </div>                
                <div class="col-md-6 col-sm-12" style="background: #fafafa;color: #777;border-top: 1px solid #dadada;border-right: 1px solid #dadada;border-bottom: 1px solid #dadada;">
                    <div class="shipping-method" bind-show="shippingMethods.length > 0" style="margin-bottom: 10px">
                        <div class="form-group">
                            <h2><label class="control-label">Vận chuyển</label></h2>
                            <div class="next-select__wrapper">
                                <select class="form-control next-select" name="phivanchuyen" id="phivanchuyen" style="position: relative;cursor: pointer;">
                                </select>
                            <img src="{{asset('frontend/asset/img/down.png')}}" class="col-md-2" style="right: -88%; margin-top: -50px;width: 14%;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group payment-method-list">
                        <h2><label class="control-label">Thanh toán</label></h2>
                        <div class="next-select__wrapper">
                                <select class="form-control next-select" name="hinhthuc" id="hinhthuc" style="position: relative;cursor: pointer;">
                                    @foreach($hinhthuc as $ht)
                                    <option value="{{$ht->id}}">{{$ht->name}}</option>
                                    @endforeach
                                </select>
                                <img src="{{asset('frontend/asset/img/down.png')}}" class="col-md-2" style="right: -88%; margin-top: -50px ;width: 14%;">
                            </div>
                        <div id="thongtin" class="radio">
                        </div>
                    </div>
                    <div id="donhang">
                    </div>
                    <div class="form-group clearfix hidden-md hidden-lg">
                        <button id="btnDathang" class="btn btn-primary col-md-12 mt10 btn-checkout" type="submit">Đặt hàng</button>
                    </div>
                </div>
            </div>
        </form>  
</div>
<script type="text/javascript">
    $(document).ready(function(){
    $("#thongtin").html(' ');
    $("#hinhthuc").change(function(){
        var hinhthuc = $("#hinhthuc").val();
        if(hinhthuc == 2)
        {
            html1 = '<div><div style="margin: 5px; border: 1px solid #AAAAAA;border-radius: 10px;padding: 10px"><i style="color: #000;font-weight: bold;">A/ Ngân hàng VCB:</i> <br>Số tài khoản: sp61<br>Chủ tài khoản: tên<br>chi nhánh<br><br><i style="color: #000;font-weight: bold;">B/ Ngân hàng ACB</i><br>Số tài khoản: sp61<br>Chủ tài khoản: tên<br>chi nhánh<br><br><i style="color: #000;font-weight: bold;">Lưu ý: Viết rõ họ và tên vào nội dung chuyển khoản.</i></div></div>';
            $("#thongtin").html(html1);
        }
        else
        {
            $("#thongtin").html(' ');
        }
    });

    var url = "./api/vanchuyen";
    $.getJSON(url,function(result){
        $.each(result, function(i, field){
            var id   = field.id;
            var name = field.name;
            var price = field.gia;
            var select = "";
            const formatter = new Intl.NumberFormat('de-DE', {
                  style: 'decimal',
                  minimumFractionDigits: 0
                })
            if(id==1)
            {
                var select     = 'selected=""';
                var gettamtinh = $("#gettamtinh").val();
                var getGiamgia = $("#getGiamgia").text();
                var tong       = parseInt(gettamtinh) + parseInt(price)-getGiamgia;
                var tong1      = parseInt(gettamtinh) + parseInt(price);
                var data       = formatter.format(tong);
                var data1      = formatter.format(tong1);
                $("#phi").html(formatter.format(price) + ' VNĐ');
                $("#donhang").append('<input type="hidden" name="laytongtien" value="'+tong+'">');
                $("#tongtien").html(data + ' VNĐ');
                $("#tongtien1").html(data1 + ' VNĐ');
            }
            $("#phivanchuyen").append('<option id="'+id+'" value="'+price+'" '+select+'>'+name+" - "+price+' VNĐ    </option>');
        });
    });

    $("#phivanchuyen").change(function(){
        $("#phivanchuyen option:selected").each(function() {
            var gettamtinh   = parseInt($("#gettamtinh").val());
            var price        = parseInt($(this).val());
            // var getGiamgia   = $("#getGiamgia").text();
            // var getGiamgiapt = $("#getGiamgiapt").text();
            // var tong         = gettamtinh+price-getGiamgia;
            var tong         = 0;
            // const formatter = new Intl.NumberFormat('de-DE', {
            //   style: 'decimal',
            //   minimumFractionDigits: 0
            // })
            // if(getGiamgia>0)
            // {
            //     tong = gettamtinh+price-getGiamgia
            //     $("#tongtien").html(formatter.format(tong)+' VNĐ');
            //     $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+tong+'">');
            // }
            tong = gettamtinh+price;
            $("#tongtien").html(formatter.format(tong)+' VNĐ');
            $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+tong+'">');
            $("#phi").html('<span class="pull-right" value="'+price+'">'+formatter.format(price)+' VNĐ</span>');
        });
    });

    var url = "./api/province";
    $.getJSON(url,function(result){
        $.each(result, function(i, field){
            var idprovince1  = $("#idprovince1").val();
            var id   = field.provinceid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==idprovince1)
            {
                select = 'selected=""';
            }
            $("#province").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });

    var idprovince1 = $("#idprovince1").val();
    var url = "./api/district/"+idprovince1;
    $.getJSON(url, function(result){
        console.log(result);
        $.each(result, function(i, field){
            var iddistrict1  = $("#iddistrict1").val();
            var id   = field.districtid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==iddistrict1)
              {
                select = 'selected=""';
              }
            $("#district").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });

    var iddistrict1 = $("#iddistrict1").val();
    $("#ward").html('<option value="-1">---- Lựa chọn Phường/ Xã ----</option>');
        var url = "./api/ward/"+iddistrict1;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                if(id==idward1)
                  {
                    select = 'selected=""';
                  }
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });

    $("#province").change(function(){
        $("#district").html('<option value="-1">---- Lựa chọn Quận/ Huyện ----</option>');
        var idprovince = $("#province").val();
        var url = "./api/district/"+idprovince;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });
    });

    $("#district").change(function(){
        $("#ward").html('<option value="-1">---- Lựa chọn Phường/ Xã ----</option>');
        var iddistrict = $("#district").val();
        var url = "./api/ward/"+iddistrict;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
    });

    $("section.alert").delay(5000).slideUp();
    // $('#magiamgia').hide();
    // $('#magiamgia_click').click(function (even){
    //     $('#magiamgia').toggle();
    // });
    // $("#btnHoantac").hide()
    $("#tongtien1").hide()
    const formatter = new Intl.NumberFormat('de-DE', {
        style: 'decimal',
        minimumFractionDigits: 0
    });
    $('#btnHoantac').click(function (even){
        function convert_int(string)
        {
            formattt     = string.replace(/[_\s\VN]/g, '').replace(/[^a-z0-9-\s]/gi, '');
            return formattt;
        }
        var value    = 0;
        var tongtien = document.getElementById("tongtien1").innerText;
        var tamtinh  = formatter.format($("#gettamtinh1").val());
        var tamtinh1  = $("#gettamtinh1").val();
        // document.getElementById("btnApdung").disabled = false;
        // document.getElementById("code").disabled      = false;
        // $('#btnHoantac').hide();
        // $('#magiamgia').show();
        $("#getGiamgia").html(value);
        $("#getGiamgiapt").html(value);
        $("#tamtinh").html(tamtinh+' VNĐ');
        // $("#luuprice").html("0");
        $("#tongtien").html(tongtien);
        $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+convert_int(tongtien)+'">');2
        $("#luutt").html('<input type="hidden" id="gettamtinh" value="'+tamtinh1+'">');
        $("#thongbao1").html('');
        $("#code").prop('disabled', false);
        $("#btnApdung").prop('disabled', false);
        $("#btnApdung").css('display','block');
        $("#btnHoantac").prop('disabled', true);
        $("#btnHoantac").css('display', 'none');
    });
   
    $('#btnApdung').click(function (even){
        var code = $("#code").val();
        var tamtinh = $("#gettamtinh").val();
        var urlgiamgia       = "./api/giamgia/"+code;
        var vanchuyen = $('#phivanchuyen').val();
        $.ajax({
            type: "GET",
            url: "./api/giam-gia-detail",
            data: {
                id: code,
                tamtinh: tamtinh,
                cart: '{{$getIdSp}}',
                vanchuyen: vanchuyen
            },
            success: function (result) {
                console.log(result['msg']);    
                if(result['msg'] != undefined)
                {
                    let resTT = formatter.format(parseInt(result.msg));
                    $("#tamtinh").html(resTT+' VNĐ');
                    $("#gettamtinh").val(parseInt(result.msg));
                    let tongcong = formatter.format(parseInt(result.msg) + parseInt(vanchuyen));
                    $("#tongtien").html(tongcong+' VNĐ');
                    console.log(tongcong);
                    $("#code").prop('disabled', true);
                    $("#btnApdung").prop('disabled', true);
                    $("#btnApdung").css('display','none');
                    $("#btnHoantac").prop('disabled', false);
                    $("#btnHoantac").css('display', 'block');
                    $("#thongbao1").html('<label style="margin-top: 5px;margin-bottom: 0px" class="btn-success" id="thanhcong1">Áp dụng mã thành công.</label>');
                }else{
                    $("#thongbao1").html('<label style="margin-top: 5px;margin-bottom: 0px;color: red" id="thanhcong1">'+result['err']+'</label>');
                }
                
            },
            error: function () {
                
            }
        })

    });
    // $('#btnApdung').click(function (even){

    //     var urldc       = "./datacart";
    //     var urlgg       = "./api/giamgia";
    //     var idGiamgia   = $("#code").val();
    //     var gettamtinh  = parseInt($("#gettamtinh").val());

    //     var phi         = document.getElementById("phi").innerText;
    //     var phi_convert = parseInt(convert_int(phi));

    //     const formatter = new Intl.NumberFormat('de-DE', {
    //                                     style: 'decimal',
    //                                     minimumFractionDigits: 0
    //                                     });

    //     function convert_int(string)
    //     {
    //         formattt     = string.replace(/[_\s\VN]/g, '').replace(/[^a-z0-9-\s]/gi, '');
    //         return formattt;
    //     }

    //     function dateToString(date) {
    //         var month = date.getMonth() + 1;
    //         var day = date.getDate();
    //         var dateOfString = date.getFullYear() + "-";
    //         dateOfString += (("" + month).length < 2 ? "0" : "") + month + "-";
    //         dateOfString += (("" + day).length < 2 ? "0" : "") + day;
    //         return dateOfString;
    //     }

    //     $.getJSON(urldc,function(sanphams1){
    //         ttgiam2 = 0;
    //         var arr = [];
    //         Object.keys(sanphams1).forEach(function(key) {
    //             if (sanphams1[key].options.type_giamgia==0){
    //                 var price = sanphams1[key].price;
    //                 ttgiam2=ttgiam2+price;
    //             }
    //         });
    //     });

    //     $.getJSON(urlgg,function(magiamgia){

    //         for (var i = 0; i < magiamgia.length; i++) 
    //         {   
    //             var maid        = magiamgia[i].id;
    //             var currentdate = new Date();
    //             var hientai     = dateToString(currentdate);
    //             var batdau      = magiamgia[i].begin;
    //             var ketthuc     = magiamgia[i].end;
    //             console.log(hientai);
    //              console.log(batdau);
    //               console.log(ketthuc);
    //             if(maid==idGiamgia && batdau<=hientai && hientai<=ketthuc)
    //             {
    //                 console.log("ok men");
    //                 var value       = magiamgia[i].money;
    //                 var loai        = magiamgia[i].loai;
    //                 if(loai==1)
    //                 {
    //                     var value       = magiamgia[i].money;
    //                     var donvi       = magiamgia[i].donvi;
    //                     $.getJSON(urldc,function(sanphams){
    //                         ttgiam1 = 0;
    //                         for (var sp in sanphams)  
    //                         { 
    //                             var type_giamgia = sanphams[sp].options.type_giamgia;
    //                             if(type_giamgia==loai)
    //                             {
                                    
    //                                 var price   = sanphams[sp].price;
    //                                 var qty     = sanphams[sp].qty;
    //                                 if(donvi==1)
    //                                 {
    //                                     ttgiam3= parseInt(ttgiam2);
    //                                     var sp = price*((100-value)/100);
    //                                     var spsl = sp*qty;
    //                                     var ttgiam1 = ttgiam1 + spsl;
    //                                     tamtinh = formatter.format(ttgiam1+ttgiam3);
    //                                     tamtinh1 = ttgiam1+ttgiam3;
    //                                     testtinh = ttgiam1+ttgiam3+phi_convert;
    //                                     $("#tamtinh").html('<span id="tamtinh" class="total-line-subprice pull-right">'+tamtinh+' VNĐ</span>');
    //                                     $("#luutt").html('<input type="hidden" id="gettamtinh" value="'+tamtinh1+'">');
    //                                     $("#tongtien").html(formatter.format(ttgiam1+ttgiam3+phi_convert)+' VNĐ');
    //                                     $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+testtinh+'">');
    //                                     document.getElementById("btnApdung").disabled = true;
    //                                     document.getElementById("code").disabled      = true;
    //                                     $('#magiamgia').hide();
    //                                     $("#btnHoantac").show()
    //                                     $("#getGiamgia").html(value);
    //                                     $("#getGiamgiapt").html(0);
    //                                     $("#thongbao1").html('<label class="btn-success" id="thanhcong1">Áp dụng mã thành công.</label>');
    //                                     console.log(value +"%");
    //                                 }
    //                                 else
    //                                 {
    //                                     ttgiam3= parseInt(ttgiam2);
    //                                     var sp = price-value;
    //                                     var spsl = sp*qty;
    //                                     var ttgiam1 = ttgiam1 + spsl;
    //                                     tamtinh = formatter.format(ttgiam1+ttgiam3);
    //                                     tamtinh1 = ttgiam1+ttgiam3;
    //                                     testtinh = ttgiam1+ttgiam3+phi_convert;
    //                                     $("#tamtinh").html('<span id="tamtinh" class="total-line-subprice pull-right">'+tamtinh+' VNĐ</span>');
    //                                     $("#luutt").html('<input type="hidden" id="gettamtinh" value="'+tamtinh1+'">');
    //                                     $("#tongtien").html(formatter.format(ttgiam1+ttgiam3+phi_convert)+' VNĐ');
    //                                     $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+testtinh+'">');
    //                                     document.getElementById("btnApdung").disabled = true;
    //                                     document.getElementById("code").disabled      = true;
    //                                     $('#magiamgia').hide();
    //                                     $("#btnHoantac").show()
    //                                     $("#getGiamgia").html(value);
    //                                     $("#getGiamgiapt").html(0);
    //                                     $("#thongbao1").html('<label class="btn-success" id="thanhcong1">Áp dụng mã thành công.</label>');
    //                                     console.log(ttgiam1);
    //                                     console.log(ttgiam3);
    //                                     console.log(value +"d");
    //                                 }
    //                             }
    //                             else
    //                             {
    //                                 console.log("ko co san pham de su dung");
    //                             }
    //                         }        
    //                     });
    //                 }else if(loai==0){
    //                     var value    = magiamgia[i].money;
    //                     var donvi    = magiamgia[i].donvi;
    //                     var dieukien = magiamgia[i].tongtien;
    //                     $.getJSON(urldc,function(sanphams){
    //                         for (var sp in sanphams)  
    //                         { 
    //                             var type_giamgia = sanphams[sp].options.type_giamgia;
    //                             if(type_giamgia==loai)
    //                             {
    //                                 if(dieukien<gettamtinh)
    //                                 {
    //                                     if(donvi==1)
    //                                     {
    //                                         var ttgiam1 = gettamtinh*((100-value)/100);
    //                                         document.getElementById("btnApdung").disabled = true;
    //                                         document.getElementById("code").disabled      = true;
    //                                         $('#magiamgia').hide();
    //                                         $("#btnHoantac").show()
    //                                         $("#getGiamgia").html(value);
    //                                         $("#getGiamgiapt").html(0);
    //                                         tamtinh = formatter.format(ttgiam1);
    //                                         tamtinh1 = ttgiam1;
    //                                         testtinh = ttgiam1+phi_convert;
    //                                         $("#tamtinh").html('<span id="tamtinh" class="total-line-subprice pull-right">'+tamtinh+' VNĐ</span>');
    //                                         $("#luutt").html('<input type="hidden" id="gettamtinh" value="'+tamtinh1+'">');
    //                                         $("#tongtien").html(formatter.format(ttgiam1+phi_convert)+' VNĐ');
    //                                         $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+testtinh+'">');
    //                                         $("#thongbao1").html('<label class="btn-success" id="thanhcong1">Áp dụng mã thành công.</label>'); 
    //                                         console.log(value +"%");
    //                                     }
    //                                     else
    //                                     {
    //                                         var ttgiam1 = gettamtinh-value;
    //                                         document.getElementById("btnApdung").disabled = true;
    //                                         document.getElementById("code").disabled      = true;
    //                                         $('#magiamgia').hide();
    //                                         $("#btnHoantac").show()
    //                                         $("#getGiamgia").html(value);
    //                                         $("#getGiamgiapt").html(0);
    //                                         tamtinh = formatter.format(ttgiam1);
    //                                         tamtinh1 = ttgiam1;
    //                                         testtinh = ttgiam1+phi_convert;
    //                                         $("#tamtinh").html('<span id="tamtinh" class="total-line-subprice pull-right">'+tamtinh+' VNĐ</span>');
    //                                         $("#luutt").html('<input type="hidden" id="gettamtinh" value="'+tamtinh1+'">');
    //                                         $("#tongtien").html(formatter.format(ttgiam1+phi_convert)+' VNĐ');
    //                                         $("#donhang").html('<input type="hidden" id="laytongtien" name="laytongtien" value="'+testtinh+'">');
    //                                         $("#thongbao1").html('<label class="btn-success" id="thanhcong1">Áp dụng mã thành công.</label>');
    //                                         console.log(value +"d");
    //                                     }
    //                                 }
    //                             }
    //                             else
    //                             {
    //                                 console.log("ko co san pham de su dung");
    //                             }
    //                         }        
    //                     });
    //                 }
    //             }
    //             else
    //             {
    //                 $("#thongbao1").html('<label  style="color: red" id="thanhcong1">Xin lỗi, mã giảm giá này không có hiệu lực. Vui lòng kiểm tra lại mã.</label>');
    //             }
    //         }
    //     });
    // });  


});
</script>
    <!-- notification JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/notifications/Lobibox.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/notifications/notification-active.js') }}"></script>
</body>
</html>