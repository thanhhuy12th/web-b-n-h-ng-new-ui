@extends('frontend.welcome')
@section('content')

<div class="jumbotron" style="padding: 20px 0px 20px 0px">
    <!-- Nav pills -->
    @include('admin.blocks.alert')
    
</div>
    
<script>
    function CopyToClipboard(containerid) {
        if (document.selection) { 
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("copy");   

        } else if (window.getSelection) {
            var range = document.createRange();
             range.selectNode(document.getElementById(containerid));
             window.getSelection().addRange(range);
             document.execCommand("copy");
             alert("text copied") 
        }
    }
    function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz,randomPoz+1);
        }
        return randomString;
    }
    function rancode() {
        let ran = (randomString(4)+({{nameLogin()->id}}).toString());
        $('#aff_code').val(ran);
    }
    function closefun() {
        $('#btnregister').show();
        $('#showinput').empty();
    }
    $(document).ready(function(){
        $('#btnregister').on('click',function () {
            $(this).hide();
            let html = `<div style="text-align: right;">
                            <button onclick="closefun()" type="button" class="btn">X</button>
                        </div>
                        <div class="row" style="padding-top: 10px;">
                        <lable>Code</lable>
                            <div class="col-8 col-sm-10 form-group" style="padding-right: 0px;">
                                <input type="text" id="aff_code" name="aff_code" class="form-control" placeholder="Nhập mã công tác viên">
                            </div>
                            <div class="col-4 col-sm-2 form-group" style="text-align: center">
                                <button onclick="rancode()" type="button" class="btn btn-success">Lấy mã</button>
                            </div>
                        </div>      
                        <div class="form-group" style="text-align: center;">
                            <button type="submit" class="btn btn-primary form-group"><i style="font-size: 23px" class="fa fa-envelope">&ensp;</i>Đăng ký</button>
                        </div>`;
            $('#showinput').append(html);
        })
        $('.nav-sidebar .nav-item .nav-link').on("click", function() {
            $(this).removeClass("active");
            $('.nav-group-sub .nav-item .nav-link').removeClass("active");
        });

        $('.nav-group-sub .nav-item .nav-link').on("click", function() {

            $('.nav-sidebar .nav-item .nav-link').removeClass("active");   
        });
        var url = "./api/province";
        $.getJSON(url,function(result){
            $.each(result, function(i, field){
                var idprovince1  = $("#idprovince1").val();
                var id   = field.provinceid;
                var name = field.name;
                var type = field.type;
                var select = "";
                if(id==idprovince1)
                {
                    select = 'selected=""';
                }
                $("#province").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
        var idprovince1 = $("#idprovince1").val();
        $("#district").html('<option value="-1">Lựa chọn Quận/ Huyện</option>');
        var url = "./api/district/"+idprovince1;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var iddistrict1  = $("#iddistrict1").val();
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                var select = "";
                if(id==iddistrict1)
                  {
                    select = 'selected=""';
                  }
                $("#district").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
        var iddistrict1 = $("#iddistrict1").val();
        $("#ward").html('<option value="-1">Lựa chọn Phường/ Xã</option>');
            var url = "./api/ward/"+iddistrict1;
            $.getJSON(url, function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var idward1  = $("#idward1").val();
                    var id   = field.wardid;
                    var name = field.name;
                    var type = field.type;
                    var select = "";
                    if(id==idward1)
                      {
                        select = 'selected=""'
                      }
                    $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
        $("#province").change(function(){
            $("#district").html('<option value="-1">---- Lựa chọn Quận/ Huyện ----</option>');
            var idprovince = $("#province").val();
            var url = "./api/district/"+idprovince;
            $.getJSON(url, function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id   = field.districtid;
                    var name = field.name;
                    var type = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
                });
            });
        });
        $("#district").change(function(){
            $("#ward").html('<option value="-1">---- Lựa chọn Phường/ Xã ----</option>');
            var iddistrict = $("#district").val();
            var url = "./api/ward/"+iddistrict;
            $.getJSON(url, function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var idward1  = $("#idward1").val();
                    var id   = field.wardid;
                    var name = field.name;
                    var type = field.type;
                    var select = "";
                    $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
                });
            });
        });
        if($("#tongtienmua").val() >= $("#vipmoney").val()){  

            $("#khachvip").html('<span class="btn btn-warning" style="position: absolute;font-weight: bold;">VIP</span>');
        }else{ 
            $("#khachvip").html('');
        }
    });
</script>
@endsection