@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('nhanxet.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Thêm</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_sua')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('nhanxet.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-sm-12 row">
                        <div class="col-sm-6 form-group">
                           <span class="input-group-addon">Họ tên</span>
                           <input value="{{old('fullname')}}" type="text" name="fullname" class="form-control" placeholder="Nhập họ tên">
                        </div>
                        <div class="col-sm-6 form-group">
                           <span class="input-group-addon ">Hình ảnh</span>
                                <input type="file" name="photos" class="btn btn-primary  ">
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <span>Nội Dung</span>
                        <textarea rows="10" id="editor1" name="content" placeholder="Type the content here!" >
                           
                        </textarea>
                    </div>
                    <div class="form-group">
                        <button name="addTintuc" name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection