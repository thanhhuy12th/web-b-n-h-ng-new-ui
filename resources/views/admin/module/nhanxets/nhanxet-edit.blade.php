@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('nhanxet.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_sua')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('nhanxet.update', ['nhanxet' => $nhanxet->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PUT') }}
                    <div class="col-sm-12 border-bottom row form-group" style="padding-bottom: 5px">
                        <div class="col-sm-6 form-group">
                            <label>Hình ảnh tin tức</label>  
                            @if($nhanxet->img == "product_default.jpg")
                                Không có hình ảnh
                            @else
                            <div class="row">
                                @php $arr_img = explode("||",$nhanxet->img);@endphp
                                @for($i=0 ; $i<count($arr_img) ; $i++)
                                 <img style="width: 95px;height: 95px;padding: 5px;margin-right: 10px" class="form-control" src="{{ asset('image/'.$nhanxet->img) }}" alt="">
                                @endfor
                                {{-- <button style="height: 35px" class="btn btn-danger" type="submit" title="Xóa ảnh" name="xoaEdit"> <i class="fa fa-trash-o" aria-hidden="true"></i></button> --}}
                                @endif
                            </div>
                           
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Chọn hình ảnh</label>
                            <input type="file" name="photos[]" multiple="" class="btn btn-primary  ">
                            <input type="hidden" name="hinhcu" value="{{$nhanxet->img}}">
                        </div>
                   
                    </div>
                    <div class="col-sm-6 form-group row">
                       <span class="input-group-addon">Họ tên</span>
                       <input value="{{old('fullname',$nhanxet->fullname)}}" type="text" name="fullname" class="form-control" placeholder="Nhập họ tên">
                    </div>
                    <div class="col-sm-12 form-group">
                        <span>Nội Dung</span>
                        <textarea rows="10" id="editor1" name="content" placeholder="Type the content here!" >
                            {{$nhanxet->content}}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <button name="addTintuc" name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection