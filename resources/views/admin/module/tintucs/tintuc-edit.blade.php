@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('tintuc.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_sua')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('tintuc.update', ['tintuc' => $tintuc->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="col-sm-12 border-bottom row" style="padding-bottom: 5px">
                        <div class="col-sm-6 form-group">
                            <label>Hình ảnh tin tức</label>  
                            @if($tintuc->img == "product_default.jpg")
                                Không có hình ảnh
                            @else
                            <div class="row">
                                @php $arr_img = explode("||",$tintuc->img);@endphp
                                @for($i=0 ; $i<count($arr_img) ; $i++)
                                 <img style="width: 95px;height: 95px;padding: 5px;margin-right: 10px" class="form-control" src="{{ asset('image/'.$arr_img[$i]) }}" alt="">
                                @endfor
                                <button style="height: 35px" class="btn btn-danger" type="submit" title="Xóa ảnh" name="xoaEdit"> <i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                @endif
                            </div>
                           
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Chọn hình ảnh</label>
                            <input type="file" name="photos[]" multiple="" class="btn btn-primary  ">
                            <input type="hidden" name="hinhcu" value="{{$tintuc->img}}">
                        </div>
                   
                    </div>
                    <div class="col-sm-12 row">
                        <div class="form-group col-sm-6">
                            <span class="input-group-addon">Tiêu đề</span>
                                            <input value="{{old('title',$tintuc->title)}}" type="text" name="title" class="form-control" placeholder="Tên sản phẩm ">
                        </div>
                        <div class="form-group col-sm-6">

                            <span class="input-group-addon">Alias</span>
                                            <input value="{{old('alias',$tintuc->alias)}}" type="text" name="alias" class="form-control" placeholder="Alias">
                        </div>
                    </div>
                    <div class="col-sm-12 row">
                        <div class="form-group col-sm-6">
                            <span class="input-group-addon">Danh mục</span>
                                <select name="category" class="form-control pro-edt-select form-control-primary">
                                        <option value="">Hãy chọn danh mục</option>                       
                                        @foreach($dmtt as $ctlg)
                                        @if($ctlg->id_parent >= 1)
                                            <option value="{{ $ctlg->id }}" {{ (old('category',$tintuc->category) == $ctlg->id) ? 'selected' : '' }}>{{$ctlg->name}}</option>
                                        @endif
                                        @endforeach
                                        
                                </select>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                         <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-solid bg-slate border-0 nav-tabs-component rounded">
                                <li class="nav-item"><a href="#colored-rounded-tab1" class="nav-link active" data-toggle="tab">Mô tả ngắn</a></li>
                                <li class="nav-item"><a href="#colored-rounded-tab2" class="nav-link" data-toggle="tab">Nội dung</a></li>
                            </ul>                   
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="colored-rounded-tab1">
                                     <textarea rows="10" id="editor1" name="intro" class="form-control col-md-12 col-xs-12" >{{old('intro',$tintuc->intro)}}</textarea>
                                </div>
                                 <div class="tab-pane fade" id="colored-rounded-tab2">
                                    <textarea rows="10" id="editor2" name="content" class="form-control col-md-12 col-xs-12" >{{old('content',$tintuc->content)}}</textarea>                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="addTintuc" name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection