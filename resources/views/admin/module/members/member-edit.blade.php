@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('member.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Cập nhật thông tin khách hàng</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{route('member.update', ['member' => $member->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Họ tên *</label>
                                                <input value="{{old('fullname',$member->fullname)}}" type="text" name="fullname" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Số điện thoại *</label>
                                                <input value="{{old('phone',$member->phone)}}" type="text" name="phone" class="form-control"  >
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Email *</label>
                                                <input value="{{old('email',$member->email)}}" type="text" name="email" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                              <label class="lable_edit" for="diachi">Địa chỉ nhận hàng</label>
                                                <input value="{{$member->diachi}}" name="diachi" type="text" placeholder="Địa chỉ" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                           <label class="lable_edit">Tên đăng nhập *</label>
                                                <input value="{{old('username',$member->username)}}" type="text" name="username" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Mật khẩu *</label>
                                                <input value="{{old('password',$member->password)}}" type="password" name="password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label for="province">Tỉnh/ Thành phố</label>
                             <select class="form-control" id="province" name="province">
                                                    @if($member->province == -1)
                                                        <option value="-1">Vui lòng chọn Tỉnh/ Thành phố</option>
                                                    @else
                                                        @foreach($getprovince as $gpv)
                                                        @if($gpv->provinceid == $member->province)
                                                        <option value="{{$gpv->provinceid}}">{{$gpv->type}} {{$gpv->name}}</option>
                                                        @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="district">Quận/ Huyện</label>
                            <select class="form-control" id="district" name="district">
                                                    @if($member->district == -1)
                                                        <option value="-1">Vui lòng chọn Quận/ Huyện</option>
                                                    @else
                                                        @foreach($getdistrict as $gdt)
                                                        @if($gdt->districtid == $member->district)
                                                        <option value="{{$gdt->districtid}}">{{$gdt->type}} {{$gdt->name}}</option>
                                                        @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label for="ward">Phường/ Xã</label>
                                            <select class="form-control" id="ward" name="ward">
                                                    @if($member->ward == -1)
                                                        <option value="-1">Vui lòng chọn Phường/ Xã</option>
                                                    @else
                                                        @foreach($getward as $gw)
                                                        @if($gw->wardid == $member->ward)
                                                        <option value="{{$gw->wardid}}">{{$gw->type}} {{$gw->name}}</option>
                                                        @endif
                                                        @endforeach
                                                    @endif
                                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
  var url = "../../../api/province";
  $.getJSON(url,function(result){
    $.each(result, function(i, field){
      var id   = field.provinceid;
      var name = field.name;
      var type = field.type;
      $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
    });
  });


$("#province").change(function(){
    $("#district").html('<option value="-1">Vui lòng chọn Quận/ Huyện</option>');
        var idprovince = $("#province").val();
        var url = "../../../api/district/"+idprovince;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
        });
    });
});

$("#district").change(function(){
    $("#ward").html('<option value="-1">Vui lòng chọn Phường/ Xã</option>');
        var iddistrict = $("#district").val();
        var url = "../../../api/ward/"+iddistrict;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                $("#ward").append('<option value="'+id+'">'+type+" "+name+'</option>');
        });
    });
});
});
</script>
@endsection