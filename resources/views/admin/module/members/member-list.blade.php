@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('member.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Danh sách người dùng</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                         <a  class="btn btn-success" href="{{ route('member.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col"><input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);"></th>
                                        <th scope="col">Mã</th>
                                        <th scope="col">Họ tên</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Địa chỉ</th>
                                        <th scope="col">Số lần mua</th>
                                        <th scope="col">Tổng tiền mua</th>
                                        <th scope="col">Khách vip</th>
                                        <th scope="col" style="text-align: center;">Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($member as $item)
                                        <tr>
                                            <td><input value="{{ $item->id }}" type="checkbox" name="checked[]"></td>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->fullname}}</td>
                                            <td>+84{{$item->phone}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>
                                                <?php 
                                                $wardname           = "";
                                                $districtname       = "";
                                                $provincename       = "";
                                                foreach ($province as $key) {
                                                    if($key->provinceid == $item->province)
                                                    {
                                                        $provincename = $key->type." ".$key->name;
                                                    }
                                                }
                                                foreach ($district as $key) {
                                                    if($key->districtid == $item->district)
                                                    {
                                                        $districtname = $key->type." ".$key->name;
                                                    }
                                                }
                                                foreach ($ward as $key) {
                                                    if($key->wardid ==  $item->ward)
                                                    {
                                                        $wardname = $key->type." ".$key->name;
                                                    }
                                                }
                                                ?>
                                                {{$item->diachi}} {{$wardname}} {{$districtname}} {{$provincename}}</td>
                                            <td>{{$item->solanmua}}</td>
                                            <td>{{convert_money($item->tongtienmua)}} đ</td>
                                            <td>
                                                @if($item->tongtienmua > getCauhinh('vipmoney'))
                                                    <span class="badge badge-warning" style="color: #212529;font-size:15px;background-color: #ffc107;">VIP</span>
                                                @else
                                                    Thường
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                                <a href="{{ route('member.edit', ['member' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            {{ method_field('DELETE') }}
                           <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </form>
                     {{-- {{$useradmin->links()}} --}}
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection