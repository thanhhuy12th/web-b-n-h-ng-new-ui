@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('dmsp.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_sua')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('dmsp.update', ['dmsp' => $dmsp->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                             <span class="input-group-addon">Tên danh mục</span>
                                            <input value="{{old('title',$dmsp->name)}}" type="text" name="name" class="form-control" placeholder="Tên danh mục ">
                        </div>
                        <div class="col-sm-6">
                             <span class="input-group-addon">Alias</span>
                                           <input value="{{old('alias',$dmsp->alias)}}" type="text" name="alias" class="form-control" placeholder="Alias">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <span class="input-group-addon">Danh mục</span>
                                <select name="id_parent" class="form-control pro-edt-select form-control-primary">
                                <option value="">Please Choose Category</option>
                                @if($dmsp->id_parent == 0)
                                    @for($i = 0; $i < count($optArr); $i++)
                                    <option value="{!!$optArr[$i]['id']!!}" {{ $dmsp->id == $optArr[$i]['id'] ? "selected" : "" }}>{!!$optArr[$i]['name']!!}</option>
                                    @endfor
                                @else
                                @for($i = 0; $i < count($optArr); $i++)
                                    <option value="{!!$optArr[$i]['id']!!}" {{ $dmsp->id_parent == $optArr[$i]['id'] ? "selected" : "" }}>{!!$optArr[$i]['name']!!}</option>
                                @endfor
                                @endif
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <span class="input-group-addon ">Vị trí</span>
                            <input value="{{old('vitri',$dmsp->vitri)}}" type="text" name="vitri" class="form-control" placeholder="Vị trí">
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="addTintuc" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection