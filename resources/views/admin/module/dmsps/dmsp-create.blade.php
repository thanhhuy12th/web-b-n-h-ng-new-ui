@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('dmsp.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Thêm</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_them')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('dmsp.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                             <span class="input-group-addon">Tên danh mục</span>
                                            <input value="{{old('name')}}" type="text" name="name" class="form-control" placeholder="Tên danh mục">
                        </div>
                        <div class="col-sm-6">
                             <span class="input-group-addon">Alias</span>
                                            <input value="{{old('alias')}}" type="text" name="alias" class="form-control" placeholder="Alias">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                             <span class="input-group-addon">Danh mục</span>
                                        <select name="id_parent" class="form-control pro-edt-select form-control-primary">
                                            <option value="0">Không chọn loại</option>
                                            @for($i = 0; $i < count($optArr); $i++)
                                                <option value="{{$optArr[$i]['id']}}">{!!$optArr[$i]['name']!!}</option>
                                            @endfor
                                        </select>
                        </div>
                        <div class="col-sm-6">
                            <span class="input-group-addon ">Vị trí</span>
                                        <input value="0" value="{{old('vitri')}}" type="text" name="vitri" class="form-control" placeholder="Vị trí">
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection