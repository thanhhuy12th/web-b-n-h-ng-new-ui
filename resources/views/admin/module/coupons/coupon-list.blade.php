@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('coupon.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Danh sách phiếu</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                         <a  class="btn btn-success" href="{{ route('coupon.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </td>
                                        <th>Mã phiếu</th>
                                        <th>Số tiền</th>
                                        <th>Đơn vị</th>
                                        <th>Loại giảm giá</th>
                                        <th>Bắt đầu</th>
                                        <th>Kết thúc</th>
                                        <th>Thời hạn</th>
                                        <th>Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach($coupon as $item)
                                        <tr style="vertical-align: top">
                                            <td>
                                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                            </td>
                                            <td style="width: 10%">{{$item->id}}</td>
                                            <td style="width: 10%">
                                                @if($item->donvi==0)
                                                    <?php echo number_format($item->money, 0, '.', ',');?>
                                                @else
                                                    {{$item->money}}
                                                @endif
                                            </td>
                                             <td style="width: 7%">
                                                @if($item->donvi==0)
                                                    VNĐ
                                                @else
                                                    %
                                                @endif
                                            </td>
                                            <td style="width: 17%">
                                                @if($item->loai==0)
                                                    Giảm theo giá
                                                @else
                                                    Giảm theo sản phẩm
                                                @endif
                                            </td>
                                            <td style="width: 15%">
                                                <?php
                                                    $date1 = convertdate($item->begin); 
                                                ?>
                                                 {{$date1}}
                                            </td>
                                            <td style="width: 15%">
                                                <?php
                                                    $date2 = convertdate($item->end); 
                                                ?>
                                                {{$date2}}
                                            </td>
                                            <td style="width: 16%">
                                                {{convertdate2($item->begin,$item->end)}}
                                            </td>
                                            <td style="width: 10%;text-align: center;">
                                                <a href="{{ route('coupon.edit', ['coupon' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>                   
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            {{ method_field('DELETE') }}
                           <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </form>
                     {{$coupon->links()}}
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection