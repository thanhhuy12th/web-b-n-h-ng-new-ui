@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('coupon.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Thêm</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Thông tin phiếu</h5>
                
            </div>
            <div class="card-body">
                <form action="{{ route('coupon.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                             <span class="input-group-addon">Mã phiếu</span>
                                            <input value="{{old('id')}}" type="text" name="id" class="form-control" placeholder="Mã phiếu">
                        </div>
                        <div class="col-sm-6 " style="padding-right: 0px">
                            <span class="input-group-addon">Thời gian</span>
                            <div class="row" style="padding-right: 0px">
                                <div class="col-sm-6 row" style="padding-right: 0px">
                                    <div class="col-sm-2">
                                        <span class="input-group-addon">Từ</span>
                                    </div>
                                    <div class="col-sm-10">
                                        <input value="{{old('begin',$hientai)}}" type="date" name="begin" class="form-control" placeholder="Nhập ngày">
                                    </div>
                                   
                                </div>
                                <div class="col-sm-6 row" style="padding-right: 0px">
                                     <div class="col-sm-3">
                                        <span class="input-group-addon">Đến</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input value="{{old('end',$hientai)}}" type="date" name="end" class="form-control" placeholder="Nhập ngày">
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-sm-12 row">
                        <div class="col-sm-6 form-group">
                            <span class="input-group-addon">Money</span>
                            <div class="row" style="flex-wrap: inherit;">
                                <div class="col-xs-10 col-sm-10">
                                    <input value="{{old('money')}}" type="text" name="money" class="form-control" placeholder="Nhập số tiền">
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <select name="donvi" style="background: #E6E6E6;color: #333;border: 1px solid #BDBDBD" class="btn" type="button">
                                        <option value="0">VNĐ</option>
                                        <option value="1">%</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <span class="input-group-addon">Loại giảm giá</span>
                            <select id="loai" name="loai" class="form-control">
                                <option value="1">Giảm theo sản phẩm</option>
                                <option value="0">Giảm theo giá đơn hàng</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6" >
                        </div>
                        <div class="col-sm-6" id="donhang">
                        </div>
                    </div>
                    <div class="form-group">
                        <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#loai").change(function(){
            var id = $("#loai").val();
            console.log(id);
            if(id==0)
            {
                $("#donhang").html('<span>Nhập tổng số tiền của đơn hàng để có thể áp dụng mã giảm giá</span><input value="{{old('tongtien')}}" type="text"name="tongtien"class="form-control" placeholder="VD: 10000">');
            }
            else
            {
                $("#donhang").html('');
            }

        });

    });
</script>
</form>

@endsection