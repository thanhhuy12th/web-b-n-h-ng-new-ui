@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('dmtt.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.danhmuc_tintuc')}}</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                         <a  class="btn btn-success" href="{{ route('dmtt.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                     <tr>
                                        <td>
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </td>
                                        <th>Tên danh mục</th>
                                        <th>Vị trí</th>
                                        <th>Danh mục</th>
                                        <th>Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dmtt as $item)
                                        <tr style="vertical-align: top">
                                            <td>
                                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                            </td>
                                            <td style="width: 32%">{{$item->name}}</td>
                                            <td style="width: 20%">{{$item->vitri}}</td>
                                            <td style="width: 35%">
                                                    @if(empty($item->id_parent))
                                                    {{$item->name}}
                                                    @else
                                                        @foreach($optArr as $opt)
                                                            @if($opt['id'] == $item->id_parent)
                                                                {{$opt['name']}}
                                                            @endif
                                                        @endforeach
                                                    @endif 
                                           </select>
                                            </td>
                                            <td style="width: 10%;text-align: center;">
                                                <a href="{{ route('dmtt.edit', ['dmtt' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>                   
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            {{-- <div class="col-sm-6"> --}}
                                {{ method_field('DELETE') }}
                                <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            {{-- </div> --}}
                            <div style="position: absolute;right: 20px">
                                {{$dmtt->links()}}
                            </div>
                        </div>
                    
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection