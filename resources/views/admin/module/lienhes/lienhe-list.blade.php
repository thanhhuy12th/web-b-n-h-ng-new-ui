@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('lienhe.index')}}" class="breadcrumb-item">Danh sách</a>
               {{--  <span class="breadcrumb-item active"></span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.danhsach_lienhe')}}</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">                  
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                       <td>
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </td>
                                        <th>Khách hàng</th>
                                        <th>Phone</th>
                                        <th>Địa chỉ</th>
                                        <th>Nội dung</th>
                                        <th>Thời gian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach($lienhe as $item)
                        <tr style="vertical-align: top">
                            <td>
                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">
                            </td>
                            <td style="width: 10%">{{$item->hoten}}</td>
                            <td style="width: 10%">{{$item->sdt}}</td>
                            <td style="width: 15%">{{$item->email}}</td>
                            <td style="width: 50%">{{$item->noidung}}</td>
                            <td style="width: 15%">{{convertdate1($item->created_at)}}</td>                 
                        </tr>
                        @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            {{ method_field('DELETE') }}
                           <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </form>
                     {{$lienhe->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection