@extends('admin.welcome')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('cauhinh.editNoId')}}" class="breadcrumb-item">Cấu hình website</a>
                {{-- <span class="breadcrumb-item active"></span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <!-- Rounded colored tabs -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">{{trans('message.cauhinh')}}</h6>

                    </div>
                     <form action="{{ route('cauhinh.updateNoId') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-tabs-solid bg-slate border-0 nav-tabs-component rounded">
                            <li class="nav-item"><a href="#colored-rounded-tab1" class="nav-link active" data-toggle="tab">Khác</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab2" class="nav-link" data-toggle="tab">Liên hệ</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab3" class="nav-link" data-toggle="tab">Giới thiệu</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab4" class="nav-link" data-toggle="tab">Footer</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab5" class="nav-link" data-toggle="tab">Mạng xã hội</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab6" class="nav-link" data-toggle="tab">Khách hàng</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab7" class="nav-link" data-toggle="tab">Email</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab8" class="nav-link" data-toggle="tab">Logo</a></li>
                        </ul>
                   
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="colored-rounded-tab1">
                                <span class="edit2 input-group-addon">Email</span>
                                <input value="{{old('email',$cauhinh[0]->chitiet)}}" type="text" name="email" class="edit2 form-control form-group" placeholder="Viết email">

                                <span class="edit2 input-group-addon">Địa chỉ</span>
                                <input value="{{old('title',$cauhinh[1]->chitiet)}}" type="text" name="title" class="edit2 form-control form-group" placeholder="Viết địa chỉ ">

                                <span class="edit2 input-group-addon">Điện thoại</span>
                                <input value="{{old('dienthoai',$cauhinh[2]->chitiet)}}" type="text" name="dienthoai" class="edit2 form-control form-group" placeholder="Viết điện thoại ">

                                <span class="edit2 input-group-addon">Tên website</span>
                                <input value="{{old('tenwebsite',$cauhinh[3]->chitiet)}}" type="text" name="tenwebsite" class="edit2 form-control form-group" placeholder="Viết tên website">

                                <span class="edit2 input-group-addon">Link website</span>
                                <input value="{{old('linkwebsite',$cauhinh[4]->chitiet)}}" type="text" name="linkwebsite" class="edit2 form-control form-group" placeholder="Nhập link">

                                <span class="edit2 input-group-addon">Link SEO</span>
                                <input value="{{old('keyword',$cauhinh[15]->chitiet)}}" type="text" name="keyword" class="edit2 form-control form-group" placeholder="Nhập link">

                                <span class="edit2 input-group-addon">Google Map</span>
                                <textarea rows="6" name="googlemap" class="edit2 form-control  form-group" placeholder="Viết code googlemap">{{old('googlemap',$cauhinh[5]->chitiet)}}</textarea>
                            </div>

                             <div class="tab-pane fade" id="colored-rounded-tab2">
                                <textarea rows="10" id="editor1" name="lienhe" class="form-control col-md-12 col-xs-12" >{{$cauhinh[6]->chitiet}}</textarea>                            
                            </div>
                            <div class="tab-pane fade" id="colored-rounded-tab3">
                               <textarea rows="10" id="editor2" name="gioithieu" class="form-control col-md-12 col-xs-12" >{{$cauhinh[7]->chitiet}}</textarea>
                            </div>

                            <div class="tab-pane fade" id="colored-rounded-tab4">
                               <textarea rows="10" id="editor3" name="footer" class="form-control col-md-12 col-xs-12" >{{$cauhinh[8]->chitiet}}</textarea>
                            </div>
                            <div class="tab-pane fade" id="colored-rounded-tab5">
                               <span class="edit2 input-group-addon">FaceBook</span>
                                <input value="{{old('facebook',$cauhinh[9]->chitiet)}}" type="text" name="facebook" class="edit2 form-control" placeholder="Link">

                                <span class="edit2 input-group-addon">Youtube</span>
                                <input value="{{old('youtube',$cauhinh[10]->chitiet)}}" type="text" name="youtube" class="edit2 form-control" placeholder="Link">

                                <span class="edit2 input-group-addon">Instagram</span>
                                <input value="{{old('instagram',$cauhinh[11]->chitiet)}}" type="text" name="instagram" class="edit2 form-control" placeholder="Link">

                            </div>

                            <div class="tab-pane fade" id="colored-rounded-tab6">
                                <p>Nhập số tiền mà khách hàng phải sử dụng nếu muốn trở thành khách hàng thân thiết</p>
                                <span class="edit2 input-group-addon">Số tiền</span>
                                <input value="{{old('vipmoney',$cauhinh[12]->chitiet)}}" type="text" name="vipmoney" class="edit2 form-control" placeholder="Link">
                            </div>
                            <div class="tab-pane fade" id="colored-rounded-tab7">
                                <span class="edit2 input-group-addon ">Email</span>
                                <input value="{{old('email_gmail',$cauhinh[13]->chitiet)}}" type="text" name="email_gmail" class="edit2 form-control form-group" placeholder="Link">

                                <span class="edit2 input-group-addon ">Password</span>
                                <input value="{{old('password_gmail',$cauhinh[14]->chitiet)}}" type="text" name="password_gmail" class="edit2 form-control form-group" placeholder="Link">
                            </div>

                            <div class="tab-pane fade " id="colored-rounded-tab8">
                                <div class="col-md-12 row">
                                    <div class="col-md-5">
                                        <input type="hidden" name="hinhcu" value="{{$cauhinh[16]->chitiet}}">
                                        <span class="edit2 input-group-addon ">Hình ảnh đại diện</span>
                                        <input type="file" name="photos" class="edit2 btn btn-primary  ">
                                    </div>
                                    <div class="col-md-7">
                                        <img style="width: 275px;height: 275px" src="{{ asset('image/'.$cauhinh[16]->chitiet) }}" alt="">
                                    </div>
                                    
                                </div>
                                
                                
                            </div> 
                           
                        </div>
                          <div class="d-flex align-items-center form-group">
                            
                            <button name="addProduct" type="submit" class="btn btn-primary ml-3">{{trans('message.btn_luu')}}<i class="icon-paperplane ml-2"></i></button>
                        </div>
                       
                    
                    </div>
                    </form>
                </div>
            </div>

           
        </div>
        <!-- /rounded colored tabs -->
</div>
</div>

@endsection