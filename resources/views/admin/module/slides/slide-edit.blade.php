@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('slide.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_sua')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('slide.update', ['slide' => $slide->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="col-sm-12 border-bottom row form-group" style="padding-bottom: 5px">
                        <div class="col-sm-6 form-group">
                            <label>Hình ảnh tin tức</label>  
                            @if($slide->img == "product_default.jpg")
                                Không có hình ảnh
                            @else
                            <div class="row">
                                @php $arr_img = explode("||",$slide->img);@endphp
                                @for($i=0 ; $i<count($arr_img) ; $i++)
                                 <img style="width: 95px;height: 95px;padding: 5px;margin-right: 10px" class="form-control" src="{{ asset('image/'.$slide->img) }}" alt="">
                                @endfor
                               {{--  <button style="height: 35px" class="btn btn-danger" type="submit" title="Xóa ảnh" name="xoaEdit"> <i class="fa fa-trash-o" aria-hidden="true"></i></button> --}}
                                @endif
                            </div>
                           
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Chọn hình ảnh</label>
                            <input type="file" name="photos[]" multiple="" class="btn btn-primary  ">
                            <input type="hidden" name="hinhcu" value="{{$slide->img}}">
                        </div>
                   
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                       <span class="input-group-addon">Tiêu đề</span>
                                        <input value="{{old('tieude',$slide->tieude)}}" type="text" name="tieude" class="form-control" placeholder="Viết tiêu đề ">
                                    </div>                                   
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                         <span class="input-group-addon">Links</span>
                                        <input value="{{old('link',$slide->link)}}" type="text" name="link" class="form-control" placeholder="Links">
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                        <span class="input-group-addon">Vị trí</span>
                                        <input value="{{old('vitri',$slide->vitri)}}" type="text" name="vitri" class="form-control" placeholder="Vị trí">
                                    </div>                                   
                                </div>                              
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-12">
                        <textarea rows="10" id="editor1" name="chitiet" placeholder="Type the content here!">{{old('chitiet',$slide->chitiet)}}</textarea>                      
                    </div>                               
                    <div class="form-group pt-3">
                        <button name="addTintuc" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection