@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('slide.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Thêm</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_them')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('slide.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                       <span class="input-group-addon">Tiêu đề</span>
                                        <input value="{{old('tieude')}}" type="text" name="tieude" class="form-control" placeholder="Viết tiêu đề ">
                                    </div>                                   
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                         <span class="input-group-addon">Links</span>
                                        <input value="{{old('link')}}" type="text" name="link" class="form-control" placeholder="Links">
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-6">                                    
                                   <div class="form-group">
                                        <span class="input-group-addon">Vị trí</span>
                                        <input value="{{old('vitri')}}" type="text" name="vitri" class="form-control" placeholder="Vị trí">
                                    </div>                                   
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="input-group-addon ">Hình ảnh</span>
                                        <input type="file" name="photos" class="btn btn-primary  ">
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-12">
                        <textarea rows="10" id="editor1" name="chitiet" placeholder="Type the content here!">{{old('chitiet')}}</textarea>                      
                    </div>                               
                    <div class="form-group pt-3">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection