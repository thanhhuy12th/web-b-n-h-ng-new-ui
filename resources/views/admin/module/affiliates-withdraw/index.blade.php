@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('affiliates-withdraw.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<style >
    .well{min-height:20px;padding:19px;background-color:#fdfdfd;border:1px solid #ddd;border-radius:3px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.05);box-shadow:inset 0 1px 1px rgba(0,0,0,.05)}
    .well blockquote{border-color:#ddd;border-color:rgba(0,0,0,.15)}
</style>
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Lịch sử rút tiền Affiliates</h5>
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                  
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                      <!--   <th scope="col"><input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);"></th> -->
                                        <th scope="col">Họ tên</th>
                                        <th scope="col">Điện thoại</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Tổng thu nhập</th>
                                        <th scope="col">Số tiền rút</th>
                                        <th scope="col">Status</th>
                                        <th scope="col" style="text-align: center;">Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($withdrawList as $item)
                                        <tr>
                                           <!--  <td><input value="{{ $item->id }}" type="checkbox" name="checked[]"></td> -->
                                            <td>{{$item->fullname}}</td>
                                            <td>{{$item->phone}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>{{number_format($item->credit_aff,0,'',',')}} đ</td>
                                            <td>{{number_format($item->amount,0,'',',')}} đ</td>
                                            <td style="width: 10%">
                                                {!! $item->status==1 ? '<span class="label label-success">Paid</span>' : '<span class="label label-danger">Unpaid</span>' !!} 
                                            </td>
                                            <td style="text-align: center;">
                                                <a href="{{ route('affiliates-withdraw.edit', ['withdrawList' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                                      
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection