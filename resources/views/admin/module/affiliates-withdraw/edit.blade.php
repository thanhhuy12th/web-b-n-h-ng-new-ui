@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('affiliates-withdraw.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Cập nhật lịch sử rút tiền Affiliates</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{route('affiliates-withdraw.update', ['withdraw' => $withdraw->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group col-sm-12 row">
                    <div class="col-sm-4">
                        <label class="lable_edit">Họ tên *</label>
                        <input value="{{old('fullname',$withdraw->fullname)}}" type="text" name="fullname" class="form-control" disabled="">
                    </div>
                    <div class="col-sm-4">
                        <label class="lable_edit">Số điện thoại *</label>
                        <input value="{{old('phone',$withdraw->phone)}}" type="text" name="phone" class="form-control"  disabled="">
                    </div>
                    <div class="col-sm-4">
                        <label class="lable_edit">Email *</label>
                        <input value="{{old('email',$withdraw->email)}}" type="text" name="email" class="form-control" disabled="">
                    </div>
                </div>
               
                <div class="form-group col-sm-12 row">
                    <div class="col-sm-4">
                        <label class="lable_edit">Thu nhập</label>
                        <input value="{{old('credit_aff',$withdraw->credit_aff)}}" type="text" name="show" class="form-control" disabled="" >
                        <input value="{{old('credit_aff',$withdraw->credit_aff)}}" type="hidden" name="credit_aff" class="form-control" >
                    </div>
                    <div class="col-sm-4">
                      <label class="lable_edit" for="diachi">Số tiền rút</label>
                      <input value="{{old('amount',$withdraw->amount)}}" name="amount" type="text" class="form-control">
                  </div>
                  <div class="col-sm-4">
                    <label class="lable_edit">Trạng thái</label>
                    <select name="status" class="form-control">
                        <option value="0"  {{$withdraw->status == 0 ? 'selected' : ''}} >Unpaid</option>
                        <option value="1"  {{$withdraw->status == 1 ? 'selected' : ''}} >Paid</option>

                    </select>                               
                </div>
            </div>
             <div class="form-group col-sm-12 row">
                <div class="col-sm-12">
                    <label class="lable_edit" for="diachi">Ghi chú</label>
                    <textarea class="form-control" name="note" placeholder="Nội dung">{{old('note',$withdraw->note)}}</textarea>
                </div>
             </div>

                    
                    <div class="form-group">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

@endsection