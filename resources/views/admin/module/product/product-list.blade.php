@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('sanpham.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Danh sách sản phẩm</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                         <a  class="btn btn-success" href="{{ route('sanpham.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                            <th scope="col"><input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);"></th>
                                            <th data-field="id">Mã</th>
                                            <th data-field="company">Hình ảnh</th>
                                            <th data-field="name">Tên sản phẩm</th>
                                            <th data-field="date">Số lượng</th>
                                            <th data-field="email">Danh mục</th>
                                            <th data-field="price">Giá</th>
                                            <th data-field="task">Loại giảm giá</th>
                                            <th scope="col" style="text-align: center;">Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sanpham as $item)
                                        <tr>
                                            <td><input value="{{ $item->id }}" type="checkbox" name="checked[]"></td>
                                            <td>{{$item->id}}</td>
                                            <td style="text-align: center;">
                                                @if($item->img == "")
                                                    Không có hình ảnh
                                                @else
                                                @php $arr_img = explode("||",$item->img);@endphp
                                                    <img style="width: 70px;height: 70px;" src="{{ asset('image/'.$arr_img[0]) }}">
                                                    <input type="hidden" name="qqq" value="{{$arr_img[0]}}">
                                                @endif
                                            </td>
                                            <td>{{$item->tensp}}</td>
                                            <td>{{$item->soluong}}</td>
                                            <td>
                                                @foreach($dmsp as $ctlg)
                                                    @if($ctlg->id == $item->category)
                                                    {{$ctlg->name}}
                                                    @endif
                                                @endforeach
                                            </td>
                                            <td>{{convert_money($item->price)}} VNĐ</td>
                                            <td>
                                                @if($item->type_giamgia==0)
                                                    Giảm theo giá
                                                @else
                                                    Giảm theo sản phẩm
                                                @endif
                                            </td>
                                            <td style="text-align: center;">
                                                <a href="{{ route('sanpham.edit', ['sanpham' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            {{ method_field('DELETE') }}
                           <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                            <div style="position: absolute;right: 20px">
                                {{$sanpham->links()}}
                            </div>
                        </div>
                    
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection