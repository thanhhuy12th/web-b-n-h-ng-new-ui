@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('sanpham.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Thêm</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Thông tin khách hàng</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('sanpham.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                               <span class="input-group-addon">Tên sản phẩm</span>
                                                <input value="{{old('tensp')}}" type="text" name="tensp" class="form-control" placeholder="Tên sản phẩm">
                        </div>
                        <div class="col-sm-6">
                            <span class="input-group-addon">Alias</span>
                                                <input value="{{old('alias')}}" type="text" name="alias" class="form-control" placeholder="Alias">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <span class="input-group-addon">Giá ban đầu</span>
                                                <input value="{{old('price')}}" type="text" name="price" class="form-control" placeholder="Giá ban đầu">
                        </div>
                        <div class="col-sm-6">
                            <span class="input-group-addon">Giá khuyến mãi</span>
                                            <input value="{{old('price_km')}}" type="text" name="price_km" class="form-control" placeholder="Giá khuyến mãi">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                           <span class="input-group-addon">Danh mục</span>
                                                <select name="category" class="form-control pro-edt-select form-control-primary">
                                                     <option value="">Hãy chọn thể loại</option>
                                                    @foreach($dmsp as $ctlg)
                                                    @if($ctlg->id_parent >= 1)
                                                     <option value="{{ $ctlg->id }}" {{ (old('category') == $ctlg->id) ? 'selected' : '' }}>{{$ctlg->name}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                        </div>
                        <div class="col-sm-6">
                             <span class="input-group-addon">Mã giảm giá</span>
                                            <select name="giamgia" class="form-control pro-edt-select form-control-primary">
                                                <option value="">Không chọn mã giảm giá</option>
                                                @foreach($coupon as $cp)
                                                    <option value="{{ $cp->id }}">{{$cp->id}}</option>
                                                @endforeach
                                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                             <span class="input-group-addon ">Hình ảnh đại diện</span>
                                            <input style="padding-bottom: 3px" type="file" name="photos[]" multiple="">
                        </div>
                        <div class="col-sm-6 " >
                            <span class="input-group-addon ">Loại sản phẩm</span>
                            <div class="col-xs-12 row form-group">
                                <div class="col-sm-3 col-xs-3 ">
                                    <input type="radio" name="type_product" value="0"  checked=""> Bình thường <br/>
                                </div>
                                <div class="col-sm-3 col-xs-3 ">
                                    <input type="radio" name="type_product" value="1"  > Bán chạy <br/>
                                </div>
                                <div class="col-sm-3 col-xs-3 ">
                                    <input type="radio" name="type_product" value="2" > Bán nhiều<br/>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                         <div class="card-body">
                            <ul class="nav nav-tabs nav-tabs-solid bg-slate border-0 nav-tabs-component rounded">
                                <li class="nav-item"><a href="#colored-rounded-tab1" class="nav-link active" data-toggle="tab">Mô tả ngắn</a></li>
                                <li class="nav-item"><a href="#colored-rounded-tab2" class="nav-link" data-toggle="tab">Nội dung</a></li>
                            </ul>                   
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="colored-rounded-tab1">
                                     <textarea rows="10" id="editor1" name="chitietngan" class="form-control col-md-12 col-xs-12" >{{old('chitietngan')}}</textarea>
                                </div>
                                 <div class="tab-pane fade" id="colored-rounded-tab2">
                                    <textarea rows="10" id="editor2" name="information" class="form-control col-md-12 col-xs-12" >{{old('information')}}</textarea>                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection
                   