@extends('admin.welcome')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('cauhinh-affiliates.editAffiliatesNoId')}}" class="breadcrumb-item">Cấu hình</a>
                <span class="breadcrumb-item active">Cập nhật</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <!-- Rounded colored tabs -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">{{trans('message.cauhinh')}} (Chiết khấu hoa hồng mặc định)</h6>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                     <form action="{{ route('cauhinh-affiliates.updateAffiliatesNoId') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                    <div class="card-body">
                        <div class="form-group col-sm-12 row">
                             <div class="col-sm-6">
                                    <label class="lable_edit">Hoa hồng cấp 1</label>
                                                <input value="{{old('percent_lv1',$cauhinh[0]->chitiet)}}" type="text" name="percent_lv1" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                                    <label class="lable_edit">Hoa hồng cấp 2</label>
                                                <input value="{{old('percent_lv2',$cauhinh[1]->chitiet)}}" type="text" name="percent_lv2" class="form-control"  >
                        </div>
                        </div>
                       
                          <div class="d-flex align-items-center form-group">
                            
                            <button name="addProduct" type="submit" class="btn btn-primary ml-3">{{trans('message.btn_luu')}}<i class="icon-paperplane ml-2"></i></button>
                        </div>
                       
                    
                    </div>
                    </form>
                </div>
            </div>

           
        </div>
        <!-- /rounded colored tabs -->
</div>
</div>

@endsection