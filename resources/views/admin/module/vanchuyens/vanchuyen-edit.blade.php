@extends('admin.welcome')
@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.btn_sua')}}</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{ route('vanchuyen.update', ['vanchuyen' => $vanchuyen->id]) }}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                          <span class="input-group-addon">Tên</span>
                                            <input value="{{old('name',$vanchuyen->name)}}" type="text" name="name" class="form-control" placeholder="Nhập tên">
                        </div>
                        <div class="col-sm-6">
                            <span class="input-group-addon">Giá</span>
                                            <input value="{{old('gia',$vanchuyen->gia)}}" type="text" name="gia" class="form-control" placeholder="Nhập giá">
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <button name="addTintuc" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
@endsection