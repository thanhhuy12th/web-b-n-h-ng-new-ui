@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('affiliates.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Chi tiết</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<style >
    .well{min-height:20px;padding:19px;background-color:#fdfdfd;border:1px solid #ddd;border-radius:3px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.05);box-shadow:inset 0 1px 1px rgba(0,0,0,.05)}
    .well blockquote{border-color:#ddd;border-color:rgba(0,0,0,.15)}
</style>
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Danh sách Affiliates</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="row">
                        <div class="col-1">
                            <a onclick="CopyToClipboard('link')" class="btn btn-warning" style="cursor: pointer;color: #fff;left: 10px;position: absolute;">Copy</a> 
                        </div>
                        <div class="col-11" style="padding-left: 0px">
                            <div class="form-group form-control" id="link">http://localhost/web-b-n-h-ng-new-ui/?res={{$aff_code_lvl_0->aff_code}}</div>
                        </div>
                    </div>
                    <div class="row form-group" style="text-align: center;padding-left: 0px">
                        <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">
                            <button class="btn btn-info" style="width: 95%">Mã cộng tác viên: {{$aff_code_lvl_0->aff_code}}</button>
                        </div>
                        <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">
                            <button class="btn btn-info" style="width: 95%">Tổng thu nhập: {{convert_money($aff_code_lvl_0->credit_aff)}} đ</button>
                        </div>
                    </div>
                    <div class="row form-group" style="text-align: center;padding-left: 0px;margin-top: 10px">
                        <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">
                            <?php
                                $count = 0;
                                for( $j = 0; $j < count($getmember) ; $j++){
                                    $count++;
                                }
                                       
                            ?>
                            <button class="btn btn-info" style="width: 95%">Tổng đăng ký: {{$count}}</button>
                        </div>
                        <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">
                            <?php
                                $count2=0;
                                
                                for( $i = 0; $i < count($getmember) ; $i++){
                                    for( $j = 0; $j < count($donhang) ; $j++){
                                        if($getmember[$i]['email'] == $donhang[$j]->email){
                                           $count2++;
                                        }
                                    }
                                }
                            ?>
                            <button class="btn btn-info" style="width: 95%">Tổng hóa đơn: {{$count2}}</button>
                        </div>
                    </div>
                    <ul class="nav nav-tabs nav-tabs-solid bg-slate border-0 nav-tabs-component rounded">
                            <li class="nav-item"><a href="#colored-rounded-tab1" class="nav-link active" data-toggle="tab">Đơn hàng lvl 1</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab2" class="nav-link" data-toggle="tab">Đơn hàng lvl 2</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab3" class="nav-link" data-toggle="tab">Khách hàng lvl 1</a></li>
                            <li class="nav-item"><a href="#colored-rounded-tab4" class="nav-link" data-toggle="tab">Khách hàng lvl 2</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="colored-rounded-tab1">
                            <div class="table-responsive-xl">
                                <table class="table table-sm table-hover ">
                                  <thead>
                                    <tr>
                                      <th scope="col">Mã</th>
                                      <th scope="col">Họ tên</th>
                                      <th scope="col">Số tiền</th>
                                      <th scope="col">Điện thoại</th>
                                      <th scope="col">Trạng thái</th>
                                      <th scope="col">Thời gian</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                        @for( $j = 0; $j < count($donhang) ; $j++)
                                            @for( $i = 0; $i < count($arr_member_lvl_1) ; $i++)
                                                @if($arr_member_lvl_1[$i]->email == $donhang[$j]->email)
                                                    <tr>
                                                        <td>HD{{$donhang[$j]->id}}</td>
                                                        <td>{{$donhang[$j]->hoten}}</td>
                                                        <td>{{$donhang[$j]->tongtien}}</td>
                                                        <td>{{$donhang[$j]->sodt}}</td>
                                                        <td>{{$donhang[$j]->status_paid == 1 ? 'Đã thanh toán' : 'Chưa thanh toán'}}</td>
                                                        <td>{{$donhang[$j]->created_at}}</td>
                                                    </tr>
                                                @endif
                                            @endfor
                                        @endfor
                                  </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="colored-rounded-tab2">
                            <div class="table-responsive-xl">
                                <table class="table table-sm table-hover ">
                                  <thead>
                                    <tr>
                                      <th scope="col">Mã</th>
                                      <th scope="col">Họ tên</th>
                                      <th scope="col">Số tiền</th>
                                      <th scope="col">Điện thoại</th>
                                      <th scope="col">Trạng thái</th>
                                      <th scope="col">Thời gian</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                        @for( $j = 0; $j < count($donhang) ; $j++)
                                            @for( $i = 0; $i < count($arr_member_lvl_2) ; $i++)
                                                @if($arr_member_lvl_2[$i]['email'] == $donhang[$j]->email)
                                                    <tr>
                                                        <td>HD{{$donhang[$j]->id}}</td>
                                                        <td>{{$donhang[$j]->hoten}}</td>
                                                        <td>{{$donhang[$j]->tongtien}}</td>
                                                        <td>{{$donhang[$j]->sodt}}</td>
                                                        <td>{{$donhang[$j]->status_paid == 1 ? 'Đã thanh toán' : 'Chưa thanh toán'}}</td>
                                                        <td>{{$donhang[$j]->created_at}}</td>
                                                    </tr>
                                                @endif
                                            @endfor
                                        @endfor
                                  </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="colored-rounded-tab3">
                            <div class="table-responsive-xl">
                                <table class="table table-sm table-hover">
                                  <thead>
                                    <tr>
                                      <th scope="col">Mã</th>
                                      <th scope="col">Họ tên</th>
                                      <th scope="col">Điện thoại</th>
                                      <th scope="col">Email</th>
                                      <th scope="col">Thời gian</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @for( $j = 0; $j < count($arr_member_lvl_1) ; $j++)
                                        <tr>
                                            <td>KH{{$arr_member_lvl_1[$j]->id}}</td>
                                              <td>{{$arr_member_lvl_1[$j]->fullname}}</td>
                                              <td>{{$arr_member_lvl_1[$j]->phone}}</td>
                                              <td>{{$arr_member_lvl_1[$j]->email}}</td>
                                              <td>{{$arr_member_lvl_1[$j]->updated_at}}</td>
                                        </tr>
                                    @endfor
                                  </tbody>
                                </table>
                                {{-- {{$arr_member_lvl_1->links()}} --}}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="colored-rounded-tab4">
                            <div class="table-responsive-xl">
                                <table class="table table-sm table-hover">
                                  <thead>
                                    <tr>
                                      <th scope="col">Mã</th>
                                      <th scope="col">Họ tên</th>
                                      <th scope="col">Điện thoại</th>
                                      <th scope="col">Email</th>
                                      <th scope="col">Thời gian</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @for( $i = 0; $i < count($arr_member_lvl_2) ; $i++)
                                        @for( $j = 0; $j < count($member) ; $j++)
                                            @if($arr_member_lvl_2[$i]['email'] == $member[$j]->email)
                                                <tr>
                                                    <td>KH{{$member[$j]->id}}</td>
                                                      <td>{{$member[$j]->fullname}}</td>
                                                      <td>{{$member[$j]->phone}}</td>
                                                      <td>{{$member[$j]->email}}</td>
                                                      <td>{{$member[$j]->updated_at}}</td>
                                                </tr>
                                            @endif
                                        @endfor
                                    @endfor
                                  </tbody>
                                </table>
                                {{-- {{$arr_member_lvl_2->links()}} --}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
     function CopyToClipboard(containerid) {
        if (document.selection) { 
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("copy");   

        } else if (window.getSelection) {
            var range = document.createRange();
             range.selectNode(document.getElementById(containerid));
             window.getSelection().addRange(range);
             document.execCommand("copy");
             alert("Text copied") 
        }
    }
    $(document).ready(function () {
    $('.switch-light #upstatus').on('click',function () {
        var Id = $(this).data('id');
        var status = this.checked;
        $.ajax({
            type: "POST",
            url: "../api/change-status-affilate",
            data: {
                id: Id,
                status: status
            },
            success: function (result) {  
            },
            error: function () {
                
            }
        })
    });
});
</script> 
@endsection