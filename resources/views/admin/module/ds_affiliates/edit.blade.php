@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('affiliates.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    {{-- @include('admin.blocks.alert') --}}
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Cập nhật Affiliates</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{route('affiliates.update', ['affiliates' => $affiliates->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Họ tên *</label>
                                                <input value="{{old('fullname',$affiliates->fullname)}}" type="text" name="fullname" class="form-control" disabled="">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Số điện thoại *</label>
                                                <input value="{{old('phone',$affiliates->phone)}}" type="text" name="phone" class="form-control"  disabled="">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Email *</label>
                                                <input value="{{old('email',$affiliates->email)}}" type="text" name="email" class="form-control" disabled="">
                        </div>
                        <div class="col-sm-6">
                              <label class="lable_edit" for="diachi">Aff_code</label>
                                                <input value="{{$affiliates->aff_code}}" name="aff_code" type="text" placeholder="Affiliates code" class="form-control" disabled="">
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Hoa hồng cấp 1</label>
                                                <input value="{{old('percent_lv1',$affiliates->percent_lv1)}}" type="text" name="percent_lv1" class="form-control" placeholder="Phần trăm hoa hồng" >
                        </div>
                        <div class="col-sm-6">
                              <label class="lable_edit" for="diachi">Hoa hồng cấp 2</label>
                                                <input value="{{old('percent_lv2',$affiliates->percent_lv2)}}" name="percent_lv2" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Doanh thu hoa hồng</label>
                                                <input value="{{old('credit_aff',$affiliates->credit_aff)}}" type="text" name="credit_aff" class="form-control" placeholder="" disabled="" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Trạng thái</label>
                            	<select name="status" class="form-control">
                                    <option value="3"  {{$affiliates->status == 3 ? 'selected' : ''}} >Duyệt</option>
                            		<option value="2" {{$affiliates->status == 2 ? 'selected' : ''}} >Hủy bỏ</option>
                            
                            	</select>                               
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

@endsection