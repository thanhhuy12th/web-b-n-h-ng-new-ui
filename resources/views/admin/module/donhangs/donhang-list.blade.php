@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('donhang.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('donhang.index')}}" class="breadcrumb-item">Danh sách</a>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.danhsach_donhang')}}</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                        <a class="btn btn-success" href="{{ route('donhang.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </td>
                                        <th>Đơn hàng</th>
                                        <th>Ngày đặt</th>
                                        <th>Khách hàng</th>
                                        <th>Thanh toán</th>
                                        <th>Giao hàng</th>
                                        <th>Tổng tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($donhang as $item)
                                        <tr style="vertical-align: top">
                                            
                                            <td style="width: 4%">
                                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                            </td>
                                            <td style="width: 12%">
                                                <a href="{{ route('donhang.edit', ['donhang' => $item->id]) }}">#{{$item->id}}</a></td>
                                            <td style="width: 18%">
                                                <?php
                                                    $date = date_create($item->created_at);
                                                    echo date_format($date, 'd-m-Y H:i');
                                                ?>
                                            </td>
                                            <td style="width: 18%">{{$item->hoten}}</td>
                                            <td style="width: 16%">
                                                @if($item->status_paid == 0)
                                                <span class="chuatt">Chưa thanh toán</span>
                                                @else
                                                <span style="font-weight: bold;">Đã thanh toán</span>
                                                @endif
                                            </td>
                                            <td style="width: 16%">
                                                @if($item->status_move == 0)
                                                <span class="chuatt">Chưa giao hàng</span>
                                                @else
                                                <span style="font-weight: bold;">Đã giao hàng</span>
                                                @endif
                                            </td>
                                            <td style="width: 16%">
                                                <?php echo convert_money($item->tongtien)." VNĐ";?>
                                            </td>                 
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            {{ method_field('DELETE') }}
                           <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="btn btn-danger">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </form>
                     {{$donhang->links()}}
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection