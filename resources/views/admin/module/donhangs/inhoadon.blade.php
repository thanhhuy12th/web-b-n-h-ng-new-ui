<!DOCTYPE html>
<html lang="vi">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
  <button class="noprint" style="padding: 5px;background: #1C6216;color: #fff;font-size: 18px" onclick="window.print();">IN ĐƠN HÀNG</button>
  <style type="text/css">
    @media print {
      /* style sheet for print goes here */
      .noprint {
        visibility: hidden;
      }
    }
  </style>
  <div class="printer-preview-content">
    <!-- Winter Breeze -->
    <!-- Common Base Styling -->

    <!-- Black text color -->


    <!-- Grey text color -->

    <script type="text/javascript">
      $.get("https://fonts.googleapis.com/css?family=Lato:300,400,700").done(function(response) {
        var pFontCss = response.replace(/ local\(.+?\)\,/g,"");
        var pStyle = document.createElement('style');
        var pHead = document.head || document.getElementsByTagName('head')[0];
        pStyle.type = 'text/css';
        pStyle.id = 'font-primary-19951';
        if (pStyle.styleSheet){
          pStyle.styleSheet.cssText = pFontCss;
        } else {
          pStyle.appendChild(document.createTextNode(pFontCss));
        }
        pHead.appendChild(pStyle);
      });
      
      $.get("https://fonts.googleapis.com/css?family=Tahoma:300,400,700").done(function(response) {
        var sFontCss = response.replace(/ local\(.+?\)\,/g,"");
        var sStyle = document.createElement('style');
        var sHead = document.head || document.getElementsByTagName('head')[0];
        sStyle.type = 'text/css';
        sStyle.id = 'font-secondary-19951';
        if (sStyle.styleSheet){
          sStyle.styleSheet.cssText = sFontCss;
        } else {
          sStyle.appendChild(document.createTextNode(sFontCss));
        }
        sHead.appendChild(sStyle);
      });
      
    </script>
    <script>
      $.ajaxSetup({
        cache: true
      });
      var url = "https://orderprintertemplates.com/JsBarcode.all.min.js";
      var options = {
        format: "CODÈ8",
        width: 1,
        height: 18,
        quite: 1,
        lineColor: "#000",
      displayValue: false, //Show the text below the barcode (can be true or false)
      fontSize: 12,
      font: "Lato",
      textAlign: "left"
    }
    $.getScript( url, function() {
      // Barcode Generator for Order Number
      $(".barcode-1982").each(function(){
        $(this).JsBarcode("#1982",options);
      });
    });
  </script>
  <script>
    $(function () {
     if (navigator.userAgent.indexOf('Safari') != -1 &&
       navigator.userAgent.indexOf('Chrome') == -1) {
       $("body").addClass("safari");
   }
 });
</script>
<style type="text/css">
  /* ### BASE - PAGE SIZING AND MARGIN SETUP NORMALIZATION ACROSS BROWSERS ### */
  @page {
    margin: 12mm !important;
    margin-top: 12mm !important;
    margin-right: 12mm !important;
    margin-bottom: 12mm !important;
    margin-left: 12mm !important;
  }
  @media print {
    .safari {
      padding-top: 15px;
    }
    * {
      overflow: visible !important;
    }
    .printer-preview-content .printer-preview-content {
      padding-right: 10px;
    }
    /*    .printer-inline-preview .printer-preview-content .printer-preview-content {
          page-break-before: always;
        }
        .printer-inline-preview-first .printer-preview-content .printer-preview-content {
          page-break-before: avoid !important;
        }*/
      }
      @media screen {
        .printer-preview-content {
          padding: 18px 0;
          min-height: 800px;
          /*background-image: url(http://basehold.it/i/18)*/
        }
        .printer-preview-content .printer-preview-content {
          margin-right: auto;
          margin-left: auto;
          max-width: 620px;
        }
      }
      @media screen,print {
        
        /* ### BASE - TYPOGRAPHY AND REMOVAL OF STANDARD SHOPIFY STYLING ### */
        .printer-preview-content .t19951 * {
          color: #444;
          font-family: 'Tahoma';
          font-size: 16px;
          font-weight: 400;
          line-height: 18px;
          text-rendering: optimizeLegibility;
          margin: 0 0 0 0;
          padding: 0 0 0 0;
          -webkit-print-color-adjust: exact;
          overflow: visible !important;
        }
        .printer-preview-content .t19951 h1 {
          font-size: 28px;
          line-height: 36px;
        }
        .printer-preview-content .t19951 h2 {
          font-size: 18px;
          line-height: 36px;
        }
        .printer-preview-content .t19951 h3 {
          font-size: 14px;
          line-height: 18px;
        }
        .printer-preview-content .t19951 h1,
        .printer-preview-content .t19951 h2,
        .printer-preview-content .t19951 h3 {
          margin-bottom: 18px;
        }
        .printer-preview-content .t19951 b,
        .printer-preview-content .t19951 b * {
          font-weight: bold;
        }
        .printer-preview-content .t19951 .text-right {
          text-align: right;
        }
        .printer-preview-content .t19951 .text-center {
          text-align: center;
        }
        .printer-preview-content .t19951 .no-wrap {
          white-space: nowrap;
        }
        
        /* ### BASE - GRID AND RE-USABLE LAYOUT COMPONENTS ### */
        .printer-preview-content .t19951 .row {
          width: 100%;
          display: block;
          clear: both;
        }
        .printer-preview-content .t19951 .row:after {
          content: "";
          display: table;
          clear: both;
        }
        .printer-preview-content .t19951 .col-xs-1,
        .printer-preview-content .t19951 .col-xs-2,
        .printer-preview-content .t19951 .col-xs-3,
        .printer-preview-content .t19951 .col-xs-4,
        .printer-preview-content .t19951 .col-xs-5,
        .printer-preview-content .t19951 .col-xs-6,
        .printer-preview-content .t19951 .col-xs-7,
        .printer-preview-content .t19951 .col-xs-8,
        .printer-preview-content .t19951 .col-xs-9,
        .printer-preview-content .t19951 .col-xs-10,
        .printer-preview-content .t19951 .col-xs-11,
        .printer-preview-content .t19951 .col-xs-12 {
          float: left;
          min-height: 1px;
          margin-bottom: 36px;
        }
        .printer-preview-content .t19951 .col-xs-12 {
          width: 100%;
        }
        .printer-preview-content .t19951 .col-xs-11 {
          width: 91.66666667%;
        }
        .printer-preview-content .t19951 .col-xs-10 {
          width: 83.33333333%;
        }
        .printer-preview-content .t19951 .col-xs-9 {
          width: 75%;
        }
        .printer-preview-content .t19951 .col-xs-8 {
          width: 66.66666667%;
        }
        .printer-preview-content .t19951 .col-xs-7 {
          width: 58.33333333%;
        }
        .printer-preview-content .t19951 .col-xs-6 {
          width: 50%;
        }
        .printer-preview-content .t19951 .col-xs-5 {
          width: 41.66666667%;
        }
        .printer-preview-content .t19951 .col-xs-4 {
          width: 33.33333333%;
        }
        .printer-preview-content .t19951 .col-xs-3 {
          width: 25%;
        }
        .printer-preview-content .t19951 .col-xs-2 {
          width: 16.66666667%;
        }
        .printer-preview-content .t19951 .col-xs-1 {
          width: 8.33333333%;
        }
        .printer-preview-content .t19951 .col-no-margin {
          margin-bottom: 0 !important;
        }
        .printer-preview-content .t19951 .clear-fix {
          clear: both;
        }
        .printer-preview-content .t19951 .half-margin-top {
          margin-top: 9px !important;
        }
        .printer-preview-content .t19951 .half-margin-bottom {
          margin-bottom: 9px !important;
        }
        .printer-preview-content .t19951 .margin-top {
          margin-top: 18px !important;
        }
        .printer-preview-content .t19951 .margin-bottom {
          margin-bottom: 18px !important;
        }
        .printer-preview-content .t19951 .double-margin-top {
          margin-top: 36px !important;
        }
        .printer-preview-content .t19951 .double-margin-bottom {
          margin-bottom: 36px !important;
        }
        
        /* ### BASE - TABLE STYLING ### */
        .printer-preview-content .t19951 table,
        .printer-preview-content .t19951 .table {
          width: 100%;
          max-width: 100%;
          background-color: transparent;
          border-collapse: collapse;
        }
        .printer-preview-content .t19951 table thead {
          display: table-row-group;
        }
        .printer-preview-content .t19951 table tbody tr {
          page-break-inside:avoid !important;
          page-break-after:auto !important;
        }
        .printer-preview-content .t19951 table tbody tr td {
          page-break-inside:avoid !important;
        }
        .printer-preview-content .t19951 th {
          white-space: nowrap;
          text-align: left;
          vertical-align: middle;
          border-top: 0;
          border-bottom: 0;
        }
        .printer-preview-content .t19951 td {
          vertical-align: middle;
          border-top: 0;
          border-bottom: 0;
        }
        .printer-preview-content .t19951 .order-table tbody > tr td {
          background-color: #FFFFFF;
        }
        .printer-preview-content .t19951 .pricing-table {
          position: relative;
          top: 1px;
        }
        .printer-preview-content .t19951 .pricing-table tbody > tr > td {
          background-color: #FFFFFF;
        }
        .printer-preview-content .t19951 th.order-table-qty {
          width: 10%;
        }
        .printer-preview-content .t19951 th.order-table-price {
          width: 10%;
        }
        .printer-preview-content .t19951 th.order-table-item-total,
        .printer-preview-content .t19951 th.order-table-reason {
          width: 15%;
        }
        .printer-preview-content .t19951 th.order-table-return-comments {
          width: 30%;
        }
        .printer-preview-content .t19951 .pricing-table-text {
          text-align: right;
          white-space: nowrap;
        }
        
        /* ### BASE - PRODUCT IMAGE SIZING TO RETAIN LINE HEIGHT RYTHM) ### */
        .printer-preview-content .t19951 .product-image-wrapper {
          width: 54px;
          max-height: 54px;
        }
        .printer-preview-content .t19951 .product-image {
          padding: 0 4px;
          page-break-inside:avoid !important;
          max-width: 54px;
        }
        
        /* ### BASE - LISTS RESET ### */
        .printer-preview-content .t19951 ul {
          list-style: none;
        }
        .printer-preview-content .t19951 li {
          color: #444;
          list-style: none;
        }
        
        /* ### BASE - LOGO ### */
        .printer-preview-content .t19951 .logo-wrapper {
          display: inline-block;
          width: 100%;
        }
        .printer-preview-content .t19951 .logo {
          float: left;
          padding-right: 15px;
          max-width: 100%;
          max-height: 122.0px;
        }
        
        /* ### BASE - BARCODE ### */
        .printer-preview-content .t19951 .order-number-barcode,
        .printer-preview-content .t19951 .product-barcode {
          display: block;
        }
        
        /* ### BASE - ADDITONAL COMMON SHARED TYPOGRAPHY AND ALIGNMENT ### */
        .printer-preview-content .t19951 .address {
          margin-right: 5%;
        }
        .printer-preview-content .t19951 .notes-title,
        .printer-preview-content .t19951 .return-codes {
          margin: 9px 0;
        }
        /* ### BASE - HORIZONTAL RULE MARKS ### */
        .printer-preview-content .t19951 hr {
          background: #èc00;
          border-top: 0;
          border: 0;
          height: 2px;
          width: 100%;
          margin-bottom: -2px;
        }

      }
    </style>
    <!-- Template Specific Styling -->
    <style type="text/css">
      @media screen,print {
        
        /* ### DESIGN SPECIFIC - TYPOGRAPHY ### */
        .printer-preview-content .t19951 h1 {
          font-family: 'Tahoma';
          font-weight: bold;
          color: #èc00;
        }
        .printer-preview-content .t19951 h2 {
          font-family: 'Tahoma';
        }
        .printer-preview-content .t19951 h3 {
          font-family: 'Tahoma';
          color: #èc00;
        }
        
        /* ### DESIGN SPECIFIC - LOGO POSITIONING ### */
        .printer-preview-content .t19951 .logo {
          float: right;
          padding-right: 0;
          margin-bottom: 18px;
        }
        
        /* ### DESIGN SPECIFIC - STORE DETAILS ### */
        .printer-preview-content .t19951 .shop-block {
          text-align: center;
        }
        .printer-preview-content .t19951 .shop-block .shop-address-block b,
        .printer-preview-content .t19951 .shop-block .shop-address-block b * {
          color: #000;
        }
        .printer-preview-content .t19951 .shop-block .shop-domain {
          color: #èc00;
        }
        
        .printer-preview-content .t19951 .shop-return-address b,
        .printer-preview-content .t19951 .shop-return-address b * {
          color: #000;
        }
        
        /* ### DESIGN SPECIFIC - ORDER DETAILS ### */
        .printer-preview-content .t19951 .order-details-title {
          display: inline-block;
          font-weight: bold;
          color: #000;
        }
        
        /* ### DESIGN SPECIFIC - BILL TO AND SHIP TO ADDRESS ### */
        .printer-preview-content .t19951 .address-title {
          font-weight: bold;
          color: #000;
        }
        
        /* ### DESIGN SPECIFIC - TABLE HEADER ### */
        .printer-preview-content .t19951 .order-table thead tr th {
          padding-left: 4px;
          padding-right: 4px;
          border-top: 2px solid #èc00;
          padding-top: 7px;
          border-bottom: 2px solid #d3d3d3;
          padding-bottom:7px;
          font-weight: bold;
          color: #èc00;
        }
        
        /* ### DESIGN SPECIFIC - TABLE BODY (SHARED) ### */
        .printer-preview-content .t19951 .order-table tbody tr td,
        .printer-preview-content .t19951 .pricing-table tbody tr td {
          padding-left: 4px;
          padding-right: 4px;
          border-top: 0;
          padding-top: 9px;
          border-bottom: 1px solid #d3d3d3;
          padding-bottom:8px;
        }
        /* ### DESIGN SPECIFIC - ORDER TABLE SPECIFIC ### */
        .printer-preview-content .t19951 .order-table tbody > tr > td.line-item-description,
        .printer-preview-content .t19951 .order-table tbody > tr > td.line-item-qty {
          font-weight: bold;
          color: #000;
        }
        .printer-preview-content .t19951 .order-table tbody > tr > td.line-item-description p.line-item-sku {
          font-weight: normal;
        }
        /* ### DESIGN SPECIFIC - PRICING TABLE SPECIFIC ### */
        .printer-preview-content .t19951 .pricing-table tbody > tr.pricing-table-total-row > td {
          border-top: 2px solid #èc00;
          border-bottom: 2px solid #èc00;
          padding-bottom:7px;
        }
        .printer-preview-content .t19951 .pricing-table tbody > tr > td.pricing-table-title,
        .printer-preview-content .t19951 .pricing-table tbody > tr > td.pricing-table-title span {
          font-weight: bold;
          color: #000;
        }
        .printer-preview-content .t19951 .pricing-table tbody > tr.pricing-table-total-row .pricing-table-title,
        .printer-preview-content .t19951 .pricing-table tbody > tr.pricing-table-total-row .pricing-table-text {
          font-weight: bold;
          color: #000;
        }

        /* ### DESIGN SPECIFIC - ORDER NOTES ### */
        .printer-preview-content .t19951 .notes-title {
          font-weight: bold;
          color: #000;
        }

        /* ### DESIGN SPECIFIC - RETURN FORM REASON CODES ### */
        .printer-preview-content .t19951 .return-codes b,
        .printer-preview-content .t19951 .return-code b * {
          font-weight: bold;
          color: #000;
        }

        /* ### DESIGN SPECIFIC - THANK YOU MESSAGE ### */
        .printer-preview-content .t19951 .thanks-text {
          text-align: center;
          font-weight: bold;
          color: #èc00;
        }
        .printer-preview-content .t19951 .thanks-text * {
          font-weight: bold;
          color: #èc00;
        }

        /* ### DESIGN SPECIFIC - GIFT MESSAGE ### */
        .printer-preview-content .t19951 .gift-text {
          text-align: center;
          font-weight: bold;
          width: 80%;
          margin: 0 10%;
          padding-bottom:18px;
          font-size: 18px;
        }
        .printer-preview-content .t19951 .gift-text * {
          font-weight: bold;
          font-size: 18px;
        }
        .printer-preview-content .t19951 .gift-text svg {
          padding-bottom:9px;
        }

        /* ### DESIGN SPECIFIC - TERMS AND CONDITIONS ### */
        .printer-preview-content .t19951 .terms-text {
          font-size: 11px;
          text-align: center;
        }
        .printer-preview-content .t19951 .terms-text * {
          font-size: 11px;
        }

        /* ### DESIGN SPECIFIC - SOCIAL ICONS ### */
        .printer-preview-content .t19951 .social-icons {
          display: inline;
          width: 20px;
          margin: 9px 4px;
        }

      }
    </style>
    <div class="printer-preview-content" contenteditable="true" spellcheck="false" title="Click to edit text (changes will be printed but not saved)">
      <div class="t19951">
        <div class="row">
          <div id="header-row">
            <div class="col-xs-6">
              <div class="template-title">
                <h1 class="editable" data-key="template_type_name">HÓA ĐƠN</h1>
              </div>
              <ul class="order-details">
                <li class="order-details-invoice">
                  <span class="order-details-title editable" data-key="invoice_number">Mã đơn hàng .</span>
                  <span class="order-details-text">#{{$donhang->id}}</span>
                </li>
                <img class="barcode-1982 order-number-barcode" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFwAAAASCAYAAADWvblEAAAA00lEQVRYR+2ZQRKAIAwD5f+P1kEoxNiOB6/rBQWKNANpCu0YzznLNt97GfXxPqvuvlm7jlH1jXq173Xx36xd29xObbMx3Tf9Dlv1M3xTH93G56v4BAY+r4VN5TiA70UF4LIjWeFCN05Jvt0yKoNSDMAMxIo7AXzExkcMgsOf4oCg6SvEVBUcDodHGFpUAqUsSN75BJQCpezIW6kQz7yq7BBZKKuJTJNM82ZezlISEKCUEZX1IJDDq6lUnDpRKagUVArn4cmlSRZgv+Spc251sfAL8As0vMIissKKJgAAAABJRU5ErkJggg==">
                <li class="order-details-date">
                  <span class="order-details-title editable" data-key="date">Ngày in hóa đơn &nbsp;</span>
                  <span class="order-details-text"><?php echo now(); ?></span>
                </li>
                <li class="order-details-payment">
                  <span class="order-details-title editable" data-key="payment_method">Thanh toán&nbsp;</span>
                  <span class="order-details-text">



                    Cash on Delivery (COD)</span>
                  </li>
                  <li class="order-details-shipping">
                    <span class="order-details-title editable" data-key="shipping_method">Vận chuyển&nbsp;</span>
                    <span class="order-details-text">Miễn phí vận chuyển</span>
                  </li>
                </ul>
              </div>
              <div class="col-xs-6 col-no-margin">
                <div class="logo-wrapper">
                  <img alt="Logo" class="logo" src="{{ asset('image/'.$logo) }}" id="logo" alt="LOGO"/>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-2">
              <div class="address-title bill-to-title editable" data-key="bill_to">Địa chỉ khách hàng</div>
            </div>
            <div class="col-xs-4">
              <ul class="address">
                @if(isset($member))
                <li>{{$member->fullname}}</li>

                <li>{{$member->diachi}}</li>

                <li><?php echo getWard($member->ward); ?><?php echo getDistrict($member->district); ?> <?php echo getProvince($member->province); ?></li>

                <li><span class="editable" data-key="tel">Số điện thoại:</span> {{$member->phone}}</li>
                @else
                <li>{{$donhang->hoten}}</li>

                <li>{{$donhang->diachi}}</li>

                <li><?php echo getWard($donhang->ward); ?><?php echo getDistrict($donhang->district); ?> <?php echo getProvince($donhang->province); ?></li>

                <li><span class="editable" data-key="tel">Số điện thoại:</span> {{$donhang->sodt}}</li>
                @endif
              </ul>
            </div>
            <div class="col-xs-2">

              <div class="address-title ship-to-title editable" data-key="ship_to">Địa chỉ giao hàng</div>
              
            </div>
            <div class="col-xs-4">
              <ul class="address">

                <li>{{$donhang->hoten}}</li>

                <li>{{$donhang->diachi}}</li>
                <li><?php echo getWard($donhang->ward); ?><?php echo getDistrict($donhang->district); ?> <?php echo getProvince($donhang->province); ?></li>

                <li><span class="editable" data-key="tel">Số điện thoại:</span> {{$donhang->sodt}}</li>

              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-no-margin">
              <table class="order-table table">
                <thead>
                  <tr>
                    <th colspan="2" class="order-table-title editable" data-key="item">Mô tả sản phẩm</th>
                    <th class="order-table-qty text-center editable" data-key="qty">Số lượng</th>
                    <th class="order-table-price text-right editable" data-key="price">Giá</th>
                    <th class="order-table-item-total text-right editable" data-key="item_total">Thành Tiền</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($sanpham as $sp)
                  <tr>
                    @foreach($sanphamimg as $spimg)
                      @if($sp->id_sanpham == $spimg->id)
                      @php $arr_img = explode("||",$spimg->img);@endphp
                        <td class="product-image-wrapper"><img class="product-image" src="{{ asset('image/'.$arr_img[0]) }}"></td>
                      @endif
                    @endforeach
                      <td class="line-item-description">{{$sp->ten_sp}}</td>
                      <td class="text-center line-item-qty">× {{$sp->soluong}}</td>
                      <td class="text-right no-wrap line-item-price">{{convert_money($sp->price)}} VNĐ</td>
                      <td class="text-right no-wrap line-item-line-price"> {{convert_money($sp->price*$sp->soluong)}} VNĐ</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="notes">
                <div class="notes-title editable" data-key="order_notes">Note:</div>
                <div class="notes-text">

                </div>
              </div>
            </div>
            <div class="col-xs-4">
                

              <table class="pricing-table table">
                <tbody>
                  

                  <tr>
                    <td class="pricing-table-title editable" data-key="subtotal">Tổng cộng</td>
                    <td class="pricing-table-text">{{convert_money($donhang->tongtien+$donhang->tonggiamgia)}} VNĐ</td>
                  </tr>
                  <tr>
                    <td class="pricing-table-title editable" data-key="shipping_handling">Vận chuyển</td>
                    <td class="pricing-table-text">{{convert_money($donhang->phivanchuyen)}} VNĐ</td>
                  </tr>
                  <tr>
                  <td class="pricing-table-title editable" data-key="shipping_handling">Giảm giá</td>
                    <td class="pricing-table-text">{{convert_money($donhang->tonggiamgia)}} VNĐ</td>
                  </tr>
                  <tr class="pricing-table-total-row">
                    <td class="pricing-table-title editable" data-key="total">Thành tiền</td>
                    <td class="pricing-table-text">{{convert_money($donhang->tongtien+$donhang->tonggiamgia+$donhang->phivanchuyen-$donhang->tonggiamgia)}} VNĐ</td>
                  </tr>




                </tbody>
                
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 margin-bottom">
              <div class="thanks-text full-editable" data-key="thanks">
                <p>
                </p><p>
              </p>
              <p>
              </p>
              <p>
              </p>
              <p>
              </p>
              <p>
              </p>
              <p>Cám Ơn BẠN Đã Mua Sắm Tại {{$tenwebsite}} !</p>
              <p></p>
              <p></p>
              <p></p>
              <p></p>
              <p></p>
              <p></p>
            </div>
            <div class="terms-text full-editable" data-key="terms_and_conditions">
              <p>
              </p><p>
            </p>
            <p><b>If you have any question. Please contact us</b></p>
            <p></p>
            <p></p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 shop-block col-no-margin">
          <div class="shop-address-block full-editable" data-key="shop_block">
            <p>
              <b>{{$tenwebsite}}</b>
            </p>
            <p>{{$diachi}}</p>
            <p>{{$dienthoai}}</p>
          </div>
          <div class="shop-social">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/Fb_icon.jpg?11755453313570768267">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/twitter_Icon.png?11314827648007113849">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/Pintrest_Icon.png?11755453313570768267">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/IG-_ICon.png?11755453313570768267">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/tumblr_icon.jpg?13415655031399799634">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/googleplus.png?4839939554867445933">
            <img class="social-icons" src="https://cdn.shopify.com/s/files/1/0398/5025/files/youtube_9.png?16509925772823836490">
          </div>
          <div class="shop-domain editable" data-key="shop_domain">
            {{$linkwebsite}}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
