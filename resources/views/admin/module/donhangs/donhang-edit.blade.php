@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('donhang.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="col-sm-12 row" style="padding-right: 0px;flex: 0 0 80%;">
    <div class="col-sm-8" style="padding-right: 0px">
        <div class="content-wrapper">
            @include('admin.blocks.alert')
            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card" style="">
                    <div class="card-header header-elements-inline" >
                        <h5 class="card-title">Thông tin đơn hàng</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                  <a href='{{url("admin/in-hoa-don?token=$donhang->token")}}' onclick="window.open(this.href,'wìn','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=1000,height=1200,directories=no,location=no'); return false;" rel="nofollow"><button class="inhd">IN ĐƠN HÀNG</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="box-shadow: 0px 0px 10px 0px #b5b3b38c;">
                        <div class="form-group col-sm-12" >
                            <div class="table-responsive-sm form-group">
                                <table class="table table-sm table-borderless" style="border-bottom: 1px solid #ddd;margin-top: 15px">
                                    <tbody>
                                        @foreach($sanpham as $sp)
                                        <tr>
                                            <td><a href="../../index.php?go=chitietsp&amp;id=38">{{$sp->ten_sp}}</a></td>
                                            <td style="text-align: right;">{{convert_money($sp->price_km)}} x {{$sp->soluong}}</td>
                                            <td style="text-align: right;">{{convert_money($sp->price_km*$sp->soluong)}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive-sm form-group">
                                <table class="table table-sm table-borderless" style="border-bottom: 1px solid #ddd;">
                                    <tbody style="float: right;">
                                        <tr>
                                            <th colspan="6" scope="row" style="text-align: right;">Giá</th>
                                            <td>{{convert_money($donhang->tongtien- $donhang->phivanchuyen)}}</td>
                                        </tr>
                                        <tr>
                                            <th colspan="6" scope="row" style="text-align: right;">Vận chuyển</th>
                                            <td>{{convert_money($donhang->phivanchuyen)}}</td>
                                        </tr>
                                        <tr>
                                            <th colspan="6" scope="row" style="text-align: right;">Tổng cộng</th>
                                            <td>{{convert_money($donhang->tongtien)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <form action="{{ route('donhang.update', ['donhang' => $donhang->id]) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                {{ method_field('PUT') }}
                                <input type="hidden" name="email" value="{{$donhang->email}}">
                                <?php if($donhang->status_paid!=1) {?>
                                    <div class="orderbtnformn">
                                        <div class="orderlaber">
                                            <h5>ĐƠN HÀNG CHƯA ĐƯỢC THANH TOÁN</h5>
                                        </div>
                                        <div class="orderbtn">
                                            <button style="background: #5cb85c;width: 75.89px" name="btnXacnhan" type="submit" class="btna" onclick="return xacnhan('Xác nhận khách hàng đã thanh toán toàn bộ số tiền cho đơn hàng này?')">Xác nhận
                                        </div>
                                    </div>
                                <?php } else {?>
                                <div class="orderbtnforms">
                                    <div class="orderlaber">
                                        <h5>Đơn hàng đã xác nhận thanh toán {{convert_money($donhang->tongtien)}}</h5>
                                    </div>
                                    <div class="orderbtn">
                                        <button  style="background: #FE2E2E" name="btnUndoXacnhan" type="submit" class="btna">Hoàn tác</button>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if($donhang->status_move!=1) {?>
                                <div class="orderbtnformn">
                                    <div class="orderlaber">
                                        <h5>ĐƠN HÀNG CHƯA ĐƯỢC GIAO</h5>
                                    </div>
                                    <div class="orderbtn">
                                        <button style="background: #5cb85c" name="btnUndoGiaohang" type="submit" class="btna">Giao hàng</button>
                                    </div>
                                </div>
                                <?php } else {?>
                                <div class="orderbtnforms">
                                    <div class="orderlaber">
                                        <h5>Đơn hàng đã được giao</h5>
                                    </div>
                                    <div class="orderbtn" >
                                        <button  style="background: #FE2E2E" name="btnGiaohang" type="submit" class="btna">Hoàn tác</button>
                                    </div>
                                </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /content area -->
        </div>
    </div>

    <div class="col-sm-4">
        <div class="content-wrapper">
            @include('admin.blocks.alert')
            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card" style="">
                    <div class="card-header header-elements-inline" >
                        <h5 class="card-title">Thông tin khách hàng</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                               <a data-toggle="modal" data-target="#myModal" style="color: #fff;font-size: 19px" class="inhd fa fa-edit"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="box-shadow: 0px 0px 10px 0px #b5b3b38c;padding-top: 15px">
                        <div class="form-group col-sm-12" id="items" >
                            <p>Họ tên: {{$donhang->hoten}}</p>
                            <p>Email: {{$donhang->email}}</p>
                            <p>Phone: +84 {{$donhang->sodt}}</p>
                            <p>Đia chỉ: {{$donhang->diachi}}
                            <br>&emsp;&emsp;&emsp;&ensp;<?php echo getWard($donhang->ward); ?> <?php echo getDistrict($donhang->district); ?>
                            <br>&emsp;&emsp;&emsp;&ensp;<?php echo getProvince($donhang->province); ?></p>
                            <p>Ghi chú: {{$donhang->ghichu}}</p>
                        </div>
                        <div class="modal modal-adminpro-general default-popup-PrimaryModal PrimaryModal-bgcolor fade" tabindex="-1" role="dialog" id="myModal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header modalback">
                                        <div class="modal-close-area modal-close-df">
                                            <a style="padding-top: 1px;" class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                        </div>
                                        <div class="modal-title pull-right"><h3>Chỉnh sửa thông tin</h3></div>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="id" value="{{$donhang->id}}">
                                            <p><input type="text" placeholder="Text" id="hoten" class="form-control" value="{{$donhang->hoten}}"></p>
                                            <p><input type="text" placeholder="Text" id="email" class="form-control" value="{{$donhang->email}}"></p>
                                            <p><input type="text" placeholder="Text" id="sodt" class="form-control" value="{{$donhang->sodt}}"></p>
                                            <p><input type="text" placeholder="Text" id="diachi" class="form-control" value="{{$donhang->diachi}}"></p>
                                            <p>
                                                <div class="form-select-list">
                                                    <input type="hidden" id="idprovince1" value="{{$donhang->province}}">
                                                    <select class="form-control custom-select-value" id="province"></select>
                                                </div>
                                            </p>
                                            <p>
                                                <div class="form-select-list">
                                                    <input type="hidden" id="iddistrict1" value="{{$donhang->district}}">
                                                    <select class="form-control custom-select-value" id="district"></select>
                                                </div>
                                            </p>
                                            <p>
                                                <div class="form-select-list">
                                                    <input type="hidden" id="idward1" value="{{$donhang->ward}}">
                                                    <select class="form-control custom-select-value" id="ward"></select>
                                                </div>
                                            </p>
                                        <p><textarea id="ghichu" class="form-control">{{$donhang->ghichu}}</textarea></p>
                                    </div>
                                    <div class="modal-footer notification-bt responsive-btn">
                                        <button type="button" class="btn btnclose" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btnsave " data-dismiss="modal" id="SaveButton">Save </button>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /content area -->
        </div>
    </div>
</div>
{{-- {{csrf_field()}} --}}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
    var url = "../../../api/province";
    $.getJSON(url,function(result){
        $.each(result, function(i, field){
            var idprovince1  = $("#idprovince1").val();
            var id   = field.provinceid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==idprovince1)
            {
                select = 'selected=""';
            }
            $("#province").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    var idprovince1 = $("#idprovince1").val();
    var url = "../../../api/district/"+idprovince1;
    $.getJSON(url, function(result){
        console.log(result);
        $.each(result, function(i, field){
            var iddistrict1  = $("#iddistrict1").val();
            var id   = field.districtid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==iddistrict1)
              {
                select = 'selected=""';
              }
            $("#district").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    var iddistrict1 = $("#iddistrict1").val();
        var url = "../../../api/ward/"+iddistrict1;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                if(id==idward1)
                  {
                    select = 'selected=""'
                  }
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    $("#province").change(function(){
        $("#district").html('<option value="-1">---- Lựa chọn Quận/ Huyện ----</option>');
        var idprovince = $("#province").val();
        var url = "../../../api/district/"+idprovince;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });
    });
    $("#district").change(function(){
        $("#ward").html('<option value="-1">---- Lựa chọn Phường/ Xã ----</option>');
        var iddistrict = $("#district").val();
        var url = "../../../api/ward/"+iddistrict;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
    });
    $('#SaveButton').click(function (even){
        var id       = $("#id").val();
        var hoten    = $("#hoten").val();
        var email    = $("#email").val();
        var sodt     = $("#sodt").val();
        var diachi   = $("#diachi").val();
        var province = $("#province").val();
        var district = $("#district").val();
        var ward     = $("#ward").val();
        var ghichu   = $("#ghichu").val();
        try{
            if(hoten=="" || email=="" ||sodt=="" ||diachi=="" ){            
                var msg = "Vui lòng nhập đầy đủ thông tin";
                console.log(msg);
            
            }
            else if (sodt.length <= 9 || sodt.length >=12)
            {
                var msg = "Vui lòng nhập đúng số điện thoại.";
                console.log(msg);
            }
            else{
                $.post('../../../admin/updatedata', {'_token':$('input[name=_token]').val(),
                                                'id'        : id,
                                                'hoten'     : hoten,
                                                'email'     : email,
                                                'sodt'      : sodt,
                                                'diachi'    : diachi,
                                                'province'  : province,
                                                'district'  : district,
                                                'ward'      : ward,
                                                'ghichu'    : ghichu
                                            },
                function(data){
                    $('#items').empty();
                    let html = ` <p>Họ tên: `+data.hoten+`</p>
                                <p>Email: `+data.email+`</p>
                                <p>Phone: +84 `+data.sodt+`</p>
                                <p>Đia chỉ: `+data.diachi+`
                                <br>&emsp;&emsp;&emsp;&ensp;
                                <br>&emsp;&emsp;&emsp;&ensp;</p>
                                <p>Ghi chú: `+data.ghichu+`</p>`;
                    $('#items').append(html);
                    console.dir(data);
                });
            }
        } catch (e){
            console.log(e.message);
        }
///////////end
    });
});
</script>
<!-- <script
  src="http://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> -->
@endsection