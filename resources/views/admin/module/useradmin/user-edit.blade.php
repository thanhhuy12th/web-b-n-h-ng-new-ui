@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">Danh sách</a>
                <span class="breadcrumb-item active">Sửa</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Cập nhật quản trị viên</h5>
                 
            </div>
            <div class="card-body">
                <form action="{{route('useradmin.update', ['sanpham' => $useradmin->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                    <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Họ tên *</label>
                            <input value="{{old('fullname',$useradmin->fullname)}}" type="text" name="fullname" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Số điện thoại *</label>
                            <input value="{{old('phone',$useradmin->phone)}}" type="text" name="phone" class="form-control"  >
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Email *</label>
                            <input value="{{old('email',$useradmin->email)}}" type="text" name="email" class="form-control" >
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Tên đăng nhập *</label>
                            <input value="{{old('username',$useradmin->username)}}" type="text" name="username" class="form-control">
                        </div>
                    </div>
                     <div class="form-group col-sm-12 row">
                        <div class="col-sm-6">
                            <label class="lable_edit">Mật khẩu *</label>
                            <input value="{{old('password')}}" type="password" name="password" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label class="lable_edit">Quyền *</label>
                            <select name="rule" class="form-control pro-edt-select form-control-primary">
                                <option value='0' {{$useradmin->rule == 0 ? 'selected' : ''}}>Admin</option>
                                <option value='1' {{$useradmin->rule == 1 ? 'selected' : ''}}>Admin post</option>
                                <option value='2' {{$useradmin->rule == 2 ? 'selected' : ''}}>Admin product</option>
                                <option value='3' {{$useradmin->rule == 3 ? 'selected' : ''}}>Admin cấp 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                       {{ method_field('PUT') }}
                        <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                            </button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>


@endsection