@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="{{route('useradmin.index')}}" class="breadcrumb-item">Danh sách</a>
                {{-- <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    @include('admin.blocks.alert')
    <div class="content">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('message.danhsach_admin')}}</h5>
                 
            </div>
            <div class="card-body">
                <div class="product-status-wrap">
                    <div class="form-group">
                        <a class="btn btn-success" href="{{ route('useradmin.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                        <div class="table-responsive-sm form-group">
                            <table class="table table-sm table-striped table-bordered">
                                <thead>
                                    <tr >
                                        <th scope="col">
                                            <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                                        </th>
                                        <th scope="col">Mã</th>
                                        <th scope="col">Họ tên</th>
                                        <th scope="col">Username</th>
                                        <th scope="col">Quyền</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Hoạt động</th>
                                        <th scope="col">Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($useradmin as $item)
                                    <tr>
                                        <td >
                                            <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                                        </td>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->fullname}}</td>
                                        <td>{{$item->username}}</td>
                                        <td>
                                            @if($item->rule==0)
                                                Admin
                                            @elseif($item->rule==1)
                                                Admin post
                                            @elseif($item->rule==2)
                                                Admin product
                                            @elseif($item->rule==3)
                                                Admin cấp 3
                                            @endif
                                        </td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>
                                                {{convertdate1($item->created_at)}}
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="{{ route('useradmin.edit', ['useradmin' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a>
                                        </td>                   
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            {{ method_field('DELETE') }}
                            <button onClick="return xacnhan('Bạn có chắc chắn muốn xóa ?');" type="submit" id="btnXoaList" name="btnXoaList" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    
                    </form>
                     {{$useradmin->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection