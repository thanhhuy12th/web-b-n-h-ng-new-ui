<div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">
    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Main sidebar
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->
    <!-- Sidebar content -->
    <div class="sidebar-content">
        <div class="card card-sidebar-mobile">
            <!-- Header -->
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Chức năng</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <!-- /header -->
            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="{{ asset('backend/global_assets/images/image.png') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                        </div>
                        <div class="media-body">
                            <div class="media-title font-weight-semibold">{{Auth::user()->fullname}}</div>
                            <div class="font-size-xs opacity-50">
                                <i style="color: green" class="icon-primitive-dot"></i> &nbsp;Online
                                {{-- /*<span style="color: green;font-size: 20px">•</span> Online*/ --}}
                            </div>
                        </div>
                        <div class="ml-3 align-self-center">
                            <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /user menu -->
            <!-- Main navigation -->
            <div class="card-body p-0">
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                    <!-- Main -->
                    <li class="nav-item">
                        <a href="{{ route('index') }}" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>Trang chủ</span>
                        </a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-users"></i> <span>Tài khoản</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Tài khoản">
                            <li class="nav-item"><a href="{{route('useradmin.index')}}" class="nav-link">Quản trị viên</a></li>
                            <li class="nav-item"><a href="{{route('member.index')}}" class="nav-link">Khách hàng</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-box"></i> <span>Sản phẩm</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Sản phẩm">
                            <li class="nav-item"><a href="{{route('dmsp.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('sanpham.index')}}" class="nav-link">Sản phẩm</a></li>
                            <li class="nav-item"><a href="{{route('donhang.index')}}" class="nav-link">Đơn hàng</a></li>
                            <li class="nav-item"><a href="{{route('coupon.index')}}" class="nav-link">Coupon</a></li>     
                        </ul>
                    </li>
                     <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-link2"></i> <span>Affiliates</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Affiliates">
                             <li class="nav-item"><a href="{{route('affiliates.index')}}" class="nav-link">Danh sách</a></li>
                             <li class="nav-item"><a href="{{route('affiliates-withdraw.index')}}" class="nav-link">Rút tiền</a></li>
                            <li class="nav-item"><a href="{{route('cauhinh-affiliates.editAffiliatesNoId')}}" class="nav-link">Cấu hình</a></li>              
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-newspaper"></i> <span>Tin tức</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Tin tức">
                             <li class="nav-item"><a href="{{route('dmtt.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('tintuc.index')}}" class="nav-link">Tin tức</a></li>              
                        </ul>
                    </li>
                    <li class="nav-item"><a href="{{route('nhanxet.index')}}" class="nav-link"><i class="icon-bubbles10"></i> <span>Nhận xét</span></a></li>
                    <li class="nav-item"><a href="{{route('slide.index')}}" class="nav-link"><i class="icon-image5"></i> <span>Slider</span></a></li>
                    <li class="nav-item"><a href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a></li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-cog5"></i> <span>Cấu hình</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Cấu hình">
                            <li class="nav-item"><a href="{{route('cauhinh.editNoId')}}" class="nav-link">Cấu hình chung</a></li>
                            <li class="nav-item"><a href="{{route('hinhthuc.index')}}" class="nav-link">Thanh toán</a></li>
                            <li class="nav-item"><a href="{{route('vanchuyen.index')}}" class="nav-link">Phí vận chuyển</a></li>             
                        </ul>
                    </li>

                    <!-- /main -->
                </ul>
            </div>
            <!-- /main navigation -->
        </div>
    </div>
    <!-- /sidebar content -->
</div>