@if(session('alertsuc'))
<section class="alert alert-success alert-st-one" role="alert">
    <i class="fa fa-check adminpro-checked-pro admin-check-pro" aria-hidden="true"></i>
        <a class="message-mg-rt">
            {{ session('alertsuc') }}
        </a>
</section>
@endif
@if(session('alerterr'))
<section class="alert alert-danger alert-mg-b alert-st-four" role="alert">
    {{-- <i class="fa fa-window-close adminpro-danger-error admin-check-pro" aria-hidden="true"></i> --}}
        <i class="fa fa-times adminpro-danger-error admin-check-pro" aria-hidden="true">
        <a class="message-mg-rt">
            {{ session('alerterr') }}
        </a></i>
</section>
@endif
@if(session('alerterrv2'))
<section class="alert alert-danger alert-mg-b" role="alert">
    <p style="font-weight: bold;">{{ session('alerterrv2') }}</p>
</section>
@endif
@if($errors->any())
<section class="alert alert-danger alert-mg-b" role="alert">
    @foreach ($errors->all() as $error)
        <span class="message-mg-rt">{{ $error }}</span><br>
    @endforeach
</section>
@endif
@if(session('alertwar'))
<section class="alert alert-warning alert-st-three" role="alert">
    <i class="fa fa-exclamation-triangle adminpro-warning-danger admin-check-pro admin-check-pro-none" aria-hidden="true"></i>
        <p class="message-mg-rt" style="padding-top: 2px">
            {{ session('alertwar') }}
        </p>
</section>
@endif
@if(session('alertwar2'))
<div class="alert alert-info alert-success-style2 alert-success-stylenone">s
    <i class="fa fa-info-circle adminpro-inform admin-check-sucess admin-check-pro-none" aria-hidden="true"></i>
    <p class="message-alert-none">{{ session('alertwar2') }}</p>
</div>
@endif
