$(function(){
    // $('[data-toggle="tooltip"]').tooltip();
     $('[data-toggle="tab"]').on("click", function() {
        $(this).removeClass("active show")  ;             
    });
    $(".side-nav .collapse").on("hide.bs.collapse", function() {
        $(this).prev().eq(1).removeClass("active show")  ;             
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
    });
    $('.side-nav .collapse').on("show.bs.collapse", function() {
        $(this).prev().eq(1).removeClass("active show") ;                   
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
    });

 <ul class="nav navbar-nav side-nav"> 
                                                <li>
                                                    <a href="#stacked-left-pill1" data-toggle="tab">
                                                        <i class="icon-home4"></i>
                                                        <span>Dashboard</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-fw fa-search"></i> Danh sách <i class="fa fa-fw fa-angle-down pull-right"></i></a>
                                                    <ul id="submenu-1" class="collapse">
                                                        <li><a href="#stacked-left-pill2" data-toggle="tab">Lv 1</a></li>
                                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.2</a></li>
                                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 1.3</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="collapse" data-target="#submenu-2"><i class="fa fa-fw fa-star"></i>  MENU 2 <i class="fa fa-fw fa-angle-down pull-right"></i></a>
                                                    <ul id="submenu-2" class="collapse">
                                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.1</a></li>
                                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.2</a></li>
                                                        <li><a href="#"><i class="fa fa-angle-double-right"></i> SUBMENU 2.3</a></li>
                                                    </ul>
                                                </li>
                                               
                                                <li>
                                                    <a href="sugerencias"><i class="fa fa-fw fa-paper-plane-o"></i> MENU 4</a>
                                                </li>
                                                <li>
                                                    <a href="faq"><i class="fa fa-fw fa fa-question-circle"></i> MENU 5</a>
                                                </li>
                                            </ul>

///////////////////

<div class="tab-content" style="width: 100%;background-color: #fff;padding: 10px">
                                            <div class="tab-pane fade" id="stacked-left-pill1">
                                                <div class="row form-group" style="text-align: center;padding-left: 0px">
                                                    <div class="col-md-4" style="padding-left: 0px;padding-right: 0px">
                                                        <button class="btn btn-info" style="width: 95%">Tổng số tiền: 120000đ</button>
                                                    </div>
                                                    <div class="col-md-4" style="padding-left: 0px;padding-right: 0px">
                                                        <button class="btn btn-info" style="width: 95%">Tổng đăng ký: 10</button>
                                                    </div>
                                                    <div class="col-md-4" style="padding-left: 0px;padding-right: 0px">
                                                        <button class="btn btn-info" style="width: 95%">Tổng hóa đơn: 10</button>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-top: 20px">
                                                    <div class="row col-lg-12" style="border-left: 3px solid #35cc7b;border-bottom: 1px solid #35cc7b;">
                                                            <h6>Hóa đơn</h6> 
                                                            <div style="position: absolute;right: 0px">
                                                                <button class="btn" style="background-color: #35cc7b;padding:0px 4px 0px 4px;color: #fff">Xem chi tiết</button>
                                                            </div>
                                                            
                                                    </div>
                                                    <div class="table-responsive-xl">
                                                         <table class="table table-sm table-hover ">
                                                      <thead>
                                                        <tr>
                                                          <th scope="col">Mã</th>
                                                          <th scope="col">Họ tên</th>
                                                          <th scope="col">Số tiền</th>
                                                          <th scope="col">Điện thoại</th>
                                                          <th scope="col">Thời gian</th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                       
                                                            @for( $i = 1; $i < 6 ; $i++)
                                                                <tr>
                                                                  <td>HD{{$i}}</td>
                                                                  <td>Khách hàng{{$i}}</td>
                                                                  <td>100.000đ</td>
                                                                  <td>09148387148</td>
                                                                  <td>12/12/2019 12:12 am</td> 
                                                                </tr>
                                                            @endfor
                                                       
                                                      </tbody>
                                                    </table>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-top: 20px">
                                                    <div class="row col-lg-12" style="border-left: 3px solid #35cc7b;border-bottom: 1px solid #35cc7b;">
                                                            <h6>Khách hàng</h6> 
                                                            <div style="position: absolute;right: 0px">
                                                                <button class="btn" style="background-color: #35cc7b;padding:0px 4px 0px 4px;color: #fff">Xem chi tiết</button>
                                                            </div>
                                                            
                                                    </div>
                                                     <div class="table-responsive-xl">
                                                        <table class="table table-sm table-hover">
                                                          <thead>
                                                            <tr>
                                                              <th scope="col">Mã</th>
                                                              <th scope="col">Họ tên</th>
                                                              <th scope="col">Điện thoại</th>
                                                              <th scope="col">Email</th>
                                                            </tr>
                                                          </thead>
                                                          <tbody>
                                                                @for( $i = 1; $i < 6 ; $i++)
                                                                    <tr>
                                                                      <td>KH{{$i}}</td>
                                                                      <td>Khách hàng{{$i}}</td>
                                                                      <td>09148387148</td>
                                                                      <td>email{{$i}}@gmail.com</td>
                                                                    </tr>
                                                                @endfor
                                                          </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="stacked-left-pill2">
                                                Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
                                            </div>

                                            <div class="tab-pane fade" id="stacked-left-pill3">
                                                DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
                                            </div>

                                            <div class="tab-pane fade" id="stacked-left-pill4">
                                                Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
                                            </div>
                                        </div>



////////////////////////////////////////
<div class="sidebar-content">
                                    <div class="card card-sidebar-mobile">
                                        <div class="card-body p-0">
                                            <ul class="nav nav-sidebar" data-nav-type="accordion">
                                                <li class="nav-item ">
                                                    <a href="#stacked-left-pill1" class="nav-link" data-toggle="tab">
                                                        <i class="icon-home4"></i>
                                                        <span>Dashboard</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item nav-item-submenu">
                                                    <a href="#" class="nav-link"><i class="icon-list3"></i> <span>Danh sách</span></a>
                                                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit" style="display: none">
                                                        <li class="nav-item">
                                                            <a href="#stacked-left-pill2" data-toggle="tab" class="nav-link">Lv 1</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#stacked-left-pill3" data-toggle="tab" class="nav-link">Lv 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="nav-item nav-item-submenu">
                                                    <a href="#" class="nav-link"><i class="icon-cash    "></i> <span>Rút tiền</span></a>

                                                    <ul class="nav nav-group-sub" data-submenu-title="Starter kit" style="display: none">
                                                        <li class="nav-item">
                                                            <a href="#stacked-left-pill2" data-toggle="tab" class="nav-link">Phương thức</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#stacked-left-pill3" data-toggle="tab" class="nav-link">Lịch sử</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>



////////////////////////////
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md align-self-start">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <span class="font-weight-semibold">Main sidebar</span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">
        <div class="card card-sidebar-mobile">

            <!-- Header -->
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Main</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            
            <!-- User menu -->
            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="{{asset('backend/global_assets/images/placeholders/placeholder.jpg')}}" width="38" height="38" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold">Victoria Baker</div>
                            <div class="font-size-xs opacity-50">
                                <i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA
                            </div>
                        </div>

                        <div class="ml-3 align-self-center">
                            <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /user menu -->

            
            <!-- Main navigation -->
            <div class="card-body p-0">
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                 @if(Auth::check())
                     @if(Auth::user()->rule==0)
                    <!-- Main -->
                    <li class="nav-item-header mt-0"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                    <li class="nav-item">
                        <a href="{{ route('index') }}" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
                                Trang chủ
                               
                            </span>
                        </a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-people"></i> <span>Tài khoản</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Page layouts">
                            <li class="nav-item"><a href="{{route('useradmin.index')}}" class="nav-link">Quản trị viên</a></li>
                            <li class="nav-item"><a href="{{route('member.index')}}" class="nav-link">Khách hàng</a></li>                           
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-balance"></i> <span>Sản phẩm</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Page layouts">
                            <li class="nav-item"><a href="{{route('dmsp.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('sanpham.index')}}" class="nav-link">Sản phẩm</a></li>
                            <li class="nav-item"><a href="{{route('donhang.index')}}" class="nav-link">Đơn hàng</a></li>
                            <li class="nav-item"><a href="{{route('coupon.index')}}" class="nav-link">Coupon</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Tin tức</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Page layouts">
                            <li class="nav-item"><a href="{{route('dmtt.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('tintuc.index')}}" class="nav-link">Tin tức</a></li>                           
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('nhanxet.index')}}" class="nav-link"><i class="icon-width"></i> <span>Nhận xét</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('slide.index')}}" class="nav-link"><i class="icon-images3"></i> <span>Slider</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('lienhe.index')}}" class="nav-link"><i class="icon-phone"></i> <span>Liên hệ</span></a>
                    </li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Cấu hình</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Page layouts">
                            <li class="nav-item"><a href="{{route('cauhinh.editNoId')}}" class="nav-link">Cấu hình chung</a></li>
                            <li class="nav-item"><a href="{{route('hinhthuc.index')}}" class="nav-link">Thanh toán</a></li>
                            <li class="nav-item"><a href="{{route('vanchuyen.index')}}" class="nav-link">Phí vận chuyển</a></li>                           
                        </ul>
                    </li>   
                    <!-- /layout -->
                @elseif(Auth::user()->rule==1)
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Tin tức</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Page layouts">
                            <li class="nav-item"><a href="{{route('dmtt.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('tintuc.index')}}" class="nav-link">Tin tức</a></li>                           
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('nhanxet.index')}}" class="nav-link"><i class="icon-width"></i> <span>Nhận xét</span></a>
                    </li>
                @else
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-balance"></i> <span>Sản phẩm</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Page layouts">
                            <li class="nav-item"><a href="{{route('dmsp.index')}}" class="nav-link">Danh mục</a></li>
                            <li class="nav-item"><a href="{{route('sanpham.index')}}" class="nav-link">Sản phẩm</a></li>
                            <li class="nav-item"><a href="{{route('donhang.index')}}" class="nav-link">Đơn hàng</a></li>
                            <li class="nav-item"><a href="{{route('coupon.index')}}" class="nav-link">Coupon</a></li>
                        </ul>
                    </li>
                @endif
            @endif
                </ul>
            </div>
            <!-- /main navigation -->

        </div>
    </div>
    <!-- /sidebar content -->
    
</div>


//////head

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title','Chào mừng bạn đến với trang quản trị website')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('backend/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('backend/global_assets/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('backend/global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>

    <script src="{{asset('backend/assets/js/app.js')}}"></script>
    <script src="{{asset('backend/global_assets/js/demo_pages/dashboard.js')}}"></script>
    <!-- /theme JS files -->


////////header
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="logo-pro">
                <a href="home"><img class="main-logo" src="{{ asset('frontend/asset_admin/img/logo/logo.png') }}" alt="" /></a>
            </div>
        </div>
    </div>
</div>
<div class="header-advance-area">
    <div class="header-top-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="header-top-wraper">
                        <div class="row">
                            <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                <div class="menu-switcher-pro">
                                    <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                            
                            </div>
                            <div style="float: right">
                                <div style="margin-right: 50px;" class="header-right-info">
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                        <li class="nav-item">
                                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                    <i class="fa fa-user adminpro-user-rounded header-riht-inf" aria-hidden="true"></i>
                                                    <span class="admin-name">{{Auth::user()->fullname}}</span>
                                                    <i class="fa fa-angle-down adminpro-icon adminpro-down-arrow"></i>
                                                </a>
                                            <ul style="width: 100px;float: right" role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                <li>
                                                    <a href="#"><span class="fa fa-user author-log-ic"></span>
                                                    @if(Auth::user()->rule=="0")
                                                        Admin
                                                    @elseif(Auth::user()->rule=="1")
                                                        Admin post
                                                    @elseif(Auth::user()->rule=="2")
                                                        Admin product
                                                    @elseif(Auth::user()->rule=="3")
                                                        Admin cấp 3
                                                    @endif
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('admin.getLogout')}}">
                                                        <span class="fa fa-lock author-log-ic"></span> Đăng xuất
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>    
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu start -->
    <!-- Mobile Menu end -->
    <div class="breadcome-area" style="margin-top: -2px;height: 55px;">
    @yield('breadcome')
    </div>
</div>


//////conten

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="analysis-progrebar-ctn123">
            <div class="product-status-wrap123">
                <form action="" name="listForm" id="listForm" method="post" >
                {!! csrf_field() !!}
                    <table>
                        <tr>
                            <th>Username</th>
                            <th>Quyền</th>
                            <th>Đăng nhập</th>
                        </tr>
                        @foreach($status as $item)
                            <tr>
                            <td style="width: 10%">{{$item->name}}</td>
                            <td style="width: 10% ">
                            @if($item->rule==0)
                                Admin
                            @elseif($item->rule==1)
                                Admin post
                            @elseif($item->rule==2)
                                Admin product
                            @elseif($item->rule==3)
                                Admin cấp 3
                            @endif
                            </td>
                            <td style="width: 20%">
                                {{convertdate1($item->time)}}
                            </td>                 
                            </tr>
                        @endforeach
                    </table>
                </form>
                {{$status->links()}}
            </div>
        </div>
</div>