	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>@yield('title','Chào mừng bạn đến với trang quản trị website')</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/global_assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/global_assets/css/icons/material/icons.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backend/assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{asset('backend/global_assets/js/main/jquery.min.js')}}"></script>
	<script src="{{asset('backend/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('backend/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
	<script src="{{asset('js/myscript.js') }}"></script>
	<!-- /core JS files -->
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/css-toggle-switch@latest/dist/toggle-switch.css" />
	{{-- <script src="{{asset('backend/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script> --}}
	{{-- <script src="{{asset('backend/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script src="{{asset('backend/global_assets/js/plugins/forms/styling/switch.min.js')}}"></script>
	 --}}

	<!-- Theme JS files -->
	<script src="{{asset('backend/assets/js/app.js')}}"></script>
	<!-- /theme JS files -->

