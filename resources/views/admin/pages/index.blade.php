@extends('admin.welcome')

@section('breadcrumb')
<div class="page-header">
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('index')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                {{-- <a href="#" class="breadcrumb-item">Link</a>
                <span class="breadcrumb-item active">Current</span> --}}
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content area -->
    <div class="row form-group" style=" margin-right: 0px; margin-left: 0px;">
        <div class="col-lg-4">
            <div class="bg-teal-400 row" style=" margin-right: 0px; margin-left: 0px; border-radius: 3px;">
                <div class="col-4"><i style="font-size: 5rem;" class="icon-piggy-bank dashboard-icon"></i></div>
                <div class="col-8" style="padding-top: 6px">
                    <p>Tổng thu nhập</p>
                    <span style="font-size: 1.5rem">100.000 đ</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="bg-teal-401 row" style=" margin-right: 0px; margin-left: 0px;border-radius: 3px;">
                <div class="col-4"><i style="font-size: 5rem;" class="icon-bag dashboard-icon"></i></div>
                <div class="col-8" style="padding-top: 6px">
                    <p>Tổng hóa đơn</p>
                    <span style="font-size: 1.5rem">100</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="bg-teal-402 row" style=" margin-right: 0px; margin-left: 0px;border-radius: 3px;">
                <div class="col-4"><i style="font-size: 5rem;" class="icon-user-tie dashboard-icon"></i></div>
                <div class="col-8" style="padding-top: 6px">
                    <p>Tổng khách hàng</p>
                    <span style="font-size: 1.5rem">100</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row form-group" style=" margin-right: 0px; margin-left: 0px;">
        <div class="col-lg-4" >
            <div class="content" style=" margin-right: 0px;margin-left: 0px">
            <!-- Basic card -->
                <div class="card " style="margin-bottom: 0px">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Liên hệ mới nhất</h5>
                        
                    </div>
                    <div class="card-body over-y-content">
                        <div class="form-group" style="padding: 5px">
                            <div class="row">
                                <div class="col-6">Nguyễn Văn A</div>
                                <div class="col-6" style="font-size: 11px;text-align: right;">12/12/2019 10:10</div>
                            </div>
                            <div class="div-content">
                                <i class="icon-envelop4"></i><span> Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây . Nội dung được hiển thị tại đây</span>
                            </div>
                        </div>
                        <div class="form-group" style="padding: 5px">
                            <div class="row">
                                <div class="col-6">Nguyễn Văn A</div>
                                <div class="col-6" style="font-size: 11px;text-align: right;">12/12/2019 10:10</div>
                            </div>
                            <div class="div-content">
                                <i class="icon-envelop4"></i><span> Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây . Nội dung được hiển thị tại đây</span>
                            </div>
                        </div>
                        <div class="form-group" style="padding: 5px">
                            <div class="row">
                                <div class="col-6">Nguyễn Văn A</div>
                                <div class="col-6" style="font-size: 11px;text-align: right;">12/12/2019 10:10</div>
                            </div>
                            <div class="div-content">
                                <i class="icon-envelop4"></i><span> Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây . Nội dung được hiển thị tại đây</span>
                            </div>
                        </div>
                        <div class="form-group" style="padding: 5px">
                            <div class="row">
                                <div class="col-6">Nguyễn Văn A</div>
                                <div class="col-6" style="font-size: 11px;text-align: right;">12/12/2019 10:10</div>
                            </div>
                            <div class="div-content">
                                <i class="icon-envelop4"></i><span> Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây . Nội dung được hiển thị tại đây</span>
                            </div>
                        </div>
                        <div class="form-group" style="padding: 5px">
                            <div class="row">
                                <div class="col-6">Nguyễn Văn A</div>
                                <div class="col-6" style="font-size: 11px;text-align: right;">12/12/2019 10:10</div>
                            </div>
                            <div class="div-content">
                                <i class="icon-envelop4"></i><span> Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây . Nội dung được hiển thị tại đây</span>
                            </div>
                        </div>
                        <div class="form-group" style="padding: 5px">
                            <div class="row">
                                <div class="col-6">Nguyễn Văn A</div>
                                <div class="col-6" style="font-size: 11px;text-align: right;">12/12/2019 10:10</div>
                            </div>
                            <div class="div-content">
                                <i class="icon-envelop4"></i><span> Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây. Nội dung được hiển thị tại đây . Nội dung được hiển thị tại đây</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8" >
            <div class="content" style=" margin-left: 0px;margin-right: 0px">
                <!-- Basic card -->
                <div class="card" >
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Danh sách 5 đơn hàng mới nhất</h5>
                        
                    </div>
                    <div class="card-body over-y-content">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Mã ĐH</th>
                                    <th>Họ tên</th>
                                    <th>Điện thoại</th>
                                    <th>Trị giá</th>
                                    <th>Trạng thái</th>
                                    <th>Thời gian</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($i = 1 ; $i < 6 ; $i++)
                                <tr>
                                    <td>ĐH0{{$i}}</td>
                                    <td>Khách hàng 01</td>
                                    <td>0995391679</td>
                                    <td>1.200.000đ</td>
                                    <td>Chưa thanh toán</td>
                                    <td>12/12/2019 12:12</td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- /content area -->
</div>
@endsection